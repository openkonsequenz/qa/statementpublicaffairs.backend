/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.api;

import java.util.List;
import java.util.Map;

import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaClaimTaskRequestBody;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaCompleteExternalTaskRequestBody;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaExternalTask;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaExternalTaskFetchAndLockRsp;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaFailureExternalTaskRequestBody;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaFetchAndLockExtTaskRequestBody;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessDefinition;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessDefinitionDiagramXML;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessDefinitionStartReq;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessDefinitionStartRsp;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessHistoryStep;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessInstance;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaTaskInstance;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaVariable;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaVariablesRequestBody;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Interfaces used to access the Camunda workflow engine.
 * 
 * @author Tobias Stummer
 *
 */
@FeignClient(name = "${services.camunda.name}")
public interface CamundaApi {

	@GetMapping(value = "engine-rest/process-definition")
	public List<CamundaProcessDefinition> getProcessDefinitions(@RequestHeader("Authorization") String auth);

	@PostMapping(value = "engine-rest/process-definition/key/{key}/tenant-id/{tenant-id}/start")
	public CamundaProcessDefinitionStartRsp startProcess(@RequestParam("key") String processKey,
			@RequestParam("tenant-id") String tenantId, @RequestHeader("Authorization") String auth,
			@RequestBody() CamundaProcessDefinitionStartReq startReq);

	@GetMapping(value = "engine-rest/process-instance")
	public List<CamundaProcessInstance> getProcessInstances(@RequestHeader("Authorization") String auth,
			@RequestParam("processDefinitionKey") String processDefinitionKey);

	@GetMapping(value = "engine-rest/history/process-instance")
	public List<CamundaProcessInstance> getProcessInstancesWithBusinessKey(@RequestHeader("Authorization") String auth,
			@RequestParam("processDefinitionKey") String processDefinitionKey,
			@RequestParam("processInstanceBusinessKey") String businessKey);

	@GetMapping(value = "engine-rest/history/activity-instance")
	public List<CamundaProcessHistoryStep> getProcessHistory(@RequestHeader("Authorization") String auth,
			@RequestParam("processInstanceId") String processInstanceId);

	@GetMapping(value = "engine-rest/task")
	public List<CamundaTaskInstance> getCurrentTaskInstances(@RequestHeader("Authorization") String auth);

	@GetMapping(value = "engine-rest/task")
	public List<CamundaTaskInstance> getCurrentTaskInstancesWithProcessInstanceBusinessKey(
			@RequestHeader("Authorization") String auth,
			@RequestParam("processDefinitionKey") String processDefinitionKey,
			@RequestParam("processInstanceBusinessKey") String businessKey);

	@GetMapping(value = "engine-rest/task/{task-id}")
	public CamundaTaskInstance getTaskInstance(@RequestHeader("Authorization") String auth,
			@RequestParam("task-id") String taskId);

	@GetMapping(value = "engine-rest/task/{task-id}/form-variables")
	public Map<String, CamundaVariable> getTaskInstanceFormVariables(@RequestHeader("Authorization") String auth,
			@RequestParam("task-id") String taskId);

	@PostMapping(value = "engine-rest/task/{task-id}/claim")
	public feign.Response claimTaskInstance(@RequestHeader("Authorization") String auth,
			@RequestParam("task-id") String taskId, @RequestBody() CamundaClaimTaskRequestBody claimTaskRequestBody);

	@PostMapping(value = "engine-rest/task/{task-id}/unclaim")
	public feign.Response unclaimTaskInstance(@RequestHeader("Authorization") String auth,
			@RequestParam("task-id") String taskId);

	@PostMapping(value = "engine-rest/task/{task-id}/complete")
	public feign.Response completeTask(@RequestHeader("Authorization") String auth,
			@RequestParam("task-id") String taskId, @RequestBody() CamundaVariablesRequestBody completeTaskRequestBody);

	@GetMapping(value = "engine-rest/external-task")
	public List<CamundaExternalTask> getCurrentExternalTaskInstances(@RequestHeader("Authorization") String auth);

	@PostMapping(value = "engine-rest/external-task/fetchAndLock")
	public List<CamundaExternalTaskFetchAndLockRsp> fetchAndLockExternalTaskInstances(
			@RequestHeader("Authorization") String auth,
			@RequestBody() CamundaFetchAndLockExtTaskRequestBody fetchAndLockExtTaskRequestBody);

	@PostMapping(value = "engine-rest/external-task/{task-id}/complete")
	public feign.Response completeExternalTaskInstance(@RequestHeader("Authorization") String auth,
			@RequestParam("task-id") String taskId,
			@RequestBody() CamundaCompleteExternalTaskRequestBody completeExternalTaskRequestBody);

	@PostMapping(value = "engine-rest/external-task/{task-id}/failure")
	public feign.Response failureExternalTaskInstance(@RequestHeader("Authorization") String auth,
			@RequestParam("task-id") String taskId,
			@RequestBody() CamundaFailureExternalTaskRequestBody failureExternalTaskRequestBody);

	@GetMapping(value = "engine-rest/process-definition/{processdefinition-id}")
	public CamundaProcessDefinition getProcessDefinitionForId(@RequestHeader("Authorization") String auth,
			@RequestParam("processdefinition-id") String processDefinitionId);

	@DeleteMapping(value = "engine-rest/process-instance/{process-id}")
	public feign.Response deleteProcessInstanceWithProcessId(@RequestHeader("Authorization") String auth,
			@RequestParam("process-id") String processId);

	@GetMapping(value = "engine-rest/process-definition/{id}/xml")
	public CamundaProcessDefinitionDiagramXML getDiagramXML(@RequestHeader("Authorization") String auth,
			@RequestParam("id") String processDefinitionId);

}
