/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.eclipse.openk.statementpublicaffairs.exceptions.ConfigurationException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailConfiguration;
import org.eclipse.openk.statementpublicaffairs.service.mail.MailContext;
import org.eclipse.openk.statementpublicaffairs.service.mail.MailSession;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailSendReport;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Mail facade used to load mail context and send mails.
 * 
 * @author Tobias Stummer
 *
 */
@Service
public class MailUtil {

	@Value("${mail.configurationPath}")
	private String mailConfigPath;

	public Properties loadMailProperties(String ressourcePath) throws ConfigurationException {
		Properties properties = new Properties();
		try {
			File file = ResourceUtils.getFile(ressourcePath);
			properties.load(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			throw new ConfigurationException("File not found. Could not load mail properties file " + ressourcePath, e);
		} catch (IOException e) {
			throw new ConfigurationException("Invalid format. Could not load mail properties file " + ressourcePath, e);
		}
		return properties;
	}

	public MailSession sessionFor(String propertiesPath) throws ConfigurationException {
		Properties properties = loadMailProperties(propertiesPath);
		return new MailSession(properties);
	}

	public MailSendReport sendMail(Message message) throws MessagingException {
		MailSendReport report = new MailSendReport();
		report.setSuccessful(false);
		try {
			Transport.send(message);
			report.setSuccessful(true);
		} catch (MessagingException e) {
			report.setReason("General Messaging Exception. - " + e.getMessage());
		}
		return report;
	}

	public MailContext contextFor(String notificationMailPropertiesPath) throws ConfigurationException {
		return new MailContext(this, notificationMailPropertiesPath);
	}

	public boolean validEmailAddress(String emailAddress) {
		if (emailAddress == null) {
			return false;
		}
		InternetAddress addr = null;
		try {
			addr = new InternetAddress(emailAddress);
		} catch (AddressException e) {
			return false;
		}
		return addr != null;
	}

	public void filterRecipients(Set<String> recipients, MailConfiguration mailConfig) {
		if (mailConfig.getEmailFilter() == null) {
			return;
		}
		Set<String> allowedEmailAddresses = mailConfig.getEmailFilter();
		Set<String> filteredRecipients = new HashSet<>();
		for (String recipient : recipients) {
			if (allowedEmailAddresses.contains(recipient)) {
				filteredRecipients.add(recipient);
			}
		}
		recipients.clear();
		recipients.addAll(filteredRecipients);
	}

	public MailConfiguration getMailConfiguration() throws InternalErrorServiceException {
		try {
			File file = ResourceUtils.getFile(mailConfigPath);
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.readValue(file, MailConfiguration.class);
		} catch (IOException e) {
			throw new InternalErrorServiceException("Could not load templateConfig from file " + mailConfigPath, e);
		}
	}
}
