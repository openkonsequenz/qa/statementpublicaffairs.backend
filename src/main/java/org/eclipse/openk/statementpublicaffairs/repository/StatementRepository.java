/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.repository;

import java.util.Collection;
/**
 * @author Tobias Stummer
 *
 */
import java.util.List;
import java.util.UUID;

import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface StatementRepository extends CrudRepository<TblStatement, Long> {

    @Query("select s from TblStatement s where s.businessKey = ?1")
    List<TblStatement> findByTaskId(UUID taskId);
    
    @Query("select s from TblStatement s where s.finished=:finished AND s.businessKey is null")
    List<TblStatement> findByFinishedAndFilterNoTaskId(@Param( "finished") Boolean finished);

    @Query("select s from TblStatement s where s.finished=:finished AND s.businessKey is not null")
    List<TblStatement> findByFinishedAndFilterHasTaskId(@Param( "finished") Boolean finished);
    
    List<TblStatement> findByIdIn(Collection<Long> ids);

    
}

