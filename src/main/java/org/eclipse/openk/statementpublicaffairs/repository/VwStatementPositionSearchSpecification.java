/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.openk.statementpublicaffairs.model.SearchParams;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementPositionSearch;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.springframework.data.jpa.domain.Specification;

public class VwStatementPositionSearchSpecification implements Specification<VwStatementPositionSearch> {

	private static final long serialVersionUID = 1L;

	private SearchParams searchParams;

	public VwStatementPositionSearchSpecification(SearchParams params) {
		this.searchParams = params;
	}

	@Override
	public Predicate toPredicate(Root<VwStatementPositionSearch> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder) {
		Predicate[] predicates = createArrayOfPredicates(root, criteriaBuilder);
		if (predicates == null || predicates.length == 0) {
			return null;
		}
		return criteriaBuilder.and(predicates);
	}

	private Predicate[] createArrayOfPredicates(Root<VwStatementPositionSearch> root, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<>();
		TypeConversion.dateOfDateString(searchParams.getDueDateFrom()).ifPresent(
				date -> predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.<LocalDate>get("dueDate"), date)));
		TypeConversion.dateOfDateString(searchParams.getDueDateTo()).ifPresent(
				date -> predicates.add(criteriaBuilder.lessThanOrEqualTo(root.<LocalDate>get("dueDate"), date)));

		if (searchParams.getTypeId() != null) {
			predicates.add(criteriaBuilder.equal(root.<Long>get("typeId"), searchParams.getTypeId()));
		}

		if (searchParams.getFinished() != null) {
			predicates.add(criteriaBuilder.equal(root.<Boolean>get("finished"), searchParams.getFinished()));
		}
		return predicates.stream().toArray(Predicate[]::new);
	}

}
