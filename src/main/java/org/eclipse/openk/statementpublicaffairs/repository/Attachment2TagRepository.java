/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.repository;

import org.eclipse.openk.statementpublicaffairs.model.db.TblAttachment2Tag;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Tobias Stummer
 *
 */

public interface Attachment2TagRepository extends CrudRepository<TblAttachment2Tag, Long> {

    @Query("select t from TblAttachment2Tag t where t.attachment_id = ?1")
	Iterable<TblAttachment2Tag> findByAttachmentId(Long attachmentId);

}
