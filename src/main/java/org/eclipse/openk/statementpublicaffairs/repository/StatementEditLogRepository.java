/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.repository;

import java.util.List;

import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementEditLog;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Tobias Stummer
 *
 */

public interface StatementEditLogRepository extends CrudRepository<TblStatementEditLog, Long> {

    @Query("select s from TblStatementEditLog s where s.statement.id= ?1")
	List<TblStatementEditLog> findByStatementId(Long id);

    @Query("select s from TblStatementEditLog s where s.statement.id= ?1 and s.user.username = ?2")
	List<TblStatementEditLog> findByStatementIdAndUsername(Long id, String username);

}
