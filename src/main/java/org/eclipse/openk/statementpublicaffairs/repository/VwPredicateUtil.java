/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.openk.statementpublicaffairs.model.SearchParams;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;

public class VwPredicateUtil {

	/**
	 * Utility - private constructor.
	 */
	private VwPredicateUtil() {

	}

	public static List<Predicate> createBasicStatementSearchPredicates(Root<?> root, CriteriaBuilder criteriaBuilder,
			SearchParams searchParams) {
		List<Predicate> predicates = new ArrayList<>();
		TypeConversion.dateOfDateString(searchParams.getReceiptDateFrom()).ifPresent(
				date -> predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.<LocalDate>get("receiptDate"), date)));
		TypeConversion.dateOfDateString(searchParams.getReceiptDateTo()).ifPresent(
				date -> predicates.add(criteriaBuilder.lessThanOrEqualTo(root.<LocalDate>get("receiptDate"), date)));

		TypeConversion.dateOfDateString(searchParams.getDueDateFrom()).ifPresent(
				date -> predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.<LocalDate>get("dueDate"), date)));
		TypeConversion.dateOfDateString(searchParams.getDueDateTo()).ifPresent(
				date -> predicates.add(criteriaBuilder.lessThanOrEqualTo(root.<LocalDate>get("dueDate"), date)));

		TypeConversion.dateOfDateString(searchParams.getCreationDateFrom()).ifPresent(date -> predicates
				.add(criteriaBuilder.greaterThanOrEqualTo(root.<LocalDate>get("creationDate"), date)));
		TypeConversion.dateOfDateString(searchParams.getCreationDateTo()).ifPresent(
				date -> predicates.add(criteriaBuilder.lessThanOrEqualTo(root.<LocalDate>get("creationDate"), date)));

		if (searchParams.getCity() != null) {
			predicates.add(criteriaBuilder.equal(root.<String>get("city"), searchParams.getCity()));
		}

		if (searchParams.getDistrict() != null) {
			predicates.add(criteriaBuilder.equal(root.<String>get("district"), searchParams.getDistrict()));
		}

		if (searchParams.getTypeId() != null) {
			predicates.add(criteriaBuilder.equal(root.<Long>get("typeId"), searchParams.getTypeId()));
		}

		if (searchParams.getFinished() != null) {
			predicates.add(criteriaBuilder.equal(root.<Boolean>get("finished"), searchParams.getFinished()));
		}

		for (String s : searchParams.getSearchStrings()) {
			predicates.add(criteriaBuilder.like(root.<String>get("searchfield"), "%" + s + "%"));
		}
		return predicates;
	}

}
