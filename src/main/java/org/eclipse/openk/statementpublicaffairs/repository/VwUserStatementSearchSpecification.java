/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.repository;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.openk.statementpublicaffairs.model.SearchParams;
import org.eclipse.openk.statementpublicaffairs.model.db.VwUserStatementSearch;
import org.springframework.data.jpa.domain.Specification;

public class VwUserStatementSearchSpecification implements Specification<VwUserStatementSearch> {

	private static final long serialVersionUID = 1L;

	private SearchParams searchParams;

	private Long userId;

	public VwUserStatementSearchSpecification(SearchParams params, Long userId) {
		this.searchParams = params;
		this.userId = userId;
	}

	@Override
	public Predicate toPredicate(Root<VwUserStatementSearch> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder) {
		Predicate[] predicates = createArrayOfPredicates(root, criteriaBuilder);
		if (predicates == null || predicates.length == 0) {
			return null;
		}
		return criteriaBuilder.and(predicates);
	}

	private Predicate[] createArrayOfPredicates(Root<VwUserStatementSearch> root, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = VwPredicateUtil.createBasicStatementSearchPredicates(root, criteriaBuilder,
				searchParams);
		if (userId != null) {
			predicates.add(criteriaBuilder.equal(root.<Long>get("userId"), userId));
		}
		return predicates.stream().toArray(Predicate[]::new);
	}

}
