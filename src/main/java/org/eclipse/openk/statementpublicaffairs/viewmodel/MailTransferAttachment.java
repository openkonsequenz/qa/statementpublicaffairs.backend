package org.eclipse.openk.statementpublicaffairs.viewmodel;

import java.util.Set;

import lombok.Data;

@Data
public class MailTransferAttachment {
	private String name;
	private Set<String> tagIds;

}
