/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.viewmodel;

import lombok.Data;

/**
 * Data object containing user info.
 * 
 * @author Tobias Stummer
 *
 */
@Data
public class UserInfoModel {

	/**
	 * First name of user.
	 */
	private String firstName;

	/**
	 * Last name of user.
	 */
	private String lastName;

	/**
	 * Roles assigned to the user.
	 */
	private String[] roles;

	/**
	 * Username of user.
	 */
	private String userName;

}
