/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * Throw exception to create BadRequest Http responses.
 * @author Tobias Stummer
 *
 */
@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerErrorException extends RuntimeException{
   
    private static final String DEFAULT_MESSAGE = "Error in internal service.";
    
    public InternalServerErrorException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Create InternalServerErrorException with own message. 
     * @param message Message
     */
    public InternalServerErrorException(String message) {
        super(message);
    }
    
    /**
     * Create InternalServerErrorException with own message and cause.
     * @param message Message
     * @param cause Cause
     */
    public InternalServerErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}


