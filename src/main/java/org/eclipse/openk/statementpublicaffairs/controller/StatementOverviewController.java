/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.springframework.data.domain.Sort.Direction.DESC;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalServerErrorException;
import org.eclipse.openk.statementpublicaffairs.model.SearchParams;
import org.eclipse.openk.statementpublicaffairs.service.StatementOverviewService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DashboardStatement;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementPosition;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller that contains end points to get an overview of the current
 * statements.
 * 
 * @author Tobias Stummer
 *
 */
@RestController
public class StatementOverviewController {
	@Autowired
	private StatementOverviewService statementOverviewService;

	/**
	 * AllStatements GET endpoint provides a list of all statements in the database.
	 * 
	 * @return List containing all Statements as StatementModel
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/statements")
	public List<StatementDetailsModel> getAllStatements(@RequestParam("id") Optional<List<Long>> filterStatementIds) {
		try {
			if (filterStatementIds.isPresent()) {
				return statementOverviewService.getStatementModelsByInIds(filterStatementIds.get());
			}
			return statementOverviewService.getAllStatementModels();
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Could not access statement list ressource.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * AllStatements GET endpoint provides a list of all statements in the database.
	 * 
	 * @return List containing all Statements as StatementModel
	 */
	@SuppressWarnings("squid:S00107")
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/statementsearch")
	public Page<StatementDetailsModel> getPagedStatements(
			@RequestParam(name = "q", required = false) List<String> searchStrings,
			@RequestParam(name = "receiptDateFrom", required = false) String receiptDateFrom,
			@RequestParam(name = "receiptDateTo", required = false) String receiptDateTo,
			@RequestParam(name = "dueDateFrom", required = false) String dueDateFrom,
			@RequestParam(name = "dueDateTo", required = false) String dueDateTo,
			@RequestParam(name = "creationDateFrom", required = false) String creationDateFrom,
			@RequestParam(name = "creationDateTo", required = false) String creationDateTo,
			@RequestParam(name = "city", required = false) String city,
			@RequestParam(name = "district", required = false) String district,
			@RequestParam(name = "typeId", required = false) Long typeId,
			@RequestParam(name = "finished", required = false) Boolean finished,
			@RequestParam(name = "editedByMe", required = false) Boolean editedByMe,
			@PageableDefault(sort = { "id" }, size = 10000, direction = DESC) Pageable pageable) {
SearchParams params = new SearchParams();
		params.setSearchStrings(streamlinedSearchStrings(searchStrings));
		params.setReceiptDateFrom(receiptDateFrom);
		params.setReceiptDateTo(receiptDateTo);
		params.setDueDateFrom(dueDateFrom);
		params.setDueDateTo(dueDateTo);
		params.setCreationDateFrom(creationDateFrom);
		params.setCreationDateTo(creationDateTo);
		params.setCity(city);
		params.setDistrict(district);
		params.setFinished(finished);
		params.setTypeId(typeId);
		try {
			if (Boolean.TRUE.equals(editedByMe)) {
				return statementOverviewService.searchStatementModelsEditedByMe(params, pageable);
			} else {
				return statementOverviewService.searchStatementModels(params, pageable);
			}
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	protected String[] streamlinedSearchStrings(List<String> searchStrings) {
		List<String> searchStringsList = new ArrayList<>();
		if (searchStrings != null && !searchStrings.isEmpty()) {
			String s = String.join(" ", searchStrings);
			searchStringsList.addAll(streamlinedSearchString(s));
		}
		return searchStringsList.toArray(new String[searchStringsList.size()]);
	}

	protected List<String> streamlinedSearchString(String searchString) {
		String streamlinedString = searchString.replaceAll("[^a-zA-Z0-9]", " ");
		streamlinedString = streamlinedString.toUpperCase();
		String[] searchStrings = streamlinedString.split(" ");
		List<String> searchStringsList = new ArrayList<>();
		for (String s : searchStrings) {
			String stripped = s.trim().toUpperCase();
			if (stripped.length() > 0) {
				searchStringsList.add(stripped);
			}
		}
		return searchStringsList;
	}

	/**
	 * AllStatementTypes GET endpoint provides a list of statement-types in the
	 * database.
	 * 
	 * @return List containing all StatenentTypes as StatementTypeModel
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/statement-data/types")
	public List<StatementTypeModel> getAllStatementTypes() {
		try {
			return statementOverviewService.getStatementTypeModels();
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Could not access statement type list ressource.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * List enriched details of statements relevant for the dash-board view.
	 * 
	 * @return List containing all relevant DashboardStatement models
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/dashboard/statements")
	public List<DashboardStatement> getDashboardStatements() {
		try {
			return statementOverviewService.getDashboardStatements();
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Could not access dashboard statement list ressource.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}
	
	/**
	 * List statement position enriched details of statements relevant for the dash-board view.
	 * 
	 * @return List containing all relevant DashboardStatement models
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/statementpositionsearch")
	public List<StatementPosition> searchStatementPositions(
			@RequestParam(name = "dueDateFrom", required = false) String dueDateFrom,
			@RequestParam(name = "dueDateTo", required = false) String dueDateTo,
			@RequestParam(name = "typeId", required = false) Long typeId,
			@RequestParam(name = "finished", required = false) Boolean finished) {
		SearchParams params = new SearchParams();
		params.setDueDateFrom(dueDateFrom);
		params.setDueDateTo(dueDateTo);
		params.setFinished(finished);
		params.setTypeId(typeId);
		try {
			return statementOverviewService.searchStatementPositions(params);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}
}
