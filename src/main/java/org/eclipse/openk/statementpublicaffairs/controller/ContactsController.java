/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.springframework.data.domain.Sort.Direction.DESC;

import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalServerErrorException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundException;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.service.ContactService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ContactModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller provides endpoints to search for contacts managed by
 * ContactBaseData module.
 * 
 * @author Tobias Stummer
 *
 */
@RestController
public class ContactsController {

	@Autowired
	private ContactService contactService;

	/**
	 * Search for available contacts. Search results are proxied to the
	 * ContactBaseData module.
	 * 
	 * @param searchString full text search value
	 * @param pageable     pagable parameters
	 * @return Paged list of {@link ContactModel} search results
	 */
	@Secured({ UserRoles.SPA_ACCESS, UserRoles.SPA_OFFICIAL_IN_CHARGE })
	@GetMapping(value = "/contacts")
	public Page<ContactModel> searchContacts(@RequestParam("q") String searchString,
			@PageableDefault(sort = { "name" }, size = 10000, direction = DESC) Pageable pageable) {
		try {
			return contactService.searchContacts(searchString, pageable);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred searching for contacts - " + e.getMessage(), e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException("Forbidden, could not access contacts module", e);
		}

	}

	/**
	 * Get {@link CompanyContactBlockModel} for contact with provided contactId from
	 * ContactBaseData module.
	 * 
	 * @param contactId contact identifier from ContactBaseData module
	 * @return {@link CompanyContactBlockModel} contact details
	 */
	@Secured({ UserRoles.SPA_ACCESS, UserRoles.SPA_OFFICIAL_IN_CHARGE })
	@GetMapping(value = "/contacts/{contactId}")
	public CompanyContactBlockModel getContactDetails(@PathVariable("contactId") String contactId) {
		try {
			Optional<CompanyContactBlockModel> oContact = contactService.getContactDetails(contactId, false);
			if (oContact.isPresent()) {
				return oContact.get();
			}
			throw new NotFoundException("Could not find contact with given id");
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred searching for contacts - " + e.getMessage(), e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException("Forbidden, could not access contacts module", e);
		}
	}

}
