/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;

import org.eclipse.openk.statementpublicaffairs.Constants;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestException;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalServerErrorException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.UnprocessableEntityException;
import org.eclipse.openk.statementpublicaffairs.exceptions.UnprocessableEntityServiceException;
import org.eclipse.openk.statementpublicaffairs.service.StatementProcessService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailSendReport;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailTransferAttachment;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ProcessHistoryModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTaskModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.WorkflowTaskVariableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.java.Log;

/**
 * REST controller provides end points to access the statement workflow.
 * 
 * @author Tobias Stummer
 *
 */
@Log
@RestController
public class StatementProcessController {

	private static final String STATEMENT_OR_TASK_DOES_NOT_EXIST = "Statement, or task does not exist";
	private static final String INVALID_REQUEST_PARAMETERS = "Invalid request parameters.";
	private static final String STATEMENT_DOES_NOT_EXIST = "Statement does not exist or currently no open user task with taskId for this available.";
	private static final String NOT_AUTHORIZED_TO_CLAIM_TASK = "Not authorized to claim task.";
	private static final String NOT_AUTHORIZED_TO_GET_TASKS = "Not authorized to get tasks.";
	private static final String NOT_AUTHORIZED_TO_GET_HISTORY = "Not authorized to get history.";
	private static final String NOT_AUTHORIZED_TO_UNCLAIM_TASK = "Not authorized to unclaim task.";
	private static final String NOT_AUTHORIZED_TO_COMPLETE_TASK = "Not authorized to complete task.";

	@Autowired
	private StatementProcessService statementProcessService;

	/**
	 * Current workflow tasks for statement with given statementId.
	 * 
	 * @param statementId Statement identifier
	 * @return list of current UserTasks
	 * 
	 * @throws NotFoundException            if statement with given identifier does
	 *                                      not exist
	 * @throws InternalServerErrorException if an error occurred when processing the
	 *                                      request.
	 * @throws ForbiddenException           if access is not authorised
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/process/statements/{statementId}/task")
	public List<StatementTaskModel> getStatementTask(@PathVariable final Long statementId) {
		try {
			Optional<List<StatementTaskModel>> statementTasks = statementProcessService
					.getCurrentStatementTasks(statementId);
			if (statementTasks.isPresent()) {
				return statementTasks.get();
			} else {
				throw new NotFoundException("Statement does not exist");
			}
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred processing the current statement task list request.",
					e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(NOT_AUTHORIZED_TO_GET_TASKS, e);
		}
	}

	/**
	 * Current workflow history for statement with given statementId.
	 * 
	 * @param statementId Statement identifier
	 * @return statement process history
	 * 
	 * @throws NotFoundException            if statement with given identifier does
	 *                                      not exist
	 * @throws InternalServerErrorException if an error occurred when processing the
	 *                                      request.
	 * @throws ForbiddenException           if access is not authorised
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "process/statements/{statementId}/history")
	public ProcessHistoryModel getStatementProcessHistory(@PathVariable final Long statementId) {
		try {
			Optional<ProcessHistoryModel> history = statementProcessService.getStatmentProcessHistory(statementId);
			if (history.isPresent()) {
				return history.get();
			} else {
				throw new NotFoundException(
						"Statement does not exist or currently no open user task for this available.");
			}
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred processing the statement history list request.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(NOT_AUTHORIZED_TO_GET_HISTORY, e);
		}
	}

	/**
	 * Process definition BPMN-Diagram XML.
	 * 
	 * @param statementId Statement identifier
	 * @return statement process workflow definition
	 * 
	 * @throws NotFoundException            if statement with given identifier does
	 *                                      not exist
	 * @throws InternalServerErrorException if an error occurred when processing the
	 *                                      request
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "process/statements/{statementId}/workflowmodel", produces = MediaType.APPLICATION_XML_VALUE)
	public String getProcessDiagramXML(@PathVariable final Long statementId) {
		try {
			Optional<String> xmlModel = statementProcessService.getStatementWorkflow(statementId);
			if (xmlModel.isPresent()) {
				return xmlModel.get();
			} else {
				throw new NotFoundException("Statement does not exist or no workflow model available.");
			}
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred processing the statement history list request.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Claim current workflow task with given taskId for statement with given
	 * statementId.
	 * 
	 * @param statementId Statement identifier
	 * @param taskId      Task identifier
	 * 
	 * @return statement task details
	 * 
	 * @throws NotFoundException            if task with with given taskId for
	 *                                      statement with given statementId does
	 *                                      not exist
	 * @throws InternalServerErrorException if an error occurred when processing the
	 *                                      request
	 * @throws ForbiddenException           if access is not authorised
	 * @throws BadRequestException          if invalid request parameters are
	 *                                      provided
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/task/{taskId}/claim")
	public StatementTaskModel claimStatementTask(@PathVariable final Long statementId,
			@PathVariable final String taskId) {
		try {
			Optional<StatementTaskModel> task = statementProcessService.claimStatementTask(statementId, taskId);
			if (task.isPresent()) {
				return task.get();
			} else {
				throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
			}
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred processing the statement task claim request.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(NOT_AUTHORIZED_TO_CLAIM_TASK, e);
		} catch (BadRequestServiceException e) {
			throw new BadRequestException(INVALID_REQUEST_PARAMETERS, e);
		}
	}

	/**
	 * Unclaim current workflow task with given taskId for statement with given
	 * statementId.
	 * 
	 * @param statementId Statement identifier
	 * @param taskId      Task identifier
	 * 
	 * @return statement task details
	 * 
	 * @throws NotFoundException            if task with with given taskId for
	 *                                      statement with given statementId does
	 *                                      not exist
	 * @throws InternalServerErrorException if an error occurred when processing the
	 *                                      request.
	 * @throws ForbiddenException           if access is not authorised
	 * @throws BadRequestException          if invalid request parameters are
	 *                                      provided
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/task/{taskId}/unclaim")
	public StatementTaskModel unClaimStatementTask(@PathVariable final Long statementId,
			@PathVariable final String taskId) {
		try {
			Optional<StatementTaskModel> task = statementProcessService.unClaimStatementTask(statementId, taskId);
			if (task.isPresent()) {
				return task.get();
			} else {
				throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
			}
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred processing the statement task unclaim request.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(NOT_AUTHORIZED_TO_UNCLAIM_TASK, e);
		} catch (BadRequestServiceException e) {
			throw new BadRequestException(INVALID_REQUEST_PARAMETERS, e);
		}
	}

	/**
	 * Complete current workflow task with given taskId for statement with given
	 * statementId.
	 * 
	 * @param statementId           Statement identifier
	 * @param taskId                Task identifier
	 * @param completeTaskVariables Map of workflow variables required to complete
	 *                              the task
	 * 
	 * @return response 204 if successful. Otherwise response 400.
	 * 
	 * @throws NotFoundException            if task with with given taskId for
	 *                                      statement with given statementId does
	 *                                      not exist
	 * @throws InternalServerErrorException if an error occurred when processing the
	 *                                      request.
	 * @throws ForbiddenException           if access is not authorised
	 * @throws BadRequestException          if invalid request parameters are
	 *                                      provided
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/task/{taskId}/complete")
	public ResponseEntity<Object> completeStatementTask(@PathVariable final Long statementId,
			@PathVariable final String taskId,
			@RequestBody Map<String, WorkflowTaskVariableModel> completeTaskVariables) {
		try {
			Optional<Boolean> task = statementProcessService.completeStatementTask(statementId, taskId,
					completeTaskVariables);
			if (task.isPresent()) {
				if (Boolean.TRUE.equals(task.get())) {
					return ResponseEntity.status(204).build();
				} else {
					return ResponseEntity.status(400).build();
				}
			} else {
				throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
			}
		} catch (BadRequestServiceException e) {
			throw new BadRequestException(INVALID_REQUEST_PARAMETERS, e);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred processing the statement task complete request.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(NOT_AUTHORIZED_TO_COMPLETE_TASK, e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST + " - " + e.getMessage(), e);
		}
	}

	/**
	 * Update statement with given statementId with the provided statement model
	 * 
	 * @param statementId Statement identifier
	 * @param taskId      Task identifier
	 * @param statement   updated statement
	 * @return Updated statement model if successful.
	 * 
	 * @throws InternalServerErrorException if error occurred updating the statement
	 * @throws BadRequestException          if provided statement is invalid
	 * @throws ForbiddenServiceException    if manipulation is not allowed
	 * @throws NotFoundException            if statement with provided statementId
	 *                                      could not be found
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/task/{taskId}/statement")
	public StatementDetailsModel updateStatement(@PathVariable final Long statementId,
			@PathVariable final String taskId, @RequestBody StatementDetailsModel statement) {
		try {
			Optional<StatementDetailsModel> newstatement = statementProcessService.updateStatement(statementId, taskId,
					statement);
			if (newstatement.isPresent()) {
				return newstatement.get();
			}
			throw new InternalServerErrorException("Error occurred processing the statement update request.");
		} catch (BadRequestServiceException e) {
			throw new BadRequestException(INVALID_REQUEST_PARAMETERS, e);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred processing the statement update request.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(NOT_AUTHORIZED_TO_COMPLETE_TASK, e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST, e);
		} catch (UnprocessableEntityServiceException e) {
			throw new UnprocessableEntityException("Invalid contact id. - " + e.getMessage());
		}
	}

	/**
	 * Set tags to a specific statement attachment.
	 * 
	 * @param statementId  statement identifier
	 * @param taskId       current task identifier, required for authorization
	 * @param attachmentId attachment identifier
	 * @param tagIds       Set of taskIds to assign
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/task/{taskId}/attachments/{attachmentId}/tags")
	public void setTags(@PathVariable final Long statementId, @PathVariable final String taskId,
			@PathVariable final Long attachmentId, @RequestBody Set<String> tagIds) {
		try {
			statementProcessService.setAttachmentTags(statementId, taskId, attachmentId, tagIds);
		} catch (BadRequestServiceException e) {
			throw new BadRequestException(INVALID_REQUEST_PARAMETERS, e);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException(
					"Error occurred processing the set statement attachment tags request.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(NOT_AUTHORIZED_TO_COMPLETE_TASK, e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException("Statement, task or attachment does not exist", e);
		}

	}

	/**
	 * Send response mail and complete the task.
	 * 
	 * @param statementId statement identifier
	 * @param taskId      task identifier
	 * @return {@link MailSendReport}
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/task/{taskId}/mailandcomplete")
	public ResponseEntity<Object> dispatchStatementResponse(@PathVariable final Long statementId,
			@PathVariable final String taskId) {
		try {

			MailSendReport mailReport = statementProcessService.dispatchStatementResponseAndCompleteTask(statementId,
					taskId);
			return new ResponseEntity<>(mailReport,
					Boolean.TRUE.equals(mailReport.getSuccessful()) ? HttpStatus.OK : HttpStatus.FAILED_DEPENDENCY);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException(
					"Error occurred processing the set mail dispatch request. - " + e.getMessage(), e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException("Mail dispatch is forbidden", e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_OR_TASK_DOES_NOT_EXIST, e);
		} catch (BadRequestServiceException e) {
			throw new BadRequestException("Invalid request", e);
		}
	}

	/**
	 * Transfer mail body as text attachment from the mail assigned to the specific
	 * statement and assign the corresponding tag identifiers.
	 * 
	 * @param statementId statement identifier
	 * @param taskId      task identifier
	 * @return {@link AttachmentModel} - Attachment details
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/task/{taskId}/transfermailtext")
	public AttachmentModel transferMailText(@PathVariable final Long statementId, @PathVariable final String taskId) {
		try {
			return statementProcessService.transferMailText(statementId, taskId);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException(
					"Error occurred processing the transfer mail text request. - " + e.getMessage(), e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException("Transfer mail text is forbidden", e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_OR_TASK_DOES_NOT_EXIST, e);
		}
	}

	/**
	 * Transder mail attachments from the mail assigned to the specific statement
	 * and assign the corresponding tag identifiers.
	 * 
	 * @param statementId         statement identifier
	 * @param taskId              task identifier
	 * @param transferAttachments List of mail-attachments to transfer
	 * @return List of transfered {@link AttachmentModel} - Attachment details
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/task/{taskId}/transfermailattachments")
	public List<AttachmentModel> transferMailAttachments(@PathVariable final Long statementId,
			@PathVariable final String taskId, @RequestBody List<MailTransferAttachment> transferAttachments) {
		try {
			return statementProcessService.transferMailAttachments(statementId, taskId, transferAttachments);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException(
					"Error occurred processing the transfer mail attachments request. - " + e.getMessage(), e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException("Transfer mail attachments is forbidden", e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_OR_TASK_DOES_NOT_EXIST, e);
		}
	}

	/**
	 * Attachment POST endpoint creates a new attachment for the specified Statement
	 * with the provided file content.
	 * 
	 * @param statementId Statement Identifier
	 * @param files       list of uploaded files
	 * 
	 * @throws BadRequestException if provided attachment data is invalid
	 * @throws NotFoundException   if statement could not be found.
	 * @return AttachmentModel of new created Attachment if successful.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "process/statements/{statementId}/task/{taskId}/attachments")
	public AttachmentModel createAttachement(@PathVariable Long statementId, @PathVariable String taskId,
			@RequestParam(value = "tagId", required = false) Set<String> tagIds,
			@RequestParam(value = "attachment", required = true) List<MultipartFile> files) {
		if (files.isEmpty()) {
			throw (new BadRequestException(Constants.NO_ATTACHMENT_FILE_SPECIFIED));
		}
		if (files.size() > 1) {
			throw (new BadRequestException(Constants.MORE_THAN_ONE_ATTACHMENT_FILE));
		}
		MultipartFile file = files.get(0);
		if (file.getContentType() == null) {
			throw new BadRequestException("Invalid file content-type");
		}
		InputStream is;
		try {
			is = file.getInputStream();
		} catch (IOException e) {
			log.log(Level.WARNING, "Attachment Upload - IOException: " + e.getMessage() + " - " + e.getStackTrace());
			throw (new BadRequestException(e.getMessage()));
		}
		if (tagIds == null) {
			tagIds = new HashSet<>();
		}

		try {
			Optional<AttachmentModel> oAttachment = statementProcessService.createStatementAttachmentToModel(
					statementId, taskId, file.getOriginalFilename(), file.getContentType(), is, file.getSize(), tagIds);
			if (oAttachment.isPresent()) {
				return oAttachment.get();
			}
			throw new NotFoundException("Error occurred processing the attachment upload request.");
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred processing the attachment upload request.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException("Not authorized to upload attachment", e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException("Not found. Statement or task could not be found", e);
		}
	}

	/**
	 * Attachment DELETE endpoint deletes the requested Attachment by statementId
	 * and attachmentId.
	 * 
	 * @param statmentId   Statement Identifier
	 * @param attachmentId Attachment Identifier
	 * 
	 * @return OK_NO_CONTENT if successful.
	 * @throws NotFoundException if attachment could not be found.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@DeleteMapping(value = "/process/statements/{statementId}/task/{taskId}/attachments/{attachmentId}")
	public ResponseEntity<Object> deleteAttachement(@PathVariable Long statementId, @PathVariable String taskId,
			@PathVariable Long attachmentId) {
		try {
			statementProcessService.deleteStatementAttachments(statementId, taskId, attachmentId);
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred processing the attachment delete request.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException("Not authorized to delete attachment", e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException("Not found. Statement or task could not be found", e);
		}
	}

}
