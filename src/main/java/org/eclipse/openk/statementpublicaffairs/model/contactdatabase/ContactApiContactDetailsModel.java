/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.contactdatabase;

import lombok.Data;

@Data
public class ContactApiContactDetailsModel {

	private Boolean anonymized;
	private String companyId;
	private String companyName;
	private String companyType;
	private String contactType;
	private String department;
	private String email;
	private String firstName;
	private Long fkContactId;
	private Long id;
	private String lastName;
	private String mainAddress;
	private String name;
	private String note;
	private String personType;
	private String personTypeUuid;
	private String salutationType;
	private String salutationUuid;
	private String searchfield;
	private String uuid;
}
