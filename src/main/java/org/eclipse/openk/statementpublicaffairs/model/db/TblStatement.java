/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.db;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Data;

/**
 * @author Tobias Stummer
 *
 */

@Entity
@Data
public class TblStatement {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false)
	private Long id;
	private String businessKey;
	private LocalDate dueDate;
	private LocalDate receiptDate;
	private LocalDate creationDate;
	private Boolean finished;
	private LocalDate finishedDate;
	private String title;
	private String city;
	private String district;
	private String contactDbId;
	private String sourceMailId;
	private String customerReference;

	@OneToMany(mappedBy = "statement", fetch = FetchType.LAZY)
	private List<TblAttachment> attachments;

	@ManyToOne()
	@JoinColumn(name = "type_id")
	private TblStatementtype type;

	@OneToMany(mappedBy = "statement", fetch = FetchType.LAZY)
	private List<TblStatementEditLog> editLog;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department_structure_id")
	private TblDepartmentstructure departmentstructure;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "statement")
	private TblWorkflowdata workflowdata;

}
