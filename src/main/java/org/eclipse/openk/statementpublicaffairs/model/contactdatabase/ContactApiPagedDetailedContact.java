package org.eclipse.openk.statementpublicaffairs.model.contactdatabase;

import java.util.List;

import lombok.Data;

@Data
public class ContactApiPagedDetailedContact {
	private List<ContactApiDetailedContact> content;
	private Boolean empty;
	private Boolean first;
	private Boolean last;
	private Long number;
	private Long numberOfElements;
	private ContactApiPagableModel pageable;
	private Long size;
	private ContactApiSortModel sort;
	private Long totalElements;
	private Long totalPages;	

}
