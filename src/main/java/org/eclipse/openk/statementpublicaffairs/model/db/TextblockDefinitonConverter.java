/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.db;

import java.io.IOException;

import javax.persistence.AttributeConverter;

import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.java.Log;

@Log
public class TextblockDefinitonConverter implements AttributeConverter<TextblockDefinition, String> {

    private final ObjectMapper objectMapper = new ObjectMapper();
    
	@Override
	public String convertToDatabaseColumn(TextblockDefinition textblockDefinitionModel) {
        String customerInfoJson = null;
        try {
            customerInfoJson = objectMapper.writeValueAsString(textblockDefinitionModel);
        } catch (final JsonProcessingException e) {
            log.warning("JSON writing error " + e.getMessage());
        }

        return customerInfoJson;
	}

	@Override
	public TextblockDefinition convertToEntityAttribute(String textblockDefinitionJson) {
		TextblockDefinition textblockDefinitionModel = null;
        try {
        	textblockDefinitionModel = objectMapper.readValue(textblockDefinitionJson,  TextblockDefinition.class);
        } catch (final IOException e) {
            log.warning("JSON reading error " + e.getMessage());
        }
        return textblockDefinitionModel;
	}


}
