/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.contactdatabase;

import lombok.Data;

@Data
public class ContactApiCompanyContactPersonModel {
	private String companyContactId;
	private String companyHrNumber;
	private String companyName;
	private String companyType;
	private Boolean contactAnonymized;
	private String contactId;
	private String contactNote;
	private String contactType;
	private String email;
	private String firstName;
	private String lastName;
	private String personType;
	private String personTypeId;
	private String salutationId;
	private String salutationType;
	private String title;

}
