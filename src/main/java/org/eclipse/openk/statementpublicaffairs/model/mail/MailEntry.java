/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.mail;

import java.util.List;

import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;

import lombok.Data;

@Data
public class MailEntry {
	private String identifier;
	private String subject;
	private String date;
	private String from;
	private String textPlain;
	private String textHtml;
	private List<AttachmentModel> attachments;
}
