/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class TextGroup implements ITextEntity{
	
	private FontModification fontModification;
	
	private List<String> words;
	
	public static TextGroup newlineTextGroup() {
		TextGroup group = new TextGroup();
		group.setFontModification(FontModification.Normal);
		List<String> words = new ArrayList<>();
		words.add("\n");
		group.setWords(words);
		return group;
	}
	
	public static TextGroup textGroupOf(String text, FontModification fontMod) {
		TextGroup group = new TextGroup();
		group.setFontModification(fontMod);
		List<String> words = new ArrayList<>();
		String[] tokens = text.split(" ");
		for (String token : tokens) {
			words.add(token);
		}
		group.setWords(words);
		return group;
	}
}
