package org.eclipse.openk.statementpublicaffairs.model.db;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

import lombok.Data;

@Entity
@Data
@IdClass(TblStatement2parentId.class)
public class TblStatement2parent {

	@Id
	private Long statement_id;

	@Id
	private Long parent_id;

}
