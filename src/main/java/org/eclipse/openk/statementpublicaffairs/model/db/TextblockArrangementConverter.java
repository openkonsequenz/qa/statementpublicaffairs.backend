/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.db;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.persistence.AttributeConverter;

import org.eclipse.openk.statementpublicaffairs.model.Textblock;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.java.Log;

@Log
public class TextblockArrangementConverter implements AttributeConverter<List<Textblock> , String> {

    private final ObjectMapper objectMapper = new ObjectMapper();
    
	@Override
	public String convertToDatabaseColumn(List<Textblock>  textArrangementModel) {
        String textArrangementJson = null;
        try {
        	textArrangementJson = objectMapper.writeValueAsString(textArrangementModel);
        } catch (final JsonProcessingException e) {
            log.warning("JSON writing error " + e.getMessage());
        }

        return textArrangementJson;
	}

	@Override
	public List<Textblock>  convertToEntityAttribute(String textArrangementJson) {
		List<Textblock>  textArrangementModel = null;
        try {
			Textblock arrangements[] = objectMapper.readValue(textArrangementJson, Textblock[].class);
			textArrangementModel =  Arrays.asList(arrangements);
        } catch (final IOException e) {
            log.warning("JSON reading error " + e.getMessage());
        }
        return textArrangementModel;
	}


}
