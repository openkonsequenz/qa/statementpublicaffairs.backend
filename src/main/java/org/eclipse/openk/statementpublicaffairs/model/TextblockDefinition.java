/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class TextblockDefinition {


	private Map<String, List<String>> selects;
	private List<TextblockGroup> groups;
	private List<TextblockGroup> negativeGroups;

	public TextblockDefinition() {
		selects = new HashMap<>();
		groups = new ArrayList<>();
		negativeGroups = new ArrayList<>();
	}
	
	public boolean valid() {
		if (selects == null || groups == null || negativeGroups == null) {
			return false;
		}
		for (List<String> entry : selects.values()) {
			if (entry == null) {
				return false;
			}
		}
		for (TextblockGroup group : groups) {
			if (group == null || !group.valid()) {
				return false;
			}
		}

		for (TextblockGroup group : groups) {
			if (group == null || !group.valid()) {
				return false;
			}
		}

		return true;
	}
	
}
