/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.db;

import java.io.IOException;
import java.util.Map;

import javax.persistence.AttributeConverter;

import org.eclipse.openk.statementpublicaffairs.viewmodel.DistrictDepartmentsModel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.java.Log;

@Log
public class DepartmentDefinitonConverter implements AttributeConverter<Map<String, DistrictDepartmentsModel>, String> {

    private final ObjectMapper objectMapper = new ObjectMapper();
    
	@Override
	public String convertToDatabaseColumn(Map<String, DistrictDepartmentsModel> statementDepartmentsModel) {
        String customerInfoJson = null;
        try {
            customerInfoJson = objectMapper.writeValueAsString(statementDepartmentsModel);
        } catch (final JsonProcessingException e) {
            log.warning("JSON writing error " + e.getMessage());
        }

        return customerInfoJson;
	}

	@Override
	public Map<String, DistrictDepartmentsModel> convertToEntityAttribute(String statementDepartmentsJson) {
		Map<String, DistrictDepartmentsModel> statementDepartmentsModel = null;
		TypeReference<Map<String, DistrictDepartmentsModel>> typeRef = new TypeReference<Map<String, DistrictDepartmentsModel>>() {};
        try {
        	statementDepartmentsModel = objectMapper.readValue(statementDepartmentsJson,  typeRef);
        } catch (final IOException e) {
            log.warning("JSON reading error " + e.getMessage());
        }
        return statementDepartmentsModel;
	}


}
