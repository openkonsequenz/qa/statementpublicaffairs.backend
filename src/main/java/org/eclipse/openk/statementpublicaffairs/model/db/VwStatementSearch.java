/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.db;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.data.annotation.Immutable;

import lombok.Data;

@Immutable
@Data
@Entity
public class VwStatementSearch {

	@Id
	private Long id;
	private String title;
	private String city;
	private String district;
	private String businessKey;
	private LocalDate dueDate;
	private LocalDate receiptDate;
	private LocalDate creationDate;
	private Boolean finished;
	private String searchfield;
	private String customerReference;
	private Long typeId;
	private String contactDbId;
	private String sourceMailId;

}
