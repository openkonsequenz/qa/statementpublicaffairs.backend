package org.eclipse.openk.statementpublicaffairs.model.db;

import java.io.Serializable;

public class TblStatement2parentId implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long statement_id;
	private Long parent_id;
}
