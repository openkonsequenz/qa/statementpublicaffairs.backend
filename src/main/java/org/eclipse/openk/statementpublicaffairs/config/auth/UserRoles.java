/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.config.auth;

import java.util.HashSet;
import java.util.Set;

/**
 * Statement module specific AuthNAuth user roles.
 * 
 * @author Tobias Stummer
 *
 */
public class UserRoles {

	public static final String SPA_ACCESS = "ROLE_SPA_ACCESS";
	public static final String SPA_OFFICIAL_IN_CHARGE = "ROLE_SPA_OFFICIAL_IN_CHARGE";
	public static final String SPA_APPROVER = "ROLE_SPA_APPROVER";
	public static final String SPA_DIVISION_MEMBER = "ROLE_SPA_DIVISION_MEMBER";
	public static final String SPA_CUSTOMER = "ROLE_SPA_CUSTOMER";
	public static final String SPA_ADMIN = "ROLE_SPA_ADMIN";

	public static String keyCloakRoleIdof(String role) {
		return role.replace("ROLE_", "").toLowerCase();
	}

	public static String userRoleOfKeyCloakRoleId(String role) {
		return ("ROLE_" + role).toUpperCase();
	}

	public static Set<String> allRoles() {
		Set<String> allRoles = new HashSet<>();
		allRoles.add(SPA_ACCESS);
		allRoles.add(SPA_ADMIN);
		allRoles.add(SPA_APPROVER);
		allRoles.add(SPA_CUSTOMER);
		allRoles.add(SPA_DIVISION_MEMBER);
		allRoles.add(SPA_OFFICIAL_IN_CHARGE);
		return allRoles;
	}

	private UserRoles() {
	}

}
