/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.openfeign.EnableFeignClients;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Service entry class.
 * 
 * @author Tobias Stummer
 *
 */
@SpringBootApplication
@EnableSwagger2
@EnableFeignClients
public class StatementPublicAffairsApplication extends SpringBootServletInitializer {

    /**
     * Service entry main.
     * 
     * @param args parameters are ignored
     */
    public static void main(String[] args) {
        SpringApplication.run(StatementPublicAffairsApplication.class, new String[0]);
    }

}
