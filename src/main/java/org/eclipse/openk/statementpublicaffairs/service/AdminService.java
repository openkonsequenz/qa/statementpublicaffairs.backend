/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.FailedDependencyServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.DepartmentModel;
import org.eclipse.openk.statementpublicaffairs.model.Pair;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;
import org.eclipse.openk.statementpublicaffairs.model.UserAdminModel;
import org.eclipse.openk.statementpublicaffairs.model.UserAdminSettingsModel;
import org.eclipse.openk.statementpublicaffairs.model.UserModel;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartmentstructure;
import org.eclipse.openk.statementpublicaffairs.model.db.TblTextblockdefinition;
import org.eclipse.openk.statementpublicaffairs.model.db.TblUser;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentstructureRepository;
import org.eclipse.openk.statementpublicaffairs.repository.TextblockdefinitionRepository;
import org.eclipse.openk.statementpublicaffairs.repository.UserRepository;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DistrictDepartmentsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private DepartmentstructureRepository departmentStructureRepository;

	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private TextblockdefinitionRepository textblockdefinitionRepository;
	
	@Autowired
	private UserRepository userRepository;

	public void syncUsers() throws InternalErrorServiceException, ForbiddenServiceException {
		usersService.syncKeycloakUsers();
	}

	public Map<String, DistrictDepartmentsModel> getDepartmentStructure() {
		List<TblDepartmentstructure> latest = departmentStructureRepository.getLatestDepartmentStructure();
		if (latest.isEmpty()) {
			return new HashMap<>();
		}
		return latest.get(0).getDefinition();
	}

	public void setDepartmentStructure(Map<String, DistrictDepartmentsModel> departmentStructure)
			throws BadRequestServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_ADMIN_SET_DEPARTMENT_STRUCTURE);
		validateDepartmentStructure(departmentStructure);
		TblDepartmentstructure newDepartmentStructure = new TblDepartmentstructure();
		newDepartmentStructure.setDefinition(departmentStructure);

		Set<Pair<String, String>> departmentPairs = createDepartmentPairs(departmentStructure);
		Iterable<TblDepartment> currentDepartments = departmentRepository.findAll();
		for (TblDepartment department : currentDepartments) {
			Pair<String, String> depPair = new Pair<>();
			depPair.setA(department.getDepartmentgroup());
			depPair.setB(department.getName());
			if (departmentPairs.contains(depPair)) {
				departmentPairs.remove(depPair);
			} else {
				// remove users
				department.getUsers().clear();
				departmentRepository.save(department);
			}
		}
		for (Pair<String, String> departmentPair : departmentPairs) {
			TblDepartment dep = new TblDepartment();
			dep.setDepartmentgroup(departmentPair.getA());
			dep.setName(departmentPair.getB());
			departmentRepository.save(dep);
		}
		departmentStructureRepository.save(newDepartmentStructure);
	}

	private Set<Pair<String, String>> createDepartmentPairs(Map<String, DistrictDepartmentsModel> departmentStructure) {
		Set<Pair<String, String>> pairs = new HashSet<>();
		for (DistrictDepartmentsModel ddm : departmentStructure.values()) {
			for (Entry<String, Set<String>> entry : ddm.getDepartments().entrySet()) {
				String group = entry.getKey();
				for (String name : entry.getValue()) {
					Pair<String, String> departmentPair = new Pair<>();
					departmentPair.setA(group);
					departmentPair.setB(name);
					pairs.add(departmentPair);
				}
			}
		}
		return pairs;
	}

	private void validateDepartmentStructure(Map<String, DistrictDepartmentsModel> departmentStructure)
			throws BadRequestServiceException {

		if (departmentStructure == null || departmentStructure.isEmpty()) {
			throw new BadRequestServiceException("Empty department structure");
		}

		for (Entry<String, DistrictDepartmentsModel> entry : departmentStructure.entrySet()) {
			String key = entry.getKey();
			DistrictDepartmentsModel model = entry.getValue();
			if (key == null || !key.matches("^[^#]+#[^#]+$")) {
				throw new BadRequestServiceException("Invalid key city district key " + entry.getKey());
			}

			if (model == null || !model.validate()) {
				throw new BadRequestServiceException("Invalid district departments model for city district key " + key);
			}
		}

	}

	public TextblockDefinition getTextblockDefinition() {
		List<TblTextblockdefinition> defs = textblockdefinitionRepository.getLatestTextblockdefinition();
		if (defs.isEmpty()) {
			return new TextblockDefinition();
		}
		return defs.get(0).getDefinition();
	}

	public void setTextblockDefinition(TextblockDefinition textblockDefinition)
			throws ForbiddenServiceException, BadRequestServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_ADMIN_SET_TEXTBLOCK_DEFINITION);
		if (textblockDefinition == null || !textblockDefinition.valid()) {
			throw new BadRequestServiceException("Invalid TextblockDefinition");
		}
		TblTextblockdefinition def = new TblTextblockdefinition();
		def.setDefinition(textblockDefinition);
		textblockdefinitionRepository.save(def);
	}

	public List<UserAdminModel> getAllUsers() throws InternalErrorServiceException {
		Set<String> allRoles = UserRoles.allRoles();
		List<UserAdminModel> users = new ArrayList<>();
		
		Iterable<TblUser> tusers = userRepository.findAll();
		for(UserModel user: usersService.getUsersWithRole(UserRoles.SPA_ACCESS)) {
			TblUser tUser = filterUserName(tusers, user.getUsername());
			if (tUser == null) {
				continue;
			}
			UserAdminModel aUser = new UserAdminModel();
			aUser.setId(tUser.getId());
			aUser.setFirstName(tUser.getFirstName());
			aUser.setLastName(tUser.getLastName());
			aUser.setUserName(tUser.getUsername());
			aUser.setRoles(user.getRoles().stream().filter(allRoles::contains).collect(Collectors.toSet()));
			
			UserAdminSettingsModel settings = new UserAdminSettingsModel();
			settings.setEmail(tUser.getEmailAddress());
			TblDepartment dep = tUser.getDepartment(); 
			if (dep != null) {
				DepartmentModel department = new DepartmentModel();
				department.setGroup(dep.getDepartmentgroup());
				department.setName(dep.getName());
				settings.setDepartment(department);
			}
			aUser.setSettings(settings);
			users.add(aUser);
		}
		return users;
	}

	private TblUser filterUserName(Iterable<TblUser> users, String userName) {
		for (TblUser user : users) {
			if (userName.equals(user.getUsername())) {
				return user;
			}
		}
		return null;
	}

	public void setUserSettings(Long userId, UserAdminSettingsModel settings) throws BadRequestServiceException, FailedDependencyServiceException, NotFoundServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_ADMIN_SET_USER_SETTINGS);
		if (settings == null) {
			throw new BadRequestServiceException("Invalid settings format");
		}
		if (settings.getEmail() != null && !validEmailAddress(settings.getEmail())) {
			throw new BadRequestServiceException("Invalid email address format");
		}
		Optional<TblUser> oUser = userRepository.findById(userId);
		if (!oUser.isPresent()) {
			throw new NotFoundServiceException("Could not find user with id " + userId);
		}
		
		TblUser user = oUser.get();
		user.setEmailAddress(settings.getEmail());
		DepartmentModel depMod = settings.getDepartment();
		if (depMod == null) {
			user.setDepartment(null);
		} else {
			boolean found = false;
			for (TblDepartment dep : departmentRepository.findAll()) {
				if (dep.getDepartmentgroup().equals(depMod.getGroup()) && dep.getName().equals(depMod.getName())) {
					user.setDepartment(dep);
					found = true;
				}
			}
			if (!found) {
				throw new FailedDependencyServiceException("Could not find department");
			}
		}
		userRepository.save(user);
		
	}

	private boolean validEmailAddress(String email) {
		return email.matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");
	}

}
