/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.model.TaskInfo;
import org.eclipse.openk.statementpublicaffairs.model.Textblock;
import org.eclipse.openk.statementpublicaffairs.model.Textblock.TextblockType;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblTextblockdefinition;
import org.eclipse.openk.statementpublicaffairs.model.db.TblWorkflowdata;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.WorkflowDataRepository;
import org.eclipse.openk.statementpublicaffairs.service.compile.TextCompileUtil;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.TextConfiguration;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ValidationError;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TextblockService {

	private static final String INVALID_TEXT_ARRANGEMENT_FORMAT_TEXT_BLOCK = "Invalid text arrangement format. Text block ";

	private static final String STATEMENT_WITH_GIVEN_IDENTIFIER_COULD_NOT_BE_FOUND = "Statement with given identifier could not be found.";

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private StatementProcessService statementProcessService;

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Autowired
	private StatementCompileService statementCompileService;

	@Autowired
	private StatementService statementService;

	@Autowired
	private ContactService contactService;

	@Autowired
	UsersService usersService; 

	@Autowired
	private WorkflowDataRepository workflowDataRepository;

	@Value("${statement.compile.dateFormatPattern:\"dd.MM.yyyy\"}")
	private String dateFormatPattern;

	public Optional<TextConfiguration> getTextblockConfiguration(Long statementId)
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);

		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (!oStatement.isPresent()) {
			return Optional.empty();
		}

		TblStatement statement = oStatement.get();
		TblWorkflowdata wfd = statement.getWorkflowdata();
		if (wfd == null) {
			throw new NotFoundServiceException("Workflowdata is not set.");
		}

		TblTextblockdefinition tbd = wfd.getTextBlockDefinition();
		if (tbd == null) {
			throw new InternalErrorServiceException("Error in statement. No TextblockDefinition assigned.");
		}

		TextConfiguration textConfiguration = new TextConfiguration();
		textConfiguration.setConfiguration(tbd.getDefinition());
		textConfiguration.setReplacements(getReplacements(statement));

		return Optional.of(textConfiguration);

	}

	protected void putReplacement(Map<String, String> replacements, String key, String value, boolean setBlankIfNull) {
		if (value == null) {
			if (setBlankIfNull) {
				replacements.put(key, "");
			}
		} else {
			replacements.put(key, value);
		}
	}

	protected Map<String, String> getReplacements(TblStatement statement)
			throws NotFoundServiceException, ForbiddenServiceException, InternalErrorServiceException {
		Map<String, String> replacements = new HashMap<>();

		replacements.put("id", statement.getId().toString());
		replacements.put("title", statement.getTitle());

		Optional<String> dueDate = TypeConversion.dateStringOfLocalDate(statement.getDueDate(), dateFormatPattern);
		if (dueDate.isPresent()) {
			replacements.put("dueDate", dueDate.get());
		}

		Optional<String> receiptDate = TypeConversion.dateStringOfLocalDate(statement.getReceiptDate(), dateFormatPattern);
		if (receiptDate.isPresent()) {
			replacements.put("receiptDate", receiptDate.get());
		}
		
		Optional<String> creationDate = TypeConversion.dateStringOfLocalDate(statement.getCreationDate(), dateFormatPattern);
		if (creationDate.isPresent()) {
			replacements.put("creationDate", creationDate.get());
		}

		replacements.put("customerReference", statement.getCustomerReference() == null ? "" : statement.getCustomerReference());
		
		replacements.put("city", statement.getCity());
		replacements.put("district", statement.getDistrict());
		replacements.put("type", statement.getType().getName());
		Map<String, List<String>> sectors = statementService.getAllSectors(statement.getId());
		String sectorkey = statement.getCity() + "#" + statement.getDistrict();
		if (sectors.containsKey(sectorkey)) {
			replacements.put("sectors", String.join(", ", sectors.get(sectorkey)));
		}

		Optional<CompanyContactBlockModel> oContactDetails;
		oContactDetails = contactService.getContactDetails(statement.getContactDbId(), true);
		if (oContactDetails.isPresent()) {
			CompanyContactBlockModel contact = oContactDetails.get();
			putReplacement(replacements, "c-community", contact.getCommunity(), false);
			putReplacement(replacements, "c-communitySuffix", contact.getCommunitySuffix(), true);
			putReplacement(replacements, "c-company", contact.getCompany(), false);
			putReplacement(replacements, "c-email", contact.getEmail(), false);
			putReplacement(replacements, "c-firstName", contact.getFirstName(), false);
			putReplacement(replacements, "c-houseNumber", contact.getHouseNumber(), false);
			putReplacement(replacements, "c-lastName", contact.getLastName(), false);
			putReplacement(replacements, "c-postCode", contact.getPostCode(), false);
			putReplacement(replacements, "c-salutation", contact.getSalutation(), true);
			putReplacement(replacements, "c-street", contact.getStreet(), false);
			putReplacement(replacements, "c-title", contact.getTitle(), true);
		}

		Optional<String> currentDate = TypeConversion.dateStringOfLocalDate(LocalDate.now(), dateFormatPattern);
		if (currentDate.isPresent()) {
			putReplacement(replacements, "b-currentDate", currentDate.get(), false);
		}

		return replacements;
	}

	public Optional<List<Textblock> > getTextArrangement(Long statementId) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);

		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (!oStatement.isPresent()) {
			return Optional.empty();
		}
		TblStatement statement = oStatement.get();
		TblWorkflowdata wfd = statement.getWorkflowdata();
		if (wfd == null) {
			return Optional.of(new ArrayList<>());
		}

		List<Textblock>  arrangement = wfd.getDraft();
		if (arrangement == null) {
			return Optional.of(new ArrayList<>());
		}
		return Optional.of(arrangement);
	}

	protected void verifyTextArrangement(List<Textblock> blocks, Long statementId)
			throws BadRequestServiceException, NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {
		if (blocks == null) {
			throw new BadRequestServiceException("Invalid text arrangement format. Provided text arrangement is null.");
		}
		Optional<TextConfiguration> oTextblockConfiguration = getTextblockConfiguration(statementId);
		if (!oTextblockConfiguration.isPresent()) {
			throw new NotFoundServiceException("Statement could not be found");
		}
		TextblockDefinition textblockDefinition = oTextblockConfiguration.get().getConfiguration();
		for (int i =0; i< blocks.size(); i++) {
			Textblock block = blocks.get(i);
			if (block == null) {
				throw new BadRequestServiceException(INVALID_TEXT_ARRANGEMENT_FORMAT_TEXT_BLOCK + i + " is null.");
			}
			if (block.getType() == null) {
				throw new BadRequestServiceException(INVALID_TEXT_ARRANGEMENT_FORMAT_TEXT_BLOCK + i + " has invalid type parameter");
			}
			if (block.getType() == TextblockType.block && TextCompileUtil.getTextblockDefForTextblockId(textblockDefinition, block.getTextblockId()) == null) {
				throw new BadRequestServiceException(INVALID_TEXT_ARRANGEMENT_FORMAT_TEXT_BLOCK + i + " does not have a valid textblockId");
			}
			if (block.getType() == TextblockType.text && block.getReplacement() == null) {
				throw new BadRequestServiceException(INVALID_TEXT_ARRANGEMENT_FORMAT_TEXT_BLOCK + i + " of type text does not have a replacement text");
			}
		}
	
	}

	public void setTextArrangement(Long statementId, String taskId, List<Textblock> textArrangement)
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException,
			NotFoundServiceException {
		Optional<TaskInfo> ti = statementProcessService.getTaskInfo(statementId, taskId);
		if (ti.isPresent()) {
			authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
			String constraint = usersService.userOfRequiredDivisionsOfStatement(statementId, ti.get().getAssignee()) == null ? AuthorizationRuleActions.C_IS_CLAIMED_BY_USER : AuthorizationRuleActions.C_IS_REQ_DIVISION_MEMBER;

			authorizationService.authorize(ti.get().getTaskDefinitionKey(), AuthorizationRuleActions.A_SET_TEXTARRANGEMENT, constraint);
		}

		verifyTextArrangement(textArrangement, statementId);

		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (!oStatement.isPresent()) {
			throw new NotFoundServiceException(STATEMENT_WITH_GIVEN_IDENTIFIER_COULD_NOT_BE_FOUND);
		}
		TblStatement statement = oStatement.get();
		TblWorkflowdata wfd = statement.getWorkflowdata();
		if (wfd == null) {
			throw new NotFoundServiceException("Workflowdata is not set.");
		}

		wfd.setDraft(textArrangement);

		workflowDataRepository.save(wfd);
	}

	public ValidationResult validate(Long statementId, List<Textblock>  textArrangement) throws InternalErrorServiceException, NotFoundServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (!oStatement.isPresent()) {
			throw new NotFoundServiceException(STATEMENT_WITH_GIVEN_IDENTIFIER_COULD_NOT_BE_FOUND);
		}
		Optional<TextConfiguration> oTextblockConfiguration = getTextblockConfiguration(statementId);
		if (!oTextblockConfiguration.isPresent()) {
			throw new InternalErrorServiceException("Could not get text block configuration");
		}
	
		TextConfiguration textConfiguration = oTextblockConfiguration.get();
		ValidationResult result = new ValidationResult();
		List<ValidationError> errors = new ArrayList<>();
		for (int i =0; i < textArrangement.size(); i++) {
			Textblock block = textArrangement.get(i);
			ValidationError validationError = new ValidationError();
			validationError.setArrangementId(i);
			String text;
			switch (block.getType()) {
			case text:
				text = TextCompileUtil.fillPlaceholder(block.getReplacement(), block.getPlaceholderValues(), textConfiguration.getReplacements(), textConfiguration.getConfiguration().getSelects());
				validationError.setMissingVariables(TextCompileUtil.getPlaceholder(text));
				validationError.setRequires(new ArrayList<>());
				validationError.setExcludes(new ArrayList<>());
				break;
			case block:
				validationError.setTextBlockId(block.getTextblockId());
				validationError.setTextBlockGroup(TextCompileUtil.textBlockGroupForId(block.getTextblockId(), textConfiguration.getConfiguration()).getGroupName());
				if (block.getReplacement() == null) {
					text = TextCompileUtil.convertToText(block, textConfiguration);
				} else {
					text = TextCompileUtil.fillPlaceholder(block.getReplacement(), block.getPlaceholderValues(), textConfiguration.getReplacements(), textConfiguration.getConfiguration().getSelects());
				}
				validationError.setMissingVariables(TextCompileUtil.getPlaceholder(text));
				validationError.setAfter(TextCompileUtil.getAfter(textArrangement, i, textConfiguration));
				validationError.setRequires(TextCompileUtil.getRequires(textArrangement, block, textConfiguration.getConfiguration()));
				validationError.setExcludes(TextCompileUtil.getExcludes(textArrangement, block, textConfiguration.getConfiguration()));
				break;
			case newline:
			case pagebreak:
			default:
				validationError.setMissingVariables(new ArrayList<>());
				validationError.setRequires(new ArrayList<>());
				validationError.setExcludes(new ArrayList<>());
				break;
			}
			if (!isValid(validationError)) {
				errors.add(validationError);
			}
		}
		result.setErrors(errors);
		result.setValid(errors.isEmpty());
		return result;
	}

	private boolean isValid(ValidationError validationError) {
		if (validationError.getAfter() != null) {
			return false;
		}
		if (!validationError.getExcludes().isEmpty()) {
			return false;
		}
		if (!validationError.getMissingVariables().isEmpty()) {
			return false;
		}
		return validationError.getRequires().isEmpty();
	}

	public AttachmentFile compileTextArrangement(Long statementId, List<Textblock>   textArrangement)
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<TextConfiguration> oTextblockConfiguration = getTextblockConfiguration(statementId);
		if (!oTextblockConfiguration.isPresent()) {
			throw new NotFoundServiceException(STATEMENT_WITH_GIVEN_IDENTIFIER_COULD_NOT_BE_FOUND);
		}
		Optional<CompanyContactBlockModel> oContact = statementService.getContactBlock(statementId);
		if (!oContact.isPresent()) {
			throw new InternalErrorServiceException("Could not get contact details");
		}
		CompanyContactBlockModel contact = oContact.get();

		return statementCompileService.generatePDF(oTextblockConfiguration.get(), contact, textArrangement);
	}

}
