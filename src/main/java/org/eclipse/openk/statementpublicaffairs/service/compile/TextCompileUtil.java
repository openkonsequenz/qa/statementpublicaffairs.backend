/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service.compile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.TextToken;
import org.eclipse.openk.statementpublicaffairs.model.Textblock;
import org.eclipse.openk.statementpublicaffairs.model.Textblock.TextblockType;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;
import org.eclipse.openk.statementpublicaffairs.model.TextblockGroup;
import org.eclipse.openk.statementpublicaffairs.model.TextblockItem;
import org.eclipse.openk.statementpublicaffairs.model.TextblockRequirement;
import org.eclipse.openk.statementpublicaffairs.viewmodel.TextConfiguration;

public class TextCompileUtil {

	private static final Pattern placeholderPattern = Pattern.compile("<([fdts]):([0-9a-zA-Z_\\-]+)>");

	/**
	 * Private constructor.
	 */
	private TextCompileUtil() {
	}

	public static List<TextToken> parseTextToToken(String blockText) {
		List<TextToken> tokens = new ArrayList<>();

		Lexer lexer = new Lexer(blockText);
		while (!lexer.isExausthed()) {
			TextToken token = new TextToken();
			token.setType(lexer.currentToken());
			token.setValue(lexer.currentLexema());
			tokens.add(token);
			lexer.next();
		}
		if (!lexer.isSuccessful()) {
			throw new BadRequestException("Textblock could not be parsed: " + lexer.errorMessage());
		}
		
		
		// second stage filter
		
		List<TextToken> filtered = new ArrayList<>();
		boolean changes;
		do {
			changes = false;
			for (int i=0; i < tokens.size(); i++) {
				if (i == 0 && tokens.size() > 1) {
					// Initial, Star, Space -> Bulletpoint
					if (tokens.get(i).getType() == Token.TK_STAR && tokens.get(i+1).getType() == Token.TK_SPACE) {
						tokens.remove(i);
						tokens.remove(i);
						tokens.add(i, TextToken.newBulletToken());
						changes = true;
						break;
					}
				}
				if (i+2 < tokens.size()) {
						// NL, Star, Space -> NL, Bulletpoint
					if (tokens.get(i).getType() == Token.TK_NL && tokens.get(i+1).getType() == Token.TK_STAR && tokens.get(i+2).getType() == Token.TK_SPACE) {
						tokens.remove(i+1);
						tokens.remove(i+1);
						tokens.add(i+1, TextToken.newBulletToken());
						changes = true;
						break;
					}
				}
				if (i + 1 < tokens.size()) {
					// Star, Star -> Bold
					if (tokens.get(i).getType() == Token.TK_STAR && tokens.get(i+1).getType() == Token.TK_STAR) {
						tokens.remove(i);
						tokens.remove(i);
						tokens.add(i, TextToken.newBoldToken());
						changes = true;
						break;
					}

					// _, _ -> Italic
					if (tokens.get(i).getType() == Token.TK_UNDERSCORE && tokens.get(i+1).getType() == Token.TK_UNDERSCORE) {
						tokens.remove(i);
						tokens.remove(i);
						tokens.add(i, TextToken.newItalicToken());
						changes = true;
						break;
					}
				}
			}
		} while (changes);
		
		// cleanup change TK_UNDERSCORE and TK_STAR to STRING
		for (TextToken token : tokens) {
			if (Token.TK_STAR == token.getType()) {
				token.setType(Token.STRING);
			} else if (Token.TK_UNDERSCORE == token.getType()) {
				token.setType(Token.STRING);
			}
		}
		
		return tokens;
	}

	public static List<List<TextToken>> convertToTextTokens(TextConfiguration textConfiguration,
			List<Textblock>   textArrangement) throws InternalErrorServiceException {

		List<List<TextToken>> textblocks = new ArrayList<>();
		for (Textblock block : textArrangement) {
			List<TextToken> textTokens = new ArrayList<>();
			switch (block.getType()) {
			case newline:
				textTokens.add(TextToken.newLineTextToken());
				break;
			case pagebreak:
				textTokens.add(TextToken.pagebreakTextToken());
				break;
			case block:
				String blockText = convertToText(block, textConfiguration);
				textTokens.addAll(TextCompileUtil.parseTextToToken(blockText));
				break;
			case text:
				String tblockText = fillPlaceholder(block.getReplacement(), block.getPlaceholderValues(),
						textConfiguration.getReplacements(), textConfiguration.getConfiguration().getSelects());
				textTokens.addAll(TextCompileUtil.parseTextToToken(tblockText));
				break;
			default:
				break;
			}
			textblocks.add(textTokens);
		}
		return textblocks;

	}

	public static TextblockItem getTextblockDefForTextblockId(TextblockDefinition textblockDefinition,
			String textblockId) {
		for (TextblockGroup g : textblockDefinition.getGroups()) {
			for (TextblockItem item : g.getTextBlocks()) {
				if (item.getId().equals(textblockId)) {
					return item;
				}
			}
		}
		for (TextblockGroup g : textblockDefinition.getNegativeGroups()) {
			for (TextblockItem item : g.getTextBlocks()) {
				if (item.getId().equals(textblockId)) {
					return item;
				}
			}
		}
		return null;
	}

	public static List<String> getPlaceholder(String text) {
		List<String> placeholder = new ArrayList<>();
		Matcher m = placeholderPattern.matcher(text);
		while (m.find()) {
			placeholder.add(m.group(0));
		}
		return placeholder;
	}

	public static String convertToText(Textblock block, TextConfiguration textConfiguration)
			throws InternalErrorServiceException {
		TextblockItem textBlockDef = getTextblockDefForTextblockId(textConfiguration.getConfiguration(),
				block.getTextblockId());
		if (textBlockDef == null) {
			throw new InternalErrorServiceException("Invalid reference to textblock with id " + block.getTextblockId());
		}

		String text = textBlockDef.getText();

		// Replace with replacement text if set
		if (block.getReplacement() != null) {
			text = block.getReplacement();
		}

		return fillPlaceholder(text, block.getPlaceholderValues(), textConfiguration.getReplacements(),
				textConfiguration.getConfiguration().getSelects());
	}

	public static String fillPlaceholder(String text, Map<String, String> textPlaceholder,
			Map<String, String> replacements, Map<String, List<String>> selects) {
		if (textPlaceholder == null) {
			textPlaceholder = new HashMap<>();
		}
		if (replacements == null) {
			replacements = new HashMap<>();
		}
		if (text == null) {
			return "";
		}
		Matcher m = placeholderPattern.matcher(text);
		Map<String, String> placeholderTexts = new HashMap<>();
		while (m.find()) {
			String phText = m.group(0);
			String type = m.group(1);
			String name = m.group(2);

			switch (type) {
			case "f":
				placeholderTexts.put(phText, textPlaceholder.get(phText));
				break;
			case ("d"):
				placeholderTexts.put(phText, textPlaceholder.get(phText));
				break;
			case ("t"):
				placeholderTexts.put(phText, replacements.get(name));
				break;
			case ("s"):
				Optional<String> oSelectValue = selectValueFor(name, textPlaceholder.get(phText), selects);
				if (oSelectValue.isPresent()) {
					placeholderTexts.put(phText, oSelectValue.get());
				}
				break;
			default:
				break;
			}
		}

		String cleanText = text;
		for (Entry<String, String> entry : placeholderTexts.entrySet()) {
			if (entry.getValue() != null) {
				cleanText = cleanText.replaceAll(entry.getKey(), entry.getValue());
			}
		}
		return cleanText;

	}

	public static Optional<String> selectValueFor(String name, String selectedId,
			Map<String, List<String>> selectValues) {
		try {
			int id = Integer.parseInt(selectedId);
			List<String> values = selectValues.get(name);
			return Optional.of(values.get(id));
		} catch (NumberFormatException | IndexOutOfBoundsException e) {
			return Optional.empty();
		}
	}

	public static TextblockGroup textBlockGroupForId(String textblockId, TextblockDefinition textDefinition) throws InternalErrorServiceException {
		if (!textDefinition.valid()) {
			throw new InternalErrorServiceException("Invalid Textblock Configuration");
		}
		for (TextblockGroup g : textDefinition.getGroups()) {
			for (TextblockItem item : g.getTextBlocks()) {
				if (item.getId().equals(textblockId)) {
					return g;
				}
			}
		}
		for (TextblockGroup g : textDefinition.getNegativeGroups()) {
			for (TextblockItem item : g.getTextBlocks()) {
				if (item.getId().equals(textblockId)) {
					return g;
				}
			}
		}
		return null;
	}

	public static String getAfter(List<Textblock> blocks, int blockIndex, TextConfiguration textConfiguration) throws InternalErrorServiceException {
		TextblockDefinition definition = textConfiguration.getConfiguration();
		TextblockGroup ownGroup = textBlockGroupForId(blocks.get(blockIndex).getTextblockId(), definition);
		if (ownGroup == null) {
			return null;
		}
		int ownGroupBlockId = getConfigGroupBlockId(ownGroup, blocks.get(blockIndex).getTextblockId());
		int textConfigGroupBlockId = -1;
		for (int i = blockIndex; i < blocks.size(); i++) {
			TextblockGroup group = textBlockGroupForId(blocks.get(i).getTextblockId(), definition);
			if (group == null || !ownGroup.getGroupName().equals(group.getGroupName())) {
				continue;
			}
			int configGroupBlockId = getConfigGroupBlockId(group, blocks.get(i).getTextblockId());
			if (configGroupBlockId < ownGroupBlockId && configGroupBlockId > textConfigGroupBlockId) {
				textConfigGroupBlockId = configGroupBlockId;
			}
		}
		if (textConfigGroupBlockId >= 0) {
			return ownGroup.getTextBlocks().get(textConfigGroupBlockId).getId();
		}
		return null;
	}

	private static int getConfigGroupBlockId(TextblockGroup group, String textblockId) {
		List<TextblockItem> blocks = group.getTextBlocks();
		if (blocks == null) {
			return Integer.MAX_VALUE;
		}
		for (int i = 0; i < blocks.size(); i++) {
			if (textblockId.equals(blocks.get(i).getId())) {
				return i;
			}
		}
		return Integer.MAX_VALUE;
	}

	public static List<String> getExcludes(List<Textblock> blocks, Textblock ownBlock,
			TextblockDefinition textDefinition) {
		TextblockItem item = getTextblockDefForTextblockId(textDefinition, ownBlock.getTextblockId());
		List<String> excludes = new ArrayList<>();
		if (item == null || item.getExcludes() == null) {
			return excludes;
		}
		Set<String> ownBlockExcludes = new HashSet<>(item.getExcludes());
		for (Textblock block : blocks) {
			String blockId = block.getTextblockId();
			if (ownBlockExcludes.contains(blockId)) {
				excludes.add(blockId);
			}
		}
		return excludes;
	}

	public static List<TextblockRequirement> getRequires(List<Textblock> blocks, Textblock block,
			TextblockDefinition textblockDefinition) {
		List<TextblockRequirement> notFullfilled = new ArrayList<>();
		TextblockItem item = getTextblockDefForTextblockId(textblockDefinition, block.getTextblockId());
		if (item == null) {
			return notFullfilled;
		}
		
		for (TextblockRequirement req : item.getRequires()) {
			boolean fullfilled = false;
			switch (req.getType()) {
			case and:
				fullfilled = containsAllBlocks(blocks, req.getIds());
				break;
			case or:
				fullfilled = containsAtLeastOneOf(blocks, req.getIds()); 
				break;
			case xor:
				fullfilled = containsOnlyOneOf(blocks, req.getIds());
				break;
			default:
				fullfilled = true;
			}

			if (!fullfilled) {
				notFullfilled.add(req);
			}
		}
		return notFullfilled;
	}

	private static boolean containsOnlyOneOf(List<Textblock> blocks, List<String> ids) {
		Set<String> requiredTextBlockIds = new HashSet<>(ids);
		int origSize = requiredTextBlockIds.size();
		for (Textblock block : blocks) {
			if (TextblockType.block == block.getType()) {
				requiredTextBlockIds.remove(block.getTextblockId());
			}
		}
		return requiredTextBlockIds.size() >= (origSize - 1);
	}

	private static boolean containsAtLeastOneOf(List<Textblock> blocks, List<String> ids) {
		Set<String> requiredTextBlockIds = new HashSet<>(ids);
		for (Textblock block : blocks) {
			if (TextblockType.block == block.getType() && requiredTextBlockIds.contains(block.getTextblockId())) {
				return true;
			}
		}
		return false;
	}

	private static boolean containsAllBlocks(List<Textblock> blocks, List<String> ids) {
		Set<String> requiredTextBlockIds = new HashSet<>(ids);
		for (Textblock block : blocks) {
			if (TextblockType.block == block.getType()) {
				requiredTextBlockIds.remove(block.getTextblockId());
			}
		}
		return requiredTextBlockIds.isEmpty();
	}

}
