/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service.compile;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum Token {

	TK_PB("<PB>"),
	TK_PAR("<PAR>"),
    TK_NL ("\n"),
    TK_SPACE(" "),
    TK_BULLET(" \\*"),
	TK_STAR("\\*"),
    TK_UNDERSCORE("_"), 
    TK_BOLD ("\\*\\*"), 
    TK_ITALIC ("__"), 
    STRING ("[^ \\*_\n]+");

	public static Token[] lexerTokens() {
		Token[] lexertokens = {TK_PB, TK_PAR, TK_NL, TK_SPACE, TK_STAR, TK_UNDERSCORE, STRING};
		return lexertokens;
	}

    private final Pattern pattern;

    Token(String regex) {
        pattern = Pattern.compile("^" + regex);
    }

    int endOfMatch(String s) {
        Matcher m = pattern.matcher(s);

        if (m.find()) {
            return m.end();
        }
        return -1;
    }
}
