/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;

import org.eclipse.openk.statementpublicaffairs.api.MailUtil;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConfigurationException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailEntry;
import org.eclipse.openk.statementpublicaffairs.model.mail.NewMailContext;
import org.eclipse.openk.statementpublicaffairs.service.mail.MailContext;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailSendReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;

@Log
@Service
public class MailService {

	@Autowired
	private NotifyService notificationService;

	@Autowired
	private MailUtil mailUtil;
	
	@Autowired
	private StatementAuthorizationService authorizationService;

	private Set<String> lastCheckStatementMailIdentifiers;

	private MailContext notificationMailContext;

	private MailContext statementMailContext;


    @Value("${mail.account.statement.properties}")
	private String statementMailPropertiesPath;

    @Value("${mail.account.notification.properties}")
	private String notificationMailPropertiesPath;
    
    @Value("${mail.newmailcheck}")
	private boolean newMailCheck;
    
    
	@PostConstruct
	public void init() throws ConfigurationException {
		statementMailContext = mailUtil.contextFor(statementMailPropertiesPath);
		notificationMailContext = mailUtil.contextFor(notificationMailPropertiesPath);
		lastCheckStatementMailIdentifiers = new HashSet<>();
	}

	/**
	 * Get List containing all mails in the statement inbox filtered by sender mail address.
	 * @return  List of MailEntries if successful. On errors an empty list is returned
	 * @throws ForbiddenServiceException 
	 *	  
	 */
	public List<MailEntry> getCurrentStatementMailInbox() throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_SEARCH_CONTACTS);

		List<MailEntry> mails;
		try {
			mails = statementMailContext.getInboxMessages(statementMailContext.getSender());
		} catch (MessagingException e) {
			log.warning("Could not get statement inbox mails - " + e.getMessage());
			mails = new ArrayList<>();
		}
		return mails;
	}
	
	/**
	 * Check if statement mailbox has mails.
	 * @return true if mails in inbox. False if no messages in inbox or when error occurred
	 */
	public boolean currentStatementMailInboxHasMail() {
		try {
			return !statementMailContext.getInboxMessageIds(statementMailContext.getSender()).isEmpty();
		} catch (MessagingException e) {
			log.warning("Could not get statement inbox messageIds - " + e.getMessage());
		}
		return false;
	}

	
	/**
	 * Check id statement mailbox has new mails compared to the last check.
	 * @return true if additional mails in inbox. False if no additional mails or when erroro occurred
	 */
	protected boolean currentStatementMailInboxHasNewMail() {
		try {
			List<String> current = statementMailContext.getInboxMessageIds(statementMailContext.getSender());
			for (String mailId : current) {
				if (!lastCheckStatementMailIdentifiers.contains(mailId)) {
					lastCheckStatementMailIdentifiers.clear();
					lastCheckStatementMailIdentifiers.addAll(current);
					return true;
				}
			}
		} catch (MessagingException e) {
			log.warning("Could not get statement inbox messageIds - " + e.getMessage());
		}
		return false;
	}

	public Optional<MailEntry> getMail(String mailId) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_MAIL);
		return statementMailContext.getMail(mailId);
	}

	public Map<String, AttachmentFile> getStatementInboxMailAttachment(String messageId, Set<String> fileNames) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_MAIL);
		return statementMailContext.getAttachments(messageId, fileNames);
	}

	public void deleteMail(String messageId) throws InternalErrorServiceException, NotFoundServiceException, InterruptedException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_MOVE_MAIL);
		statementMailContext.deleteMail(messageId);
	}

	public void moveMailFromInboxToStatements(String messageId) throws InternalErrorServiceException, NotFoundServiceException, InterruptedException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_MOVE_MAIL);
		statementMailContext.setProcessedMail(messageId);
	}

	public MailSendReport sendStatementMail(NewMailContext mail) {
		return statementMailContext.sendMail(mail);
	}

	public void sendNotifyMail(NewMailContext mail) {
		notificationMailContext.sendMail(mail);
	}


	/**
	 * Checks statement inbox every hour for new mails. Notifies Statement_OIC about
	 * new mails.
	 */
	@Scheduled(cron = "${mail.newmailcheckCron}")
	public void newStatementRequestInboxCheck() {
		log.info("Check for new mail");
		if (!newMailCheck) {
			return;
		}
		boolean newMails = false;
		synchronized (statementMailContext) {
			newMails = currentStatementMailInboxHasNewMail();
		}
		if (newMails) {
			notificationService.notifyNewStatementMailInboxNotification();
		}
	}

	protected MailContext getStatementMailContext() {
		return statementMailContext;
	}

	public MailContext getNotificationMailContext() {
		return notificationMailContext;
	}

}
