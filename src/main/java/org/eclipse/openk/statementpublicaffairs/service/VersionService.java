/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.Getter;

/**
 * VersionService provides information about the module version.
 * 
 * @author Tobias Stummer
 *
 */
@Service
@Getter
public class VersionService {

    /**
     * Build version.
     */
    @Value("${build.version}")
    private String buildVersion;

    /**
     * Application name.
     */
    @Value("${application.name}")
    private String applicationName;
    

}
