/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.SearchParams;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.model.db.TblReqdepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementtype;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementPositionSearch;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementSearch;
import org.eclipse.openk.statementpublicaffairs.model.db.VwUserStatementSearch;
import org.eclipse.openk.statementpublicaffairs.repository.PagedAndSortedStatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.PagedAndSortedUserStatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.ReqDepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementEditLogRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementtypeRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementPositionRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementPositionSearchSpecification;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementSearchSpecification;
import org.eclipse.openk.statementpublicaffairs.repository.VwUserStatementSearchSpecification;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DashboardStatement;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementPosition;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTaskModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * StatementOverviewService uses the StatementRepository to access the current
 * statements.
 * 
 * @author Tobias Stummer
 *
 */
@Service
public class StatementOverviewService {

	private static final String EXCEPTION_ACCESSING_STATEMENT_REPOSITORY = "Exception occurred when accessing the statement repository.";

	private static final String INVALID_PARAMETERS_BROWSING = "Invalid parameters when browsing statement repository for all statements.";

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private PagedAndSortedStatementRepository pagedStatementRepository;

	@Autowired
	private PagedAndSortedUserStatementRepository pagedUserStatementRepository;

	@Autowired
	private StatementtypeRepository statementtypeRepository;

	@Autowired
	private StatementEditLogRepository statementEditLogRepository;

	@Autowired
	private ReqDepartmentRepository reqDepartmentRepository;
	
	@Autowired
	private VwStatementPositionRepository statementPositionRepository;

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Autowired
	private StatementProcessService statementProcessService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private UserInfoService userInfoService;

	/**
	 * Get all statements from the database.
	 * 
	 * @return List of StatementModel containing the statement overview data of each
	 *         statement in the database.
	 * @throws ForbiddenServiceException
	 */
	public List<StatementDetailsModel> getAllStatementModels()
			throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		List<StatementDetailsModel> statements = new ArrayList<>();
		try {
			Iterable<TblStatement> results = statementRepository.findAll();
			statements.addAll(tblStatementsToStatementModels(results));
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException(INVALID_PARAMETERS_BROWSING, e);
		} catch (PersistenceException e) {
			throw new InternalErrorServiceException(EXCEPTION_ACCESSING_STATEMENT_REPOSITORY, e);
		}
		return statements;
	}

	/**
	 * Get all statments from the database filtered by finish state and taskId
	 * existence.
	 * 
	 * @param finished  only get finished statements
	 * @param hasTaskId only get statements with a taskId
	 * @return Filtered List of StatementModels.
	 */
	public List<StatementDetailsModel> getStatementModels(boolean finished, boolean hasTaskId)
			throws InternalErrorServiceException {
		List<StatementDetailsModel> statements = new ArrayList<>();
		try {
			Iterable<TblStatement> results;
			if (hasTaskId) {
				results = statementRepository.findByFinishedAndFilterHasTaskId(finished);
			} else {
				results = statementRepository.findByFinishedAndFilterNoTaskId(finished);
			}
			statements.addAll(tblStatementsToStatementModels(results));
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException(INVALID_PARAMETERS_BROWSING, e);
		} catch (PersistenceException e) {
			throw new InternalErrorServiceException(EXCEPTION_ACCESSING_STATEMENT_REPOSITORY, e);
		}
		return statements;
	}

	private List<StatementDetailsModel> tblStatementsToStatementModels(Iterable<TblStatement> tblStatements) {
		List<StatementDetailsModel> statements = new ArrayList<>();
		for (TblStatement statement : tblStatements) {
			StatementDetailsModel model = new StatementDetailsModel();
			model.setFinished(statement.getFinished());
			model.setId(statement.getId());
			if (statement.getBusinessKey() != null) {
				model.setBusinessKey(statement.getBusinessKey());
			}
			TypeConversion.dateStringOfLocalDate(statement.getDueDate()).ifPresent(model::setDueDate);
			TypeConversion.dateStringOfLocalDate(statement.getReceiptDate()).ifPresent(model::setReceiptDate);
			TypeConversion.dateStringOfLocalDate(statement.getCreationDate()).ifPresent(model::setCreationDate);
			TypeConversion.dateStringOfLocalDate(statement.getFinishedDate()).ifPresent(model::setFinishedDate);
			model.setTitle(statement.getTitle());
			model.setDistrict(statement.getDistrict());
			model.setCity(statement.getCity());
			model.setTypeId(statement.getType().getId());
			model.setContactId(statement.getContactDbId());
			model.setCustomerReference(statement.getCustomerReference());

			statements.add(model);
		}
		return statements;
	}

	/**
	 * Get all statments from the database filterted by finish state.
	 * 
	 * @param finished only get finished statements
	 * @return Filtered List of StatementModels
	 */
	public List<StatementDetailsModel> getStatementModels(boolean finished) throws InternalErrorServiceException {
		List<StatementDetailsModel> statements = new ArrayList<>();
		statements.addAll(getStatementModels(finished, true));
		statements.addAll(getStatementModels(finished, false));
		return statements;
	}

	/**
	 * Get all statementtypes from the database.
	 * 
	 * @return List of StatementTypeModels
	 * @throws ForbiddenServiceException
	 */
	public List<StatementTypeModel> getStatementTypeModels()
			throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_BASE_DATA);
		List<StatementTypeModel> statementTypes = new ArrayList<>();
		try {
			for (TblStatementtype sType : statementtypeRepository.findAll()) {
				StatementTypeModel model = new StatementTypeModel();
				model.setId(sType.getId());
				model.setName(sType.getName());
				statementTypes.add(model);
			}
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException(
					"Invalid parameters when browsing statement repository statement types.", e);
		} catch (PersistenceException e) {
			throw new InternalErrorServiceException("Exception occurred when accessing the statement repositor.", e);
		}
		return statementTypes;
	}

	protected String postgresRegexSearch(String[] searchStrings) {
		return "%(" + String.join("|", searchStrings) + ")%";
	}

	public Page<StatementDetailsModel> searchStatementModels(SearchParams params, Pageable pPageable)
			throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);

		VwStatementSearchSpecification spec = new VwStatementSearchSpecification(params);

		Page<VwStatementSearch> tStatements = pagedStatementRepository.findAll(spec, pPageable);
		Pageable pageable = tStatements.getPageable();
		List<StatementDetailsModel> statements = new ArrayList<>();
		for (VwStatementSearch statement : tStatements) {
			StatementDetailsModel model = new StatementDetailsModel();
			model.setFinished(statement.getFinished());
			model.setId(statement.getId());
			if (statement.getBusinessKey() != null) {
				model.setBusinessKey(statement.getBusinessKey());
			}
			TypeConversion.dateStringOfLocalDate(statement.getDueDate()).ifPresent(model::setDueDate);
			TypeConversion.dateStringOfLocalDate(statement.getReceiptDate()).ifPresent(model::setReceiptDate);
			TypeConversion.dateStringOfLocalDate(statement.getCreationDate()).ifPresent(model::setCreationDate);
			model.setTitle(statement.getTitle());
			model.setDistrict(statement.getDistrict());
			model.setCity(statement.getCity());
			model.setContactId(statement.getContactDbId());
			model.setTypeId(statement.getTypeId());
			model.setCustomerReference(statement.getCustomerReference());
			model.setSourceMailId(statement.getSourceMailId());
			statements.add(model);
		}
		return new PageImpl<>(statements, pageable, tStatements.getTotalElements());
	}

	public Page<StatementDetailsModel> searchStatementModelsEditedByMe(SearchParams params, Pageable pPageable)
			throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);


		List<StatementDetailsModel> statements = new ArrayList<>();
		Optional<Long> oUserId = usersService.getUserId(userInfoService.getUserName());
		if (!oUserId.isPresent()) {
			return new PageImpl<>(statements, pPageable, 0);
		}

		VwUserStatementSearchSpecification spec = new VwUserStatementSearchSpecification(params, oUserId.get());

		Page<VwUserStatementSearch> tStatements = pagedUserStatementRepository.findAll(spec, pPageable);
		Pageable pageable = tStatements.getPageable();
		for (VwUserStatementSearch statement : tStatements) {
			StatementDetailsModel model = new StatementDetailsModel();
			model.setId(statement.getId());
			model.setFinished(statement.getFinished());
			TypeConversion.dateStringOfLocalDate(statement.getCreationDate()).ifPresent(model::setCreationDate);
			TypeConversion.dateStringOfLocalDate(statement.getReceiptDate()).ifPresent(model::setReceiptDate);
			TypeConversion.dateStringOfLocalDate(statement.getDueDate()).ifPresent(model::setDueDate);
			if (statement.getBusinessKey() != null) {
				model.setBusinessKey(statement.getBusinessKey());
			}
			model.setTypeId(statement.getTypeId());
			model.setContactId(statement.getContactDbId());
			model.setCustomerReference(statement.getCustomerReference());
			model.setTitle(statement.getTitle());
			model.setCity(statement.getCity());
			model.setSourceMailId(statement.getSourceMailId());
			model.setDistrict(statement.getDistrict());
			statements.add(model);
		}
		return new PageImpl<>(statements, pageable, tStatements.getTotalElements());
	}

	public List<StatementDetailsModel> getStatementModelsByInIds(List<Long> statementIds)
			throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		List<StatementDetailsModel> statements = new ArrayList<>();
		try {
			List<TblStatement> results;
			results = statementRepository.findByIdIn(statementIds);
			statements.addAll(tblStatementsToStatementModels(results));
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException(INVALID_PARAMETERS_BROWSING, e);
		} catch (PersistenceException e) {
			throw new InternalErrorServiceException(EXCEPTION_ACCESSING_STATEMENT_REPOSITORY, e);
		}
		return statements;
	}

	public List<DashboardStatement> getDashboardStatements()
			throws InternalErrorServiceException, ForbiddenServiceException {

		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_DASHBOARD_STATEMENTS);
		List<DashboardStatement> statements = new ArrayList<>();
		List<StatementDetailsModel> currentStatements = tblStatementsToStatementModels(
				statementRepository.findByFinishedAndFilterHasTaskId(false));
		String username = userInfoService.getUserName();
		for (StatementDetailsModel statement : currentStatements) {
			DashboardStatement dstatement = new DashboardStatement();
			dstatement.setMandatoryContributionsCount(0);
			dstatement.setMandatoryDepartmentsCount(0);
			dstatement.setInfo(statement);
			Optional<List<StatementTaskModel>> oTasks = statementProcessService
					.getCurrentStatementTasks(statement.getId());
			if (oTasks.isPresent()) {
				dstatement.setTasks(oTasks.get());
			} else {
				dstatement.setTasks(new ArrayList<>());
			}

			dstatement.setEditedByMe(
					!statementEditLogRepository.findByStatementIdAndUsername(statement.getId(), username).isEmpty());

			List<TblReqdepartment> reqDepartments = reqDepartmentRepository.findByStatementId(statement.getId());

			Optional<Long> ownDepartmentId = usersService.ownDepartmentId();
			for (TblReqdepartment rdp : reqDepartments) {
				fillDashboardStatementContributions(dstatement, ownDepartmentId, rdp);
			}
			statements.add(dstatement);
		}
		return statements;
	}

	private void fillDashboardStatementContributions(DashboardStatement dstatement, Optional<Long> ownDepartmentId,
			TblReqdepartment rdp) {
		if (ownDepartmentId.isPresent() && ownDepartmentId.get().equals(rdp.getDepartment().getId())) {
			dstatement.setCompletedForMyDepartment(rdp.getContributed() != null && rdp.getContributed());
			dstatement.setOptionalForMyDepartment(rdp.getOptional() != null && rdp.getOptional());
		}
		// is mandatory
		if (!Boolean.TRUE.equals(rdp.getOptional())) {
			dstatement.setMandatoryDepartmentsCount(dstatement.getMandatoryDepartmentsCount() + 1);
			if (Boolean.TRUE.equals(rdp.getContributed())) {
				dstatement.setMandatoryContributionsCount(dstatement.getMandatoryContributionsCount() + 1);
			}
		}
	}

	public List<StatementPosition> searchStatementPositions(SearchParams params) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		VwStatementPositionSearchSpecification spec = new VwStatementPositionSearchSpecification(params);
		List<VwStatementPositionSearch> results = statementPositionRepository.findAll(spec);
		List<StatementPosition> statementPositions = new ArrayList<>();
		for (VwStatementPositionSearch res : results) {
			StatementPosition pos = new StatementPosition();
			pos.setId(res.getId());
			TypeConversion.dateStringOfLocalDate(res.getDueDate()).ifPresent(pos::setDueDate);
			pos.setFinished(res.getFinished());
			pos.setPosition(res.getPosition());
			pos.setTitle(res.getTitle());
			pos.setTypeId(res.getTypeId());
			statementPositions.add(pos);
		}
		return statementPositions;
	}

}
