/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.eclipse.openk.statementpublicaffairs.model.Tags.STATEMENT;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.UnprocessableEntityServiceException;
import org.eclipse.openk.statementpublicaffairs.model.ProcessActivityHistoryModel;
import org.eclipse.openk.statementpublicaffairs.model.ProcessDefinitionModel;
import org.eclipse.openk.statementpublicaffairs.model.TaskInfo;
import org.eclipse.openk.statementpublicaffairs.model.WorkflowTaskModel;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.model.db.TblAttachment;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailSendReport;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailTransferAttachment;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ProcessActivityHistoryViewModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ProcessHistoryModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTaskModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.WorkflowTaskVariableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * StatementProcessService provides interfaces to access and manipulate
 * statement workflow process.
 * 
 * @author Tobias Stummer
 *
 */
@Service
public class StatementProcessService {

	private static final String INVALID_TASK_ID = "Invalid taskId";

	private static final String RNOT_BOUND_TO_A_WORKFLOW_PROCESS = "Requested statement is not bound to a workflow process.";

	private static final String STATEMENT_IS_BOUND_NOT_EXISTENT_WORKFLOW = "Requested statement is bound to a not existent workflow process.";

	@Autowired
	private WorkflowService workflowService;

	@Autowired
	private StatementService statementService;

	@Autowired
	private UserInfoService userInfoService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Value("${workflow.handleServiceTasks}")
	private boolean handleServiceTasks;

	/**
	 * Get list of current tasks for statement with provided statement identifier.
	 * 
	 * @param statementId statement identifier
	 * @return List of current workflow process tasks if successful. Optional#empty
	 *         if statement does not exist.
	 * @throws InternalErrorServiceException if statement is not bound to a workflow
	 *                                       process instance, bounded workflow
	 *                                       process does not exist or an issue
	 *                                       occurred when accessing the workflow
	 *                                       process details.
	 * @throws ForbiddenServiceException     if access is not authorised.
	 */
	public Optional<List<StatementTaskModel>> getCurrentStatementTasks(Long statementId)
			throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<StatementDetailsModel> statementModel = statementService.getStatement(statementId);
		List<StatementTaskModel> statementTasks = new ArrayList<>();
		if (!statementModel.isPresent()) {
			return Optional.empty();
		}
		String businessKey = statementModel.get().getBusinessKey();
		if (businessKey == null) {
			throw new InternalErrorServiceException(RNOT_BOUND_TO_A_WORKFLOW_PROCESS);
		}

		Optional<List<WorkflowTaskModel>> workflowTaskModels = workflowService
				.getCurrentTasksOfProcessBusinessKey(businessKey);
		if (!workflowTaskModels.isPresent()) {
			throw new InternalErrorServiceException(STATEMENT_IS_BOUND_NOT_EXISTENT_WORKFLOW);
		}
		for (WorkflowTaskModel workflowTaskModel : workflowTaskModels.get()) {
			authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_WORKFLOW_TASK);
			StatementTaskModel statementTaskModel = new StatementTaskModel();
			statementTaskModel.setAuthorized(
					usersService.isAllowedToClaimTask(statementId, workflowTaskModel.getTaskDefinitionKey()));
			statementTaskModel.setStatementId(statementId);
			statementTaskModel.setName(workflowTaskModel.getName());
			statementTaskModel.setProcessDefinitionKey(workflowTaskModel.getProcessDefinitionKey());
			statementTaskModel.setTaskDefinitionKey(workflowTaskModel.getTaskDefinitionKey());
			statementTaskModel.setTaskId(workflowTaskModel.getTaskId());
			statementTaskModel.setAssignee(workflowTaskModel.getAssignee());
			statementTaskModel.setRequiredVariables(workflowTaskModel.getRequiredVariables());
			statementTasks.add(statementTaskModel);
		}
		return Optional.of(statementTasks);
	}

	/**
	 * Get workflow process history for statement with provided statement
	 * identifier. The history contains details about the statement process, such as
	 * the definition key, version, an ordered list of already finished tasks and a
	 * list of current tasks.
	 *
	 * @param statementId statement identifier
	 * @return Process history if successful. Optional#empty is statement does not
	 *         exist
	 * @throws InternalErrorServiceException if statement is not bound to a workflow
	 *                                       process instance, bounded workflow
	 *                                       process does not exist or an issue
	 *                                       occurred when accessing the workflow
	 *                                       process details.
	 * @throws ForbiddenServiceException     if access is not authorised.
	 */
	public Optional<ProcessHistoryModel> getStatmentProcessHistory(Long statementId)
			throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<StatementDetailsModel> statementModel = statementService.getStatement(statementId);
		if (!statementModel.isPresent()) {
			return Optional.empty();
		}

		ProcessHistoryModel history = new ProcessHistoryModel();

		String businessKey = statementModel.get().getBusinessKey();
		if (businessKey == null) {
			throw new InternalErrorServiceException(RNOT_BOUND_TO_A_WORKFLOW_PROCESS);
		}

		Optional<ProcessDefinitionModel> oProcessDefinition = workflowService.getProcessDefinition(businessKey);
		if (!oProcessDefinition.isPresent()) {
			throw new InternalErrorServiceException(STATEMENT_IS_BOUND_NOT_EXISTENT_WORKFLOW);
		}
		authorizationService.authorize("ANY", "READ_STATEMENT_PROCESS_HISTORY");
		ProcessDefinitionModel pdm = oProcessDefinition.get();
		history.setProcessName(pdm.getName());
		history.setProcessVersion(pdm.getVersion());

		Optional<List<ProcessActivityHistoryModel>> activityHistory = workflowService
				.getStatementProcessHistory(businessKey);
		if (!activityHistory.isPresent()) {
			throw new InternalErrorServiceException(STATEMENT_IS_BOUND_NOT_EXISTENT_WORKFLOW);
		}
		List<ProcessActivityHistoryModel> finished = activityHistory.get().stream()
				.filter(activity -> activity.getStartTime() != null && activity.getEndTime() != null)
				.sorted(Comparator.comparing(ProcessActivityHistoryModel::getStartTime)).collect(Collectors.toList());
		List<ProcessActivityHistoryModel> current = activityHistory.get().stream()
				.filter(activity -> activity.getStartTime() != null && activity.getEndTime() == null)
				.sorted(Comparator.comparing(ProcessActivityHistoryModel::getStartTime)).collect(Collectors.toList());

		history.setCurrentProcessActivities(processActivityHistoryViewModelListOf(current));
		history.setFinishedProcessActivities(processActivityHistoryViewModelListOf(finished));

		return Optional.of(history);
	}

	private List<ProcessActivityHistoryViewModel> processActivityHistoryViewModelListOf(
			List<ProcessActivityHistoryModel> models) {
		List<ProcessActivityHistoryViewModel> viewmodels = new ArrayList<>();
		for (ProcessActivityHistoryModel model : models) {
			ProcessActivityHistoryViewModel viewmodel = new ProcessActivityHistoryViewModel();
			viewmodel.setActivityId(model.getActivityId());
			viewmodel.setActivityName(model.getActivityName());
			viewmodel.setActivityType(model.getActivityType());
			viewmodel.setAssignee(model.getAssignee());
			viewmodel.setCanceled(model.getCanceled());
			viewmodel.setCompleteScope(model.getCompleteScope());
			viewmodel.setDurationInMillis(model.getDurationInMillis());
			if (model.getEndTime() != null) {
				TypeConversion.iso8601InstantStringOfZonedDateTime(model.getEndTime()).ifPresent(viewmodel::setEndTime);
			}
			viewmodel.setId(model.getId());
			viewmodel.setProcessDefinitionId(model.getProcessDefinitionId());
			viewmodel.setProcessDefinitionKey(model.getProcessDefinitionKey());
			TypeConversion.iso8601InstantStringOfZonedDateTime(model.getStartTime()).ifPresent(viewmodel::setStartTime);
			viewmodel.setTenantId(model.getTenantId());
			viewmodels.add(viewmodel);
		}
		return viewmodels;
	}

	/**
	 * Claim workflow process task with provided taskId of statement with provided
	 * statementId.
	 * 
	 * Claiming a workflow process task assigns the current user to the task.
	 * 
	 * @param statementId statement identifier
	 * @param taskId      task identifier
	 * @return Current workflow process task with provided taskId if successful.
	 *         Optional#empty if statement does not exist.
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       statement task.
	 * @throws ForbiddenServiceException     if access is not authorised.
	 * @throws BadRequestServiceException    if workflow process task is already
	 *                                       claimed by another user
	 */
	public Optional<StatementTaskModel> claimStatementTask(Long statementId, String taskId)
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {
		Optional<TaskInfo> oTaskinfo = getTaskInfo(statementId, taskId);
		if (!oTaskinfo.isPresent()) {
			return Optional.empty();
		}

		String constraint = isRequiredDepartmentMember(statementId, userInfoService.getUserName())
				? AuthorizationRuleActions.C_IS_REQ_DIVISION_MEMBER
				: Rule.ANY;
		authorizationService.authorize(oTaskinfo.get().getTaskDefinitionKey(), AuthorizationRuleActions.A_CLAIM_TASK,
				constraint);
		Optional<List<StatementTaskModel>> currentTasks = getCurrentStatementTasks(statementId);
		if (!currentTasks.isPresent()) {
			return Optional.empty();
		}
		for (StatementTaskModel task : currentTasks.get()) {
			if (task.getTaskId().contentEquals(taskId)) {
				return claim(statementId, task);
			}
		}
		return Optional.empty();
	}

	private boolean isRequiredDepartmentMember(Long statementId, String userName) {
		return usersService.isRequiredDepartmentUser(statementId, userName);
	}

	private Optional<StatementTaskModel> claim(Long statementId, StatementTaskModel task)
			throws InternalErrorServiceException, BadRequestServiceException {
		if (task.getAssignee() == null) {
			Optional<WorkflowTaskModel> oWtm = workflowService.claimTask(task.getTaskId(),
					userInfoService.getUserName());
			if (oWtm.isPresent()) {
				statementService.editLog(statementId, task.getTaskDefinitionKey() + " - claim");
				return Optional.of(statementTaskModelOfworkflowTaskModel(statementId, oWtm.get()));
			} else {
				throw new InternalErrorServiceException("Not able to get task after claim");
			}
		} else {
			// already claimed by user
			if (task.getAssignee().equals(userInfoService.getUserName())) {
				return Optional.of(task);
			}
			throw new BadRequestServiceException("Tried to claim already claimed task");
		}
	}

	private StatementTaskModel statementTaskModelOfworkflowTaskModel(Long statementId,
			WorkflowTaskModel workflowTaskModel) {
		StatementTaskModel statementTaskModel = new StatementTaskModel();
		statementTaskModel.setStatementId(statementId);
		statementTaskModel.setProcessDefinitionKey(workflowTaskModel.getProcessDefinitionKey());
		statementTaskModel.setTaskDefinitionKey(workflowTaskModel.getTaskDefinitionKey());
		statementTaskModel.setTaskId(workflowTaskModel.getTaskId());
		statementTaskModel.setAssignee(workflowTaskModel.getAssignee());
		statementTaskModel.setAuthorized(
				usersService.isAllowedToClaimTask(statementId, workflowTaskModel.getTaskDefinitionKey()));
		statementTaskModel.setRequiredVariables(workflowTaskModel.getRequiredVariables());
		statementTaskModel.setName(workflowTaskModel.getName());
		return statementTaskModel;
	}

	/**
	 * Unclaim workflow process task with provided taskId of statement with provided
	 * statementId.
	 * 
	 * unClaiming a workflow process task removes the user assignment of a task.
	 * 
	 * @param statementId statement identifier
	 * @param taskId      task identifier
	 * @return Current workflow process task with provided taskId if successful.
	 *         Optional#empty if statement does not exist.
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       statement task.
	 * @throws ForbiddenServiceException     if access is not authorised.
	 * @throws BadRequestServiceException    if workflow process task is not claimed
	 */
	public Optional<StatementTaskModel> unClaimStatementTask(Long statementId, String taskId)
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {
		Optional<TaskInfo> oTaskinfo = getTaskInfo(statementId, taskId);
		if (!oTaskinfo.isPresent()) {
			return Optional.empty();
		}
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_UNCLAIM_TASK);
		Optional<List<StatementTaskModel>> currentTasks = getCurrentStatementTasks(statementId);
		if (!currentTasks.isPresent()) {
			return Optional.empty();
		}
		for (StatementTaskModel task : currentTasks.get()) {
			if (task.getTaskId().contentEquals(taskId)) {
				if (task.getAssignee() != null) {
					Optional<WorkflowTaskModel> oWtm = workflowService.unClaimTask(taskId);
					if (oWtm.isPresent()) {
						return Optional.of(statementTaskModelOfworkflowTaskModel(statementId, oWtm.get()));
					} else {
						throw new InternalErrorServiceException("Not able to get task after unclaim");
					}
				} else {
					throw new BadRequestServiceException("Tried to unclaim not claimed task");
				}
			}
		}
		return Optional.empty();
	}

	/**
	 * Complete workflow process task with provided taskId of statement with
	 * provided statementId.
	 * 
	 * Completion of a workflow process task can only be successful if all of the
	 * required workflow process variables are provided.
	 * 
	 * @param statementId           statement identifier
	 * @param taskId                task identifier
	 * @param completeTaskVariables variables required to successful complete the
	 *                              task
	 * 
	 * @return Optional#true if successful. Optional#empty if statement does not
	 *         exist.
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       statement task.
	 * @throws ForbiddenServiceException     if access is not authorised.
	 * @throws BadRequestServiceException    if workflow process task is not claimed
	 *                                       or not all required variables are
	 *                                       provided.
	 * @throws NotFoundServiceException
	 */
	public Optional<Boolean> completeStatementTask(Long statementId, String taskId,
			Map<String, WorkflowTaskVariableModel> completeTaskVariables) throws InternalErrorServiceException,
			ForbiddenServiceException, BadRequestServiceException, NotFoundServiceException {
		Optional<TaskInfo> oTaskinfo = getTaskInfo(statementId, taskId);
		if (!oTaskinfo.isPresent()) {
			return Optional.empty();
		}
		TaskInfo taskInfo = oTaskinfo.get();
		authorizationService.authorizeByClaimedUser(taskInfo.getAssignee());
		if (taskInfo.getAssignee() == null) {
			throw new BadRequestServiceException("Tried to complete not claimed task");
		} else {
			if (!taskInfo.getAssignee().equals(userInfoService.getUserName())) {
				throw new BadRequestServiceException("Tried to complete task claimed by other user");
			}
		}
		Optional<Boolean> oCompleted = workflowService.completeTask(statementId, taskInfo, completeTaskVariables);
		if (oCompleted.isPresent() && Boolean.TRUE.equals(oCompleted.get())) {
			// check history and close
			Optional<String> oBusinessKey = statementService.getStatementBusinessKey(statementId);
			if (oBusinessKey.isPresent() && (workflowService.isFinishedWorkflow(oBusinessKey.get()))) {
				LocalDate today = LocalDate.now();
				statementService.finishStatement(statementId, today);
			}
		}
		return oCompleted;

	}

	/**
	 * Create new workflow process instance for provided statement.
	 * 
	 * @param statement statement
	 * @return statement details if successful. Optional#empty if workflow process
	 *         could not be created.
	 * @throws InternalErrorServiceException       if statementService create
	 *                                             statement throws
	 *                                             InternalErrorServiceException
	 * @throws BadRequestServiceException          if statementService create
	 *                                             statement throws
	 *                                             BadRequestServiceException
	 * @throws ForbiddenServiceException
	 * @throws InterruptedException
	 * @throws NotFoundServiceException
	 * @throws UnprocessableEntityServiceException
	 */
	public Optional<StatementDetailsModel> createStatement(StatementDetailsModel statement)
			throws InternalErrorServiceException, BadRequestServiceException, ForbiddenServiceException,
			NotFoundServiceException, InterruptedException, UnprocessableEntityServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_CREATE_STATEMENT);
		String businessKey = workflowService.startProcessLatestVersion();
		statement.setBusinessKey(businessKey);
		try {
			return statementService.createStatement(statement);
		} catch (InternalErrorServiceException e) {
			// cleanup
			workflowService.deleteProcessInstance(businessKey);
			throw new InternalErrorServiceException("Error occurred creating a statement with the statementService", e);
		}
	}

	/**
	 * Get workflow diagram xml for statement with statementId.
	 * 
	 * @param statementId statement identifier
	 * @return workflow definition as String in xml format if successfull. Otherwise
	 *         {@link Optional#empty()}.
	 * @throws InternalErrorServiceException
	 * @throws ForbiddenServiceException
	 */
	public Optional<String> getStatementWorkflow(Long statementId)
			throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<StatementDetailsModel> statementModel = statementService.getStatement(statementId);
		if (!statementModel.isPresent()) {
			return Optional.empty();
		}
		String businessKey = statementModel.get().getBusinessKey();
		if (businessKey == null) {
			throw new InternalErrorServiceException(RNOT_BOUND_TO_A_WORKFLOW_PROCESS);
		}

		Optional<ProcessDefinitionModel> oProcessDefinition = workflowService.getProcessDefinition(businessKey);
		if (!oProcessDefinition.isPresent()) {
			throw new InternalErrorServiceException(STATEMENT_IS_BOUND_NOT_EXISTENT_WORKFLOW);
		}

		ProcessDefinitionModel pd = oProcessDefinition.get();
		String pdId = pd.getId();

		return workflowService.getWorkflowDefinitionXML(pdId);
	}

	public Optional<TaskInfo> getTaskInfo(Long statementId, String taskId)
			throws InternalErrorServiceException, ForbiddenServiceException {
		Optional<List<StatementTaskModel>> currentTasks = getCurrentStatementTasks(statementId);
		if (!currentTasks.isPresent()) {
			return Optional.empty();
		}
		for (StatementTaskModel task : currentTasks.get()) {
			if (task.getTaskId().contentEquals(taskId)) {
				TaskInfo ti = new TaskInfo();
				ti.setAssignee(task.getAssignee());
				ti.setTaskDefinitionKey(task.getTaskDefinitionKey());
				ti.setTaskId(taskId);
				return Optional.of(ti);
			}
		}
		return Optional.empty();
	}

	public Optional<StatementDetailsModel> updateStatement(Long statementId, String taskId,
			StatementDetailsModel statement) throws ForbiddenServiceException, BadRequestServiceException,
			InternalErrorServiceException, NotFoundServiceException, UnprocessableEntityServiceException {
		if (statement.getId() != null && !statementId.equals(statement.getId())) {
			throw new BadRequestServiceException("StatementId does not match statement model.");
		}
		Optional<TaskInfo> ti = getTaskInfo(statementId, taskId);
		if (ti.isPresent()) {
			authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
			authorizationService.authorize(ti.get().getTaskDefinitionKey(),
					AuthorizationRuleActions.A_MANIPULATE_STATEMENT, AuthorizationRuleActions.C_IS_CLAIMED_BY_USER);
		}

		return statementService.updateStatement(statementId, statement);

	}

	public void setAttachmentTags(Long statementId, String taskId, Long attachmentId, Set<String> tagIds)
			throws ForbiddenServiceException, InternalErrorServiceException, NotFoundServiceException,
			BadRequestServiceException {
		Optional<TaskInfo> ti = getTaskInfo(statementId, taskId);
		if (!ti.isPresent()) {
			throw new NotFoundServiceException(INVALID_TASK_ID);
		}
		authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
		authorizationService.authorize(ti.get().getTaskDefinitionKey(),
				AuthorizationRuleActions.A_MANIPULATE_ATTACHMENTS, AuthorizationRuleActions.C_IS_CLAIMED_BY_USER);

		statementService.setTags(statementId, attachmentId, tagIds);
	}

	public Optional<AttachmentModel> createStatementAttachmentToModel(Long statementId, String taskId, String fileName,
			String fileType, InputStream inputStream, long length, Set<String> tagIds)
			throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {
		Optional<TaskInfo> ti = getTaskInfo(statementId, taskId);
		if (!ti.isPresent()) {
			throw new NotFoundServiceException(INVALID_TASK_ID);
		}
		authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
		authorizationService.authorize(ti.get().getTaskDefinitionKey(),
				AuthorizationRuleActions.A_MANIPULATE_ATTACHMENTS, AuthorizationRuleActions.C_IS_CLAIMED_BY_USER);
		if (tagIds.contains("statement")) {
			authorizationService.authorize(ti.get().getTaskDefinitionKey(),
					AuthorizationRuleActions.A_REPLACE_STATEMENT_RESPONSE,
					AuthorizationRuleActions.C_IS_CLAIMED_BY_USER);
			// delete all attachments with tag statement
			Optional<List<AttachmentModel>> oAttachments = statementService.getStatementAttachments(statementId);
			if (oAttachments.isPresent()) {
				List<AttachmentModel> attachments = oAttachments.get();
				for (AttachmentModel attachment : attachments) {
					if (attachment.getTagIds().contains(STATEMENT.tagId())) {
						statementService.deleteStatementAttachments(ti.get().getTaskDefinitionKey(), statementId,
								attachment.getId(), AuthorizationRuleActions.C_IS_CLAIMED_BY_USER);
					}
				}
			}
		}
		Optional<TblAttachment> oAttachment = statementService.createStatementTblAttachment(statementId, fileName,
				fileType, inputStream, length);
		if (oAttachment.isPresent()) {
			TblAttachment attachment = oAttachment.get();
			Set<String> allTagIds = statementService.setTags(attachment.getId(), tagIds);
			AttachmentModel model = new AttachmentModel();
			model.setId(attachment.getId());
			model.setName(attachment.getFileName());
			model.setSize(attachment.getSize());
			TypeConversion.iso8601InstantStringOfLocalDateTimeUTC(attachment.getTimestamp())
					.ifPresent(model::setTimestamp);
			model.setType(attachment.getFileType());
			model.setTagIds(new ArrayList<>(allTagIds));
			return Optional.of(model);

		}
		return Optional.empty();
	}

	public void deleteStatementAttachments(Long statementId, String taskId, Long attachmentId)
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {
		Optional<TaskInfo> ti = getTaskInfo(statementId, taskId);
		if (!ti.isPresent()) {
			throw new NotFoundServiceException(INVALID_TASK_ID);
		}
		authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
		authorizationService.authorize(ti.get().getTaskDefinitionKey(),
				AuthorizationRuleActions.A_MANIPULATE_ATTACHMENTS, AuthorizationRuleActions.C_IS_CLAIMED_BY_USER);

		statementService.deleteStatementAttachments(ti.get().getTaskDefinitionKey(), statementId, attachmentId,
				AuthorizationRuleActions.C_IS_CLAIMED_BY_USER);

	}

	public MailSendReport dispatchStatementResponseAndCompleteTask(Long statementId, String taskId)
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {
		Optional<TaskInfo> ti = getTaskInfo(statementId, taskId);
		if (!ti.isPresent()) {
			throw new NotFoundServiceException(INVALID_TASK_ID);
		}
		authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
		authorizationService.authorize(ti.get().getTaskDefinitionKey(),
				AuthorizationRuleActions.A_DISPATCH_STATEMENT_RESPONSE, AuthorizationRuleActions.C_IS_CLAIMED_BY_USER);

		MailSendReport report = statementService.dispatchStatementResponse(statementId);
		if (Boolean.TRUE.equals(report.getSuccessful())) {
			completeStatementTask(statementId, taskId, new HashMap<String, WorkflowTaskVariableModel>());
		}
		return report;
	}

	public AttachmentModel transferMailText(Long statementId, String taskId)
			throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {
		Optional<TaskInfo> ti = getTaskInfo(statementId, taskId);
		if (!ti.isPresent()) {
			throw new NotFoundServiceException(INVALID_TASK_ID);
		}
		authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
		authorizationService.authorize(ti.get().getTaskDefinitionKey(),
				AuthorizationRuleActions.A_MANIPULATE_ATTACHMENTS, AuthorizationRuleActions.C_IS_CLAIMED_BY_USER);

		return statementService.transferMailTextToAttachment(statementId);
	}

	public List<AttachmentModel> transferMailAttachments(Long statementId, String taskId,
			List<MailTransferAttachment> transferAttachments)
			throws ForbiddenServiceException, NotFoundServiceException, InternalErrorServiceException {

		Optional<TaskInfo> ti = getTaskInfo(statementId, taskId);
		if (!ti.isPresent()) {
			throw new NotFoundServiceException(INVALID_TASK_ID);
		}
		authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
		authorizationService.authorize(ti.get().getTaskDefinitionKey(),
				AuthorizationRuleActions.A_MANIPULATE_ATTACHMENTS, AuthorizationRuleActions.C_IS_CLAIMED_BY_USER);

		return statementService.transferMailAttachments(statementId, transferAttachments);
	}

}
