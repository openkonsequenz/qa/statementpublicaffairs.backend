/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.model.conf.EvaluationResult;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.util.CsvReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;

/**
 * @author Tobias Stummer
 *
 */
@Log
@Service
public class StatementAuthorizationService {

	@Autowired
	private UserInfoService userinfoservice;

	@Value("${statement.authorization.path}")
	private String ressourcePath;

	@Value("${statement.authorization.csv-delimiter}")
	private String delimiter;

	private Set<Rule> ruleSet = new HashSet<>();

	@PostConstruct
	public void inid() throws IOException {
		parseRuleConfig();
	}

	private void parseRuleConfig() throws IOException {
		ruleSet.clear();
		for (String[] line : CsvReader.parseCsvFile(ressourcePath, delimiter)) {
			Rule.ofParameters(line).ifPresent(ruleSet::add);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("### Authorization Rules BEGIN\n");
		for (Rule rule : ruleSet) {
			sb.append(rule.getTaskDefinition() + ";" + rule.getRole() + ";" + rule.getAction() + ";"
					+ rule.getOptionalConstraint() + "\n");
		}
		sb.append("### Authorization Rules END");
		log.info(sb.toString());
	}

	public void authorize(String taskDefinitionKey, String action) throws ForbiddenServiceException {
		authorize(taskDefinitionKey, action, Rule.ANY);
	}

	/**
	 * @param taskDefinitionKey
	 * @param string
	 */
	public void authorize(String taskDefinitionKey, String action, String optionalConstraintValue)
			throws ForbiddenServiceException {
		String[] roles = userinfoservice.getUserRoles();
		String user = userinfoservice.getUserName();
		String logString = String.join("; ", taskDefinitionKey, "[" + String.join(":", roles) + "]", action,
				optionalConstraintValue);
		if (hasMatchingRule(taskDefinitionKey, roles, action, optionalConstraintValue)) {
			log.info("[I-AUTHORIZE REQ] ; " + user + " ;" + logString);
			return;
		}
		log.warning("[W-AUTHORIZE REQ] ; " + user + " ;" + logString);
		throw new ForbiddenServiceException("Action not authorized. Check authorization rules");
	}

	private boolean hasMatchingRule(String taskDefinitionKey, String[] roles, String action,
			String optionalConstraintValue) {

		Rule checkRule = new Rule();
		checkRule.setTaskDefinition(taskDefinitionKey);
		checkRule.setAction(action);
		checkRule.setOptionalConstraint(optionalConstraintValue);
		for (String role : roles) {
			checkRule.setRole(role);
			if (ruleSet.contains(checkRule)) {
				return true;
			}
		}
		return false;
	}

	public void authorizeByClaimedUser(String assignee) throws ForbiddenServiceException {
		if (Boolean.FALSE.equals(userinfoservice.isOwnUserName(assignee))) {
			throw new ForbiddenServiceException("Task is not claimed by requesting user");
		}
	}

	/**
	 * Evaluate general task claim authorization based on the given
	 * taskDefinitionKey.
	 * 
	 * @param taskDefinitionKey task definition key
	 * @return DENIED if no rule entry with matching taskDefinition and userRoles is
	 *         configured. ONLY_REQ_DEPARTMENT_USER if SPA_DIVISION_MEMBER is the
	 *         only matching user role. Otherwise ACCEPTED.
	 */
	public EvaluationResult evaluateAuthorization(String taskDefinitionKey) {

		Set<String> userRoles = new HashSet<>(Arrays.asList(userinfoservice.getUserRoles()));

		Set<String> matchingRoles = new HashSet<>();
		for (Rule rule : ruleSet) {
			if (rule.getTaskDefinition().equals(taskDefinitionKey) && userRoles.contains(rule.getRole())) {
				matchingRoles.add(rule.getRole());
			}
		}

		if (matchingRoles.isEmpty()) {
			return EvaluationResult.DENIED;
		}

		if (matchingRoles.size() == 1 && matchingRoles.contains(UserRoles.SPA_DIVISION_MEMBER)) {
			return EvaluationResult.ONLY_REQ_DEPARTMENT_USER;
		}
		return EvaluationResult.ACCEPTED;
	}

}
