/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.repository;

import java.time.LocalDate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.eclipse.openk.statementpublicaffairs.model.SearchParams;
import org.eclipse.openk.statementpublicaffairs.model.db.VwUserStatementSearch;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class VwUserStatementSearchSpecificationTest {

	@Test
	void toPredicateTest() {
		SearchParams searchParams = new SearchParams();

		searchParams.setCity("city");
		searchParams.setCreationDateFrom(TypeConversion.dateStringOfLocalDate(LocalDate.now()).get());
		searchParams.setCreationDateTo(TypeConversion.dateStringOfLocalDate(LocalDate.now()).get());
		searchParams.setDistrict("district");
		searchParams.setDueDateFrom(TypeConversion.dateStringOfLocalDate(LocalDate.now()).get());
		searchParams.setDueDateTo(TypeConversion.dateStringOfLocalDate(LocalDate.now()).get());
		searchParams.setFinished(true);
		searchParams.setReceiptDateFrom(TypeConversion.dateStringOfLocalDate(LocalDate.now()).get());
		searchParams.setReceiptDateTo(TypeConversion.dateStringOfLocalDate(LocalDate.now()).get());
		searchParams.setSearchStrings(new String[] { "" });
		searchParams.setTypeId(1L);

		Long userId = 1L;
		VwUserStatementSearchSpecification searchSpec = new VwUserStatementSearchSpecification(searchParams, userId);
		// The criteria root is the tag entity.
		Root<VwUserStatementSearch> root = Mockito.mock(Root.class);
		CriteriaQuery<?> query = Mockito.mock(CriteriaQuery.class);
		CriteriaBuilder criteriaBuilder = Mockito.mock(CriteriaBuilder.class);
		searchSpec.toPredicate(root, query, criteriaBuilder);
	}

}
