/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.service.ContactService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ContactModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ContactsControllerTest {

	@MockBean
	private ContactService contactService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void searchContactsWithSearchQueryShouldRespondWithContactModels() throws Exception {

		String q = "search query";

		String id = "id";
		String companyId = "companyId";
		String companyName = "companyName";
		String email = "email";
		String firstName = "firstName";
		String lastName = "lastName";
		List<ContactModel> contacts = new ArrayList<>();
		ContactModel contact = new ContactModel();
		contact.setCompanyId(companyId);
		contact.setCompanyName(companyName);
		contact.setEmail(email);
		contact.setFirstName(firstName);
		contact.setId(id);
		contact.setLastName(lastName);
		contacts.add(contact);

		Mockito.when(contactService.searchContacts(Mockito.eq(q), Mockito.any(Pageable.class)))
				.thenReturn(new PageImpl<ContactModel>(contacts));

		mockMvc.perform(get("/contacts?q=" + q)).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("content[0].companyId", is(companyId))).andExpect(jsonPath("content[0].id", is(id)))
				.andExpect(jsonPath("content[0].companyName", is(companyName)))
				.andExpect(jsonPath("content[0].email", is(email)))
				.andExpect(jsonPath("content[0].firstName", is(firstName)))
				.andExpect(jsonPath("content[0].lastName", is(lastName)));

	}

	@Test
	void searchContactsWithSearchQueryInternalErrorServiceExceptionSearchContactsShouldRespondWithInternalServerError()
			throws Exception {
		String q = "search query";

		Mockito.doThrow(new InternalErrorServiceException()).when(contactService).searchContacts(Mockito.eq(q),
				Mockito.any(Pageable.class));

		mockMvc.perform(get("/contacts?q=" + q)).andExpect(status().is(500));
	}

	@Test
	void searchContactsWithSearchQueryForbiddenServiceExceptionShouldRespondWithForbidden() throws Exception {
		String q = "search query";

		Mockito.doThrow(new ForbiddenServiceException()).when(contactService).searchContacts(Mockito.eq(q),
				Mockito.any(Pageable.class));

		mockMvc.perform(get("/contacts?q=" + q)).andExpect(status().is(403));
	}

	@Test
	void getContactDetailsWithValidContactIdShouldRespondWithContactBlock() throws Exception {

		String contactId = "contactId";

		String community = "community";
		String communitySuffix = "communitySuffix";
		String company = "company";
		String email = "email";
		String firstName = "firstName";
		String houseNumber = "houseNumber";
		String lastName = "lastName";
		String postCode = "postCode";
		String salutation = "salutation";
		String street = "street";
		String title = "title";

		CompanyContactBlockModel contactBlock = new CompanyContactBlockModel();
		contactBlock.setCommunity(community);
		contactBlock.setCommunitySuffix(communitySuffix);
		contactBlock.setCompany(company);
		contactBlock.setEmail(email);
		contactBlock.setFirstName(firstName);
		contactBlock.setHouseNumber(houseNumber);
		contactBlock.setLastName(lastName);
		contactBlock.setPostCode(postCode);
		contactBlock.setSalutation(salutation);
		contactBlock.setStreet(street);
		contactBlock.setTitle(title);

		Mockito.when(contactService.getContactDetails(Mockito.eq(contactId), Mockito.eq(false)))
				.thenReturn(Optional.of(contactBlock));

		mockMvc.perform(get("/contacts/" + contactId)).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("community", is(community)))
				.andExpect(jsonPath("communitySuffix", is(communitySuffix))).andExpect(jsonPath("company", is(company)))
				.andExpect(jsonPath("email", is(email))).andExpect(jsonPath("firstName", is(firstName)))
				.andExpect(jsonPath("houseNumber", is(houseNumber))).andExpect(jsonPath("lastName", is(lastName)))
				.andExpect(jsonPath("postCode", is(postCode))).andExpect(jsonPath("salutation", is(salutation)))
				.andExpect(jsonPath("street", is(street))).andExpect(jsonPath("title", is(title)));
	}

	@Test
	void getContactDetailsWithInvalidContactIdShouldRespondWithNotFound() throws Exception {

		String contactId = "contactId";

		Mockito.when(contactService.getContactDetails(Mockito.eq(contactId), Mockito.eq(false)))
				.thenReturn(Optional.empty());

		mockMvc.perform(get("/contacts/" + contactId)).andExpect(status().is(404));
	}

	@Test
	void getContactDetailsWithValidContactIdInternalErrorServiceExceptionShouldRespondWithInternalServerError()
			throws Exception {

		String contactId = "contactId";

		Mockito.doThrow(new InternalErrorServiceException()).when(contactService)
				.getContactDetails(Mockito.eq(contactId), Mockito.eq(false));

		mockMvc.perform(get("/contacts/" + contactId)).andExpect(status().is(500));
	}

	@Test
	void getContactDetailsWithValidContactIdForbiddenServiceExceptionShouldRespondWithForbidden() throws Exception {
		String contactId = "contactId";

		Mockito.doThrow(new ForbiddenServiceException()).when(contactService).getContactDetails(Mockito.eq(contactId),
				Mockito.eq(false));

		mockMvc.perform(get("/contacts/" + contactId)).andExpect(status().is(403));
	}

}
