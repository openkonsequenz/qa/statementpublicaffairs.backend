/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConflictServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.UnprocessableEntityServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.service.SessionService;
import org.eclipse.openk.statementpublicaffairs.service.StatementProcessService;
import org.eclipse.openk.statementpublicaffairs.service.StatementService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

/**
 * Test StatementController REST endpoints.
 * 
 * @author Tobias Stummer
 *
 */
@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class StatementControllerTest {

	@MockBean
	private StatementService statementService;

	@MockBean
	private StatementProcessService statementProcessService;

	@MockBean
	private SessionService sessionService;

	@Autowired
	private MockMvc mockMvc;

	/**
	 * Test GET endpoint /statements/{statementId} interface with valid statementId.
	 * 
	 * @throws Exception
	 */
	@Test
	void statementGetRequestWithValidStatementIdShouldRespondWithStatementDetilsModel() throws Exception {
		StatementDetailsModel model = new StatementDetailsModel();
		Long id = 4711L;
		String dueDate = "2020-04-30";
		String receiptDate = "2020-04-10";
		String businessKey = "businessKey";
		String title = "title";
		String city = "City";
		String district = "District";
		model.setId(id);
		model.setFinished(false);
		model.setDueDate(dueDate);
		model.setReceiptDate(receiptDate);
		model.setTitle(title);
		model.setBusinessKey(businessKey);
		model.setDistrict(district);
		model.setCity(city);
		model.setTypeId(1L);
		Mockito.when(statementService.getStatement(id)).thenReturn(Optional.of(model));
		mockMvc.perform(get("/statements/4711")).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("id", is(4711)))
				.andExpect(jsonPath("finished", is(false))).andExpect(jsonPath("dueDate", is(dueDate)))
				.andExpect(jsonPath("receiptDate", is(receiptDate))).andExpect(jsonPath("title", is(title)))
				.andExpect(jsonPath("city", is(city))).andExpect(jsonPath("district", is(district)))
				.andExpect(jsonPath("typeId", is(1))).andExpect(jsonPath("businessKey", is(businessKey)));
	}

	/**
	 * Test GET endpoint /statements/{statementId} interface with invalid
	 * statementId.
	 * 
	 * @throws Exception
	 */
	@Test
	void statementGetRequestWithInValidStatementIdShouldRespondWithNotFoundResponse() throws Exception {
		Mockito.when(statementService.getStatement(1234L)).thenReturn(Optional.empty());
		mockMvc.perform(get("/statements/1234")).andExpect(status().is(404));
	}

	
	/**
	 * Test POST endpoint /statements/ interface with valid statement.
	 * 
	 * @throws Exception
	 */
	@Test
	void statementCreateRequestWithValidContentRespondsWithOkAndCreateStatementResponse() throws Exception {
		String dueDate = "2020-04-30";
		String receiptDate = "2020-04-10";
		String title = "Title Text.";
		String city = "City";
		String district = "District";
		Long statementTypeId = 1L;
		String newstatementjson = "{\"dueDate\":\"" + dueDate + "\", \"receiptDate\":\"" + receiptDate
				+ "\", \"title\":\"" + title + "\", \"city\":\"" + city + "\" , \"district\":\"" + district
				+ "\" , \"typeId\":\"" + statementTypeId + "\" }";

		Mockito.when(statementProcessService.createStatement(Mockito.any(StatementDetailsModel.class)))
				.thenAnswer(new Answer<Optional<StatementDetailsModel>>() {
					@Override
					public Optional<StatementDetailsModel> answer(InvocationOnMock invocation) throws Throwable {
						Object[] args = invocation.getArguments();
						if (args[0] instanceof StatementDetailsModel) {
							StatementDetailsModel model = (StatementDetailsModel) args[0];
							if (dueDate.equals(model.getDueDate()) && title.equals(model.getTitle())
									&& statementTypeId.equals(model.getTypeId()) && district.equals(model.getDistrict())
									&& receiptDate.equals(model.getReceiptDate()) && city.equals(model.getCity())) {
								model.setId(1234L);
								model.setFinished(false);
								return Optional.of(model);
							}
						}
						return Optional.empty();
					}
				});
		mockMvc.perform(post("/statements").content(newstatementjson).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(200))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("id", is(1234)))
				.andExpect(jsonPath("finished", is(false))).andExpect(jsonPath("dueDate", is(dueDate)))
				.andExpect(jsonPath("receiptDate", is(receiptDate))).andExpect(jsonPath("city", is(city)))
				.andExpect(jsonPath("district", is(district))).andExpect(jsonPath("typeId", is(1)))
				.andExpect(jsonPath("title", is(title)));
	}

	@Test
	void statementGetAttachementsForStatementWithValidStatementIdShouldRespondWithListOfAttachmentModels()
			throws Exception {
		List<AttachmentModel> attachments = new ArrayList<>();
		AttachmentModel am1 = new AttachmentModel();
		am1.setId(1L);
		attachments.add(am1);
		AttachmentModel am2 = new AttachmentModel();
		am2.setId(2L);
		attachments.add(am2);
		Optional<List<AttachmentModel>> oAttachments = Optional.of(attachments);
		Mockito.when(statementService.getStatementAttachments(1234L)).thenReturn(oAttachments);
		mockMvc.perform(get("/statements/1234/attachments")).andExpect(status().is(200))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("[0].id", is(1)))
				.andExpect(jsonPath("[1].id", is(2)));
	}

	@Test
	void statementGetAttachementsForStatementWithInValidStatementIdShouldRespondWithNotFoundResponse()
			throws Exception {
		Mockito.when(statementService.getStatementAttachments(404L)).thenReturn(Optional.empty());
		mockMvc.perform(get("/statements/404/attachments")).andExpect(status().is(404));
	}

	@Test
	void statementAddConsiderationForStatementWithValidStatementIdShouldRespondWithAttachmentModelContainingTheAttachmentId()
			throws Exception {
		AttachmentModel model = new AttachmentModel();
		model.setId(34L);
		model.setName("file.txt");
		model.setType("text/plain");
		byte[] rawFile = "this is some test data.".getBytes();
		Mockito.when(statementService.addConsideration(Mockito.eq(12L), Mockito.any(String.class),
				Mockito.any(String.class), Mockito.any(ByteArrayInputStream.class), Mockito.eq((long) rawFile.length)))
				.thenReturn(Optional.of(model));
		MockMultipartFile attachmentFile = new MockMultipartFile("attachment", "file.txt", "text/plain", rawFile);

		mockMvc.perform(MockMvcRequestBuilders.multipart("/statements/12/consideration").file(attachmentFile))
				.andExpect(status().is(200)).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is(34))).andExpect(jsonPath("type", is("text/plain")))
				.andExpect(jsonPath("name", is("file.txt")));
	}

	@Test
	void statementAddAttachmentForStatementWithInValidStatementIdShouldRespondWithNotFoundResponse() throws Exception {
		byte[] rawFile = "this is some test data.".getBytes();
		Mockito.when(statementService.addConsideration(Mockito.eq(404L), Mockito.any(String.class),
				Mockito.any(String.class), Mockito.any(ByteArrayInputStream.class), Mockito.eq((long) rawFile.length)))
				.thenReturn(Optional.empty());
		MockMultipartFile attachmentFile = new MockMultipartFile("attachment", "file.txt", "text/plain", rawFile);
		mockMvc.perform(MockMvcRequestBuilders.multipart("/statements/404/attachments").file(attachmentFile))
				.andExpect(status().is(404));
	}


	@Test
	void statementGetAttachmentForStatementWithValidStatementIdAndAttachmentIdShouldRespondWithAttachmentModel()
			throws Exception {
		AttachmentModel model = new AttachmentModel();
		model.setId(34L);
		model.setName("file.txt");
		model.setType("text/plain");
		Mockito.when(statementService.getStatementAttachment(12L, 34L)).thenReturn(Optional.of(model));
		mockMvc.perform(get("/statements/12/attachments/34")).andExpect(status().is(200))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("id", is(34)))
				.andExpect(jsonPath("type", is("text/plain"))).andExpect(jsonPath("name", is("file.txt")));
	}

	@Test
	void statementGetAttachmentForStatementWithValidStatementIdAndInvalidAttachmentIdShouldRespondWithNotFoundResponse()
			throws Exception {
		Mockito.when(statementService.getStatementAttachment(12L, 404L)).thenReturn(Optional.empty());
		mockMvc.perform(get("/statements/12/attachments/404")).andExpect(status().is(404));
	}

	@Test
	void statementGetAttachmentForStatementWithInValidStatementIdShouldRespondWithNotFoundResponse() throws Exception {
		Mockito.when(statementService.getStatementAttachment(404L, 34L)).thenReturn(Optional.empty());
		mockMvc.perform(get("/statements/404/attachments/34")).andExpect(status().is(404));
	}

	@Test
	void statementGetAttachmentFileForStatementWithValidStatementIdShouldRespondWithMultipartResponse()
			throws Exception {
		String name = "test.txt";
		String type = "text/plain";
		byte[] rawFile = "{this is some test data.".getBytes();
		AttachmentFile af = new AttachmentFile();
		af.setName(name);
		af.setType(type);
		af.setLength(rawFile.length);
		af.setRessource(new ByteArrayInputStream(rawFile));
		Mockito.when(sessionService.hasRoles(Mockito.any(), Mockito.eq("testToken"))).thenReturn(true);
		Mockito.when(statementService.getStatementAttachmentFile(12L, 34L)).thenReturn(Optional.of(af));
		mockMvc.perform(get("/statements/12/attachments/34/file").param("accessToken", "testToken"))
				.andExpect(status().is(200)).andExpect(content().contentType(MediaType.TEXT_PLAIN))
				.andExpect(content().bytes(rawFile))
				.andExpect(header().string("Content-Disposition", "inline; filename=\"" + name + "\""));
	}

	@Test
	void addTagWithNewTagLabelShouldRespondWithOkCreated() throws Exception {
		String label = "label";
		mockMvc.perform(put("/tags?label=" + label)).andExpect(status().is(201));
	}

	@Test
	void addTagWithAlreadyExistingTagLabelConflictServiceExceptionShouldRespondWithConflict() throws Exception {
		String label = "label";
		Mockito.doThrow(new ConflictServiceException()).when(statementService).addTag(Mockito.eq(label));
		mockMvc.perform(put("/tags?label=" + label)).andExpect(status().is(409));
	}

	@Test
	void addTagWithNewTagLabelForbiddenServiceExcetionShouldRespondWithForbidden() throws Exception {
		String label = "label";
		Mockito.doThrow(new ForbiddenServiceException()).when(statementService).addTag(Mockito.eq(label));
		mockMvc.perform(put("/tags?label=" + label)).andExpect(status().is(403));
	}

	@Test
	void addTagWithNewTagLabelInternalErrorServiceExceptionShouldRespondWithInternalServerError() throws Exception {
		String label = "label";
		Mockito.doThrow(new InternalErrorServiceException()).when(statementService).addTag(Mockito.eq(label));
		mockMvc.perform(put("/tags?label=" + label)).andExpect(status().is(500));
	}

	@Test
	void getStatementContactWithValidStatementIdShouldRespondContactModel() throws Exception {

		Long statementId = 1234L;
		String firstName = "firstName";

		CompanyContactBlockModel contact = new CompanyContactBlockModel();
		contact.setFirstName(firstName);

		Mockito.when(statementService.getContactBlock(Mockito.eq(statementId))).thenReturn(Optional.of(contact));
		mockMvc.perform(get("/statements/" + statementId + "/contact")).andExpect(status().is(200))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("firstName", is(firstName)));

	}

	@Test
	void getStatementContactWithInvalidStatementIdShouldRespondNotFound() throws Exception {
		Long statementId = 404L;

		Mockito.when(statementService.getContactBlock(Mockito.eq(statementId))).thenReturn(Optional.empty());
		mockMvc.perform(get("/statements/" + statementId + "/contact")).andExpect(status().is(404));
	}

	@Test
	void getStatementContactWithValidStatementIdForbiddenServiceExceptionShouldRespondInternalServerError()
			throws Exception {
		Long statementId = 1234L;

		Mockito.doThrow(new ForbiddenServiceException()).when(statementService)
				.getContactBlock(Mockito.eq(statementId));
		mockMvc.perform(get("/statements/" + statementId + "/contact")).andExpect(status().is(500));

	}

	@Test
	void getStatementContactWithValidStatementIdInternalErrorServiceExceptionShouldRespondInternalServerError()
			throws Exception {
		Long statementId = 1234L;

		Mockito.doThrow(new InternalErrorServiceException()).when(statementService)
				.getContactBlock(Mockito.eq(statementId));
		mockMvc.perform(get("/statements/" + statementId + "/contact")).andExpect(status().is(500));

	}

	@Test
	void getSectorsShouldRespondSectors() throws Exception {

		Map<String, List<String>> sectors = new HashMap<>();
		List<String> testSectors = new ArrayList<>();
		testSectors.add("Sparte");
		sectors.put("Ort#Ortsteil", testSectors);
		Mockito.when(statementService.getAllSectors()).thenReturn(sectors);
		mockMvc.perform(get("/sectors")).andExpect(status().is(200))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("Ort#Ortsteil[0]", is("Sparte")));

	}

	@Test
	void getStatementSectorsShouldRespondSectors() throws Exception {
		Long statementId = 1234L;
		Map<String, List<String>> sectors = new HashMap<>();
		List<String> testSectors = new ArrayList<>();
		testSectors.add("Sparte");
		sectors.put("Ort#Ortsteil", testSectors);
		Mockito.when(statementService.getAllSectors(Mockito.eq(statementId))).thenReturn(sectors);
		mockMvc.perform(get("/statements/" + statementId + "/sectors")).andExpect(status().is(200))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("Ort#Ortsteil[0]", is("Sparte")));

	}
	
	@Test
	void getStatementByIdInternalErrorServiceExceptionRespondsWithInternalServerError() throws Exception {
		Mockito.doThrow(new InternalErrorServiceException()).when(statementService).getStatement(Mockito.anyLong());
		mockMvc.perform(get("/statements/1")).andExpect(status().is(500));
	}

	@Test
	void getStatementByIdForbiddenServiceExceptionRespondsWithForbidden() throws Exception {
		Mockito.doThrow(new ForbiddenServiceException()).when(statementService).getStatement(Mockito.anyLong());
		mockMvc.perform(get("/statements/1")).andExpect(status().is(403));
	}

	@Test
	void createStatementForbiddenServiceExceptionRespondsWithForbidden() throws Exception {
		Mockito.doThrow(new ForbiddenServiceException()).when(statementProcessService).createStatement(Mockito.any(StatementDetailsModel.class));
		mockMvc.perform(post("/statements").content("{}").contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(403));
	}

	@Test
	void createStatementUnprocessableEntityServiceExceptiondRespondsWithUnprocessableEntity() throws Exception {
		Mockito.doThrow(new UnprocessableEntityServiceException()).when(statementProcessService).createStatement(Mockito.any(StatementDetailsModel.class));
		mockMvc.perform(post("/statements").content("{}").contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(422));
	}

	@Test
	void createStatementBadRequestServiceExceptionRespondsWithBadRequest() throws Exception {
		Mockito.doThrow(new BadRequestServiceException()).when(statementProcessService).createStatement(Mockito.any(StatementDetailsModel.class));
		mockMvc.perform(post("/statements").content("{}").contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(400));
	}

	@Test
	void createStatementInternalErrorServiceExceptionRespondsWithInternalServerError() throws Exception {
		InternalErrorServiceException e = new InternalErrorServiceException("", new Exception(""));
		Mockito.doThrow(e).when(statementProcessService).createStatement(Mockito.any(StatementDetailsModel.class));
		mockMvc.perform(post("/statements").content("{}").contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(500));
	}

	@Test
	void createStatementNotFoundServiceExceptionRespondsWithNotFound() throws Exception {
		Mockito.doThrow(new NotFoundServiceException()).when(statementProcessService).createStatement(Mockito.any(StatementDetailsModel.class));
		mockMvc.perform(post("/statements").content("{}").contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(400));
	}
	
	@Test
	void getStatementAttachmentsForbiddenServiceExceptionRespondsWithForbidden() throws Exception {
		Mockito.doThrow(new ForbiddenServiceException()).when(statementService).getStatementAttachments(Mockito.anyLong());
		mockMvc.perform(get("/statements/1/attachments")).andExpect(status().is(403));	
	}
	
	@Test
	void getStatementAttachmentForbiddenServiceExceptionRespondsWithForbidden() throws Exception {
		Mockito.doThrow(new ForbiddenServiceException()).when(statementService).getStatementAttachment(Mockito.anyLong(), Mockito.anyLong());
		mockMvc.perform(get("/statements/1/attachments/2")).andExpect(status().is(403));	
	}
}
