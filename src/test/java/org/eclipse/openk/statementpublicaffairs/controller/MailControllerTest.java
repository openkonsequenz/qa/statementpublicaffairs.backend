/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailEntry;
import org.eclipse.openk.statementpublicaffairs.service.MailService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class MailControllerTest {

	@MockBean
	private MailService mailService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void listAllMailsOfInbox() throws Exception {
		String from = "from";
		String identifier = "identifier";
		String subject = "subject";
		String textPlain = "textPlain";
		List<MailEntry> mailEntries = new ArrayList<>();
		MailEntry mailEntry = new MailEntry();
		mailEntry.setFrom(from);
		mailEntry.setIdentifier(identifier);
		mailEntry.setSubject(subject);
		mailEntry.setTextPlain(textPlain);
		mailEntries.add(mailEntry);

		Mockito.when(mailService.getCurrentStatementMailInbox()).thenReturn(mailEntries);
		mockMvc.perform(get("/mail/inbox")).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("[0].from", is(from)))
				.andExpect(jsonPath("[0].identifier", is(identifier))).andExpect(jsonPath("[0].subject", is(subject)))
				.andExpect(jsonPath("[0].textPlain", is(textPlain)));
	}

	@Test
	void deleteMail() throws Exception {
		mockMvc.perform(delete("/mail/inbox/messageId")).andExpect(status().is2xxSuccessful());

		Mockito.doThrow(new NotFoundServiceException()).when(mailService).deleteMail(Mockito.eq("messageId"));
		mockMvc.perform(delete("/mail/inbox/messageId")).andExpect(status().is(404));

		Mockito.doThrow(new InternalErrorServiceException()).when(mailService).deleteMail(Mockito.eq("messageId"));
		mockMvc.perform(delete("/mail/inbox/messageId")).andExpect(status().is(500));

	}

	@Test
	void moveMailToProcessed() throws Exception {
		mockMvc.perform(patch("/mail/inbox/messageId")).andExpect(status().is2xxSuccessful());

		Mockito.doThrow(new NotFoundServiceException()).when(mailService)
				.moveMailFromInboxToStatements(Mockito.eq("messageId"));
		mockMvc.perform(patch("/mail/inbox/messageId")).andExpect(status().is(404));

		Mockito.doThrow(new InternalErrorServiceException()).when(mailService)
				.moveMailFromInboxToStatements(Mockito.eq("messageId"));
		mockMvc.perform(patch("/mail/inbox/messageId")).andExpect(status().is(500));
	}

	@Test
	void getMail() throws Exception {
		MailEntry mailEntry = new MailEntry();
		String date = "date";
		String from = "from";
		String identifier = "identifier";
		String subject = "subject";
		String textPlain = "textPlain";
		String textHtml = "textHtml";
		;
		String name = "name";
		String type = "type";
		Long size = 123L;
		mailEntry.setDate(date);
		mailEntry.setFrom(from);
		mailEntry.setIdentifier(identifier);
		mailEntry.setSubject(subject);
		mailEntry.setTextPlain(textPlain);
		mailEntry.setTextHtml(textHtml);
		List<AttachmentModel> attachments = new ArrayList<>();
		AttachmentModel attachment = new AttachmentModel();
		attachment.setName(name);
		attachment.setType(type);
		attachment.setSize(size);
		attachments.add(attachment);
		mailEntry.setAttachments(attachments);
		Mockito.when(mailService.getMail(Mockito.eq("messageId"))).thenReturn(Optional.of(mailEntry));
		mockMvc.perform(get("/mail/identifier/messageId")).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("date", is(date)))
				.andExpect(jsonPath("from", is(from))).andExpect(jsonPath("identifier", is(identifier)))
				.andExpect(jsonPath("subject", is(subject))).andExpect(jsonPath("textPlain", is(textPlain)))
				.andExpect(jsonPath("textHtml", is(textHtml))).andExpect(jsonPath("attachments[0].name", is(name)))
				.andExpect(jsonPath("attachments[0].type", is(type)))
				.andExpect(jsonPath("attachments[0].size", is(size.intValue())));

		Mockito.when(mailService.getMail(Mockito.eq("messageId"))).thenReturn(Optional.empty());
		mockMvc.perform(get("/mail/identifier/messageId")).andExpect(status().is(404));

	}

	@Test
	void getMailAttachment() throws Exception {
		String messageId = "messageId";
		String fileName = "filename";

		Map<String, AttachmentFile> attachmentFiles = new HashMap<>();
		AttachmentFile attachmentFile = new AttachmentFile();

		String attachmentContent = "Das ist ein test";
		byte[] bytes = attachmentContent.getBytes();
		ByteArrayInputStream is = new ByteArrayInputStream(bytes);
		attachmentFile.setLength(bytes.length);
		attachmentFile.setName(fileName);
		attachmentFile.setType("text/plain");
		attachmentFile.setRessource(is);
		attachmentFiles.put(fileName, attachmentFile);
		Mockito.when(mailService.getStatementInboxMailAttachment(Mockito.eq(messageId), Mockito.any()))
				.thenReturn(attachmentFiles);
		mockMvc.perform(get("/mail/identifier/" + messageId + "/" + fileName)).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.TEXT_PLAIN)).andExpect(content().bytes(bytes))
				.andExpect(header().string("Content-Disposition", "attachment; filename=\"" + fileName + "\""));
	}

}
