/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.FailedDependencyException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;
import org.eclipse.openk.statementpublicaffairs.model.TextblockGroup;
import org.eclipse.openk.statementpublicaffairs.model.TextblockItem;
import org.eclipse.openk.statementpublicaffairs.model.UserAdminModel;
import org.eclipse.openk.statementpublicaffairs.model.UserAdminSettingsModel;
import org.eclipse.openk.statementpublicaffairs.service.AdminService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DistrictDepartmentsModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class AdminControllerTest {

	@MockBean
	private AdminService adminService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void syncUsers() throws Exception {

		mockMvc.perform(post("/admin/users-sync")).andExpect(status().is2xxSuccessful());
		Mockito.verify(adminService).syncUsers();

	}
	
	@Test
	void syncUsersInternalErrorServiceExceptionShouldRespondInternalServerError() throws Exception {
		Mockito.doThrow(new InternalErrorServiceException()).when(adminService).syncUsers();
		mockMvc.perform(post("/admin/users-sync")).andExpect(status().is(500));
	}

	@Test
	void syncUsersForbidenServiceExceptionShouldRespondForbidden() throws Exception {
		Mockito.doThrow(new ForbiddenServiceException()).when(adminService).syncUsers();
		mockMvc.perform(post("/admin/users-sync")).andExpect(status().is(403));
	}

	@Test
	void getDepartments() throws Exception {

		Map<String, DistrictDepartmentsModel> definition = new HashMap<>();
		String location = "City#Department";
		DistrictDepartmentsModel model = new DistrictDepartmentsModel();
		List<String> provides = new ArrayList<>();
		provides.add("SecA");
		provides.add("SecB");
		model.setProvides(provides);
		Map<String, Set<String>> departments = new HashMap<>();
		Set<String> names = new HashSet<>();
		names.add("DepA");
		names.add("DepB");
		String group = "DepGroupA";
		departments.put(group, names);
		model.setDepartments(departments);
		definition.put(location, model);

		Mockito.when(adminService.getDepartmentStructure()).thenReturn(definition);
		mockMvc.perform(get("/admin/departments")).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath(location + ".provides[0]", is("SecA")))
				.andExpect(jsonPath(location + ".departments.DepGroupA[0]", is("DepA")));
	}
	
	@Test
	void setDepartments() throws Exception {
		String content = "{}";
		mockMvc.perform(put("/admin/departments").contentType(MediaType.APPLICATION_JSON).content(content)).andExpect(status().is2xxSuccessful());
		Mockito.verify(adminService).setDepartmentStructure(Mockito.anyMap());
	}
	
	
	@Test
	void setDepartmentsBadRequestServiceExceptionShouldRespondBadRequest() throws Exception {
		String content = "{}";
		Mockito.doThrow(new BadRequestServiceException()).when(adminService).setDepartmentStructure(Mockito.anyMap());
		mockMvc.perform(put("/admin/departments").contentType(MediaType.APPLICATION_JSON).content(content)).andExpect(status().is(400));
	}

	@Test
	void setDepartmentsForbidenServiceExceptionShouldRespondForbidden() throws Exception {
		String content = "{}";
		Mockito.doThrow(new ForbiddenServiceException()).when(adminService).setDepartmentStructure(Mockito.anyMap());
		mockMvc.perform(put("/admin/departments").contentType(MediaType.APPLICATION_JSON).content(content)).andExpect(status().is(403));
	}

	
	
	@Test
	void getTextblockDefinition() throws Exception {
	
		TextblockDefinition tbd = new TextblockDefinition();
		List<TextblockGroup> groups = new ArrayList<>();
		TextblockGroup group = new TextblockGroup();
		List<TextblockItem> textblocks = new ArrayList<>();
		TextblockItem textblock = new TextblockItem();
		textblock.setId("textBlockId");
		textblocks.add(textblock );
		group.setTextBlocks(textblocks );
		group.setGroupName("groupName");
		groups.add(group);
		tbd.setGroups(groups);
		Mockito.when(adminService.getTextblockDefinition()).thenReturn(tbd);
		mockMvc.perform(get("/admin/textblockconfig")).andExpect(status().is2xxSuccessful())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("groups.[0].groupName", is("groupName")))
		.andExpect(jsonPath("groups.[0].textBlocks[0].id", is("textBlockId")));
		
	}
	
	@Test
	void setTextblockDefinition() throws Exception {
		String content = "{\"groups\":[], \"negativeGroups\":[], \"selects\":{}}";
		mockMvc.perform(put("/admin/textblockconfig").contentType(MediaType.APPLICATION_JSON).content(content)).andExpect(status().is2xxSuccessful());
		Mockito.verify(adminService).setTextblockDefinition(Mockito.any(TextblockDefinition.class));
	}

	
	
	@Test
	void setTextblockDefinitioBadRequestServiceExceptionShouldRespondBadRequest() throws Exception {
		Mockito.doThrow(new BadRequestServiceException()).when(adminService).setTextblockDefinition(Mockito.any());
		String content = "{\"groups\":[], \"negativeGroups\":[], \"selects\":{}}";
		mockMvc.perform(put("/admin/textblockconfig").contentType(MediaType.APPLICATION_JSON).content(content)).andExpect(status().is(400));
	}

	@Test
	void setTextblockDefinitioForbidenServiceExceptionShouldRespondForbidden() throws Exception {
		Mockito.doThrow(new ForbiddenServiceException()).when(adminService).setTextblockDefinition(Mockito.any());
		String content = "{\"groups\":[], \"negativeGroups\":[], \"selects\":{}}";
		mockMvc.perform(put("/admin/textblockconfig").contentType(MediaType.APPLICATION_JSON).content(content)).andExpect(status().is(403));
	}

	
	
	@Test
	void getUsers() throws Exception {

		List<UserAdminModel> users = new ArrayList<>();
		UserAdminModel user = new UserAdminModel();
		Long id = 2L;
		user.setId(id);

		users.add(user);
		Mockito.when(adminService.getAllUsers()).thenReturn(users);
		mockMvc.perform(get("/admin/users")).andExpect(status().is2xxSuccessful())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("[0].id", is(id.intValue())));

	}
	
	@Test
	void getUsersInternalErrorServiceExceptionShouldRespondInternalServerError() throws Exception {
		Mockito.doThrow(new InternalErrorServiceException()).when(adminService).getAllUsers();
		mockMvc.perform(get("/admin/users")).andExpect(status().is(500));
	}
	
	@Test
	void setUserSettings() throws Exception {
		Long userId = 2L;
		String content = "{\"email\":\"asd@sdf.tld\"}";
		mockMvc.perform(post("/admin/users/" + userId + "/settings").contentType(MediaType.APPLICATION_JSON).content(content))
		.andExpect(status().is2xxSuccessful());
		Mockito.verify(adminService).setUserSettings(Mockito.eq(userId), Mockito.any(UserAdminSettingsModel.class));
	}
	
	@Test
	void setUserBadRequestServiceExceptionShouldRespondBadRequest() throws Exception {
		Long userId = 2L;
		String content = "{\"email\":\"asd@sdf.tld\"}";
		
		Mockito.doThrow(new BadRequestServiceException()).when(adminService).setUserSettings(Mockito.eq(userId), Mockito.any());
		mockMvc.perform(post("/admin/users/" + userId + "/settings").contentType(MediaType.APPLICATION_JSON).content(content))
		.andExpect(status().is(400));
	}

	@Test
	void setUserForbiddenServiceExceptionShouldRespondForbidde() throws Exception {
		Long userId = 2L;
		String content = "{\"email\":\"asd@sdf.tld\"}";
		
		Mockito.doThrow(new ForbiddenServiceException()).when(adminService).setUserSettings(Mockito.eq(userId), Mockito.any());
		mockMvc.perform(post("/admin/users/" + userId + "/settings").contentType(MediaType.APPLICATION_JSON).content(content))
		.andExpect(status().is(403));
	}

	@Test
	void setUserFailedDependencyServiceExceptionShouldRespondFailedDependency() throws Exception {
		Long userId = 2L;
		String content = "{\"email\":\"asd@sdf.tld\"}";
		
		Mockito.doThrow(new FailedDependencyException()).when(adminService).setUserSettings(Mockito.eq(userId), Mockito.any());
		mockMvc.perform(post("/admin/users/" + userId + "/settings").contentType(MediaType.APPLICATION_JSON).content(content))
		.andExpect(status().is(424));
	}

	@Test
	void setUserNotFoundServiceExceptionShouldRespondNotFound() throws Exception {
		Long userId = 2L;
		String content = "{\"email\":\"asd@sdf.tld\"}";
		
		Mockito.doThrow(new NotFoundServiceException()).when(adminService).setUserSettings(Mockito.eq(userId), Mockito.any());
		mockMvc.perform(post("/admin/users/" + userId + "/settings").contentType(MediaType.APPLICATION_JSON).content(content))
		.andExpect(status().is(404));
	}

}
