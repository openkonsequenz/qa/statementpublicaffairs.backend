/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationArchiveService;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.Tags;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationArchiveService.class)
@ActiveProfiles("test")
class ArchiveServiceTest {

	@Autowired
	private StatementService statementService;

	@Autowired
	private ArchiveService archiveService;

	@Test
	void archiveStatement() throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {
		Long statementId = 1234L;

		StatementDetailsModel statement = new StatementDetailsModel();
		statement.setId(statementId);
		statement.setFinishedDate(TypeConversion.dateStringOfLocalDate(LocalDate.now()).get());
		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statement));
		List<AttachmentModel> attachmentModels = new ArrayList<>();
		AttachmentModel aM1 = new AttachmentModel();
		aM1.setId(1L);
		List<String> aM1tagIds = new ArrayList<>();
		aM1tagIds.add(Tags.OUTBOX.tagId());
		aM1.setTagIds(aM1tagIds);
		attachmentModels.add(aM1);
		AttachmentModel aM2 = new AttachmentModel();
		aM2.setId(2L);
		List<String> aM2tagIds = new ArrayList<>();
		aM2.setTagIds(aM2tagIds);
		attachmentModels.add(aM2);
		Mockito.when(statementService.getStatementAttachments(statementId)).thenReturn(Optional.of(attachmentModels));

		AttachmentFile attachmentFile1 = new AttachmentFile();
		attachmentFile1.setName("Sent.txt");
		attachmentFile1.setRessource(new ByteArrayInputStream(
				"Sent.txt\nThat is a test file\nShould be in sent-data subfolder".getBytes(StandardCharsets.UTF_8)));

		AttachmentFile attachmentFile2 = new AttachmentFile();
		attachmentFile2.setName("Info.txt");
		attachmentFile2.setRessource(new ByteArrayInputStream(
				"Info.txt\nThat is a test file\nShould be in info-data subfolder".getBytes(StandardCharsets.UTF_8)));

		Mockito.when(statementService.getStatementAttachmentFile(statementId, 1L))
				.thenReturn(Optional.of(attachmentFile1));
		Mockito.when(statementService.getStatementAttachmentFile(statementId, 2L))
				.thenReturn(Optional.of(attachmentFile2));

		archiveService.archiveStatement(statementId, LocalDate.now());
	}

	@Test
	void archiveStatementBaseFolderDoesNotExistThrowNewInternalErrorServiceException()
			throws ForbiddenServiceException, NotFoundServiceException, InternalErrorServiceException {
		Long statementId = 1234L;
		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.empty());

		try {
			archiveService.archiveStatement(statementId, LocalDate.now());
			fail("Should have thrown InternalErrorServiceException");
		} catch (NotFoundServiceException e) {
			// pass
		}
	}

}
