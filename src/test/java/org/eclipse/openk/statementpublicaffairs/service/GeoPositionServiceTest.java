/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationGeoPositionService;
import org.eclipse.openk.statementpublicaffairs.model.Position;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationGeoPositionService.class)
@ActiveProfiles("test")
class GeoPositionServiceTest {

	

	@Autowired
	private GeoPositionService geoPositionService;
	
	@Test
	void transform() {
		
		String from = "EPSG:4326";
		String to = "EPSG:25832";

		Map<String, Position> positions = new HashMap<>();
		
		//nbg-castle-garden
		Position wgsPos1 = new Position();
		wgsPos1.setX(11.074659);
		wgsPos1.setY(49.458501);
		
		// Paris Eiffel Tower
		Position wgsPos2 = new Position();
		wgsPos2.setX(2.294466);
		wgsPos2.setY(48.858263);

		positions.put("nbg-castle-garden", wgsPos1);
		positions.put("paris-eiffel-tower", wgsPos2);
		
		Map<String, Position> transformed = geoPositionService.transform(positions, from, to);
	
		Position nbgCastleUtm32 = transformed.get("nbg-castle-garden");
		Position parisEiffelTowerUtm32= transformed.get("paris-eiffel-tower");
		
		assertNotNull(nbgCastleUtm32);
		
		
		
		assertNotNull(parisEiffelTowerUtm32);
		
		double maxOffsetInMeter = 1.0;
		
		assertTrue(Math.abs(parisEiffelTowerUtm32.getX() - 8301.96) < maxOffsetInMeter);
		assertTrue(Math.abs(parisEiffelTowerUtm32.getY() - 5433414.40) < maxOffsetInMeter);
	
		assertTrue(Math.abs(nbgCastleUtm32.getX() - 650342.97 ) < maxOffsetInMeter);
		assertTrue(Math.abs(nbgCastleUtm32.getY() - 5480496.11) < maxOffsetInMeter);
		
		
	}

}
