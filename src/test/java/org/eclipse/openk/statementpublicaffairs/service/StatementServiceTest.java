/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.eclipse.openk.statementpublicaffairs.api.MailUtil;
import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationStatementService;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConflictServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.UnprocessableEntityServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.model.Tags;
import org.eclipse.openk.statementpublicaffairs.model.db.TblAttachment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblAttachment2Tag;
import org.eclipse.openk.statementpublicaffairs.model.db.TblAttachmentLob;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartmentstructure;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementEditLog;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementtype;
import org.eclipse.openk.statementpublicaffairs.model.db.TblTag;
import org.eclipse.openk.statementpublicaffairs.model.db.TblUser;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailConfiguration;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailEntry;
import org.eclipse.openk.statementpublicaffairs.model.mail.MessageConfiguration;
import org.eclipse.openk.statementpublicaffairs.repository.Attachment2TagRepository;
import org.eclipse.openk.statementpublicaffairs.repository.AttachmentLobRepository;
import org.eclipse.openk.statementpublicaffairs.repository.AttachmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentstructureRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementEditLogRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementtypeRepository;
import org.eclipse.openk.statementpublicaffairs.repository.TagRepository;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DistrictDepartmentsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailSendReport;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailTransferAttachment;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.TagModel;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/**
 * Test verifies SessionService interfaces.
 * 
 * @author Tobias Stummer
 *
 */
@SpringBootTest(classes = TestConfigurationStatementService.class)
@ActiveProfiles("test")

class StatementServiceTest {

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private AttachmentRepository attachmentRepository;

	@Autowired
	private StatementtypeRepository statementTypeRepository;

	@Autowired
	private DepartmentstructureRepository departmentStructureRepository;

	@Autowired
	private ContactService contactService;

	@Autowired
	private Attachment2TagRepository attachment2TagRepository;

	@Autowired
	private TagRepository tagRepository;
	
	@Autowired
	private AttachmentLobRepository attachmentLobRepository;

	@Autowired
	private MailUtil mailUtil;

	@Autowired
	private MailService mailService;

	@Autowired
	private StatementService statementService;

	@Autowired
	private StatementEditLogRepository statementEditLogRepository;

	@Autowired
	private UsersService usersService;

	@Autowired
	private UserInfoService userInfoService;

	@Test
	void getStatementWithValidStatementIdRespondsWithOptionalStatmentDetailsModel()
			throws InternalErrorServiceException, ForbiddenServiceException {
		TblStatement tblstatement = Mockito.mock(TblStatement.class);
		String sDueDate = "2020-04-22";
		String SReceiptDate = "2020-04-22";
		String sBusinessKey = "123e4567-e89b-12d3-a456-426655440000";
		String city = "City";
		String district = "District";
		String title = "title";
		TblStatementtype tst = Mockito.mock(TblStatementtype.class);
		Mockito.when(tst.getId()).thenReturn(1L);
		Mockito.when(tblstatement.getType()).thenReturn(tst);
		Mockito.when(tblstatement.getDueDate()).thenReturn(TypeConversion.dateOfDateString(sDueDate).get());
		Mockito.when(tblstatement.getReceiptDate()).thenReturn(TypeConversion.dateOfDateString(SReceiptDate).get());
		Mockito.when(tblstatement.getFinished()).thenReturn(false);
		Mockito.when(tblstatement.getId()).thenReturn(12L);
		Mockito.when(tblstatement.getBusinessKey()).thenReturn(sBusinessKey);
		Mockito.when(tblstatement.getDistrict()).thenReturn(district);
		Mockito.when(tblstatement.getCity()).thenReturn(city);
		Mockito.when(tblstatement.getTitle()).thenReturn(title);
		Mockito.when(statementRepository.findById(12L)).thenReturn(Optional.of(tblstatement));

		Optional<StatementDetailsModel> oStatementDetails = statementService.getStatement(12L);
		assertTrue(oStatementDetails.isPresent());
		assertEquals(12L, oStatementDetails.get().getId());
		assertEquals(sDueDate, oStatementDetails.get().getDueDate());
		assertEquals(SReceiptDate, oStatementDetails.get().getReceiptDate());
		assertEquals(sBusinessKey, oStatementDetails.get().getBusinessKey());
		assertEquals(false, oStatementDetails.get().getFinished());
		assertEquals(1L, oStatementDetails.get().getTypeId());
		assertEquals(city, oStatementDetails.get().getCity());
		assertEquals(district, oStatementDetails.get().getDistrict());
		assertEquals(title, oStatementDetails.get().getTitle());

	}

	@Test
	void getStatementWithInValidStatementIdRespondsWithOptionalEmpty()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Mockito.when(statementRepository.findById(404L)).thenReturn(Optional.empty());

		assertFalse(statementService.getStatement(null).isPresent());
		assertFalse(statementService.getStatement(404L).isPresent());
	}

	@Test
	void finishStatementWithValidStatementIdStatementNotFinishedYetRespondsWithOptionalTrue()
			throws ForbiddenServiceException, InternalErrorServiceException, NotFoundServiceException {
		TblStatement tblstatementorig = Mockito.mock(TblStatement.class);
		String sDueDate = "2020-04-22";
		String sBusinessKey = "123e4567-e89b-12d3-a456-426655440000";
		String title = "title";
		Mockito.when(tblstatementorig.getDueDate()).thenReturn(TypeConversion.dateOfDateString(sDueDate).get());
		Mockito.when(tblstatementorig.getFinished()).thenReturn(false);
		Mockito.when(tblstatementorig.getId()).thenReturn(12L);
		Mockito.when(tblstatementorig.getBusinessKey())
				.thenReturn(sBusinessKey);
		Mockito.when(tblstatementorig.getTitle()).thenReturn(title);
		Mockito.when(statementRepository.findById(12L)).thenReturn(Optional.of(tblstatementorig));

		TblStatement tblstatementchanged = Mockito.mock(TblStatement.class);
		Mockito.when(tblstatementchanged.getDueDate()).thenReturn(TypeConversion.dateOfDateString(sDueDate).get());
		Mockito.when(tblstatementchanged.getFinished()).thenReturn(true);
		Mockito.when(tblstatementchanged.getId()).thenReturn(12L);
		Mockito.when(tblstatementchanged.getBusinessKey())
				.thenReturn(sBusinessKey);
		Mockito.when(tblstatementchanged.getTitle()).thenReturn(title);
		Mockito.when(statementRepository.save(tblstatementchanged)).thenReturn(tblstatementchanged);

		assertTrue(statementService.finishStatement(12L, LocalDate.now()).get());
	}

	@Test
	void finishStatementWithInValidStatementIdRespondsWithOptionalEmpty()
			throws ForbiddenServiceException, InternalErrorServiceException, NotFoundServiceException {
		Mockito.when(statementRepository.findById(404L)).thenReturn(Optional.empty());

		assertFalse(statementService.finishStatement(404L, LocalDate.now()).isPresent());
	}

	@Test
	void createStatementWithValidStatementDetailsModelRespondsWithOptionalStatementDetailsModel()
			throws InternalErrorServiceException, BadRequestServiceException, ForbiddenServiceException,
			NotFoundServiceException, InterruptedException, UnprocessableEntityServiceException {
		String sDueDate = "2020-04-22";
		String sReceiptDate = "2020-04-22";
		String sCreationDate = "2020-04-22";
		String sBusinessKey = "123e4567-e89b-12d3-a456-426655440000";
		String title = "Title";
		String city = "City";
		String district = "District";
		String contactId = "contactId";
		String customerReference = "customerReference";
		StatementDetailsModel model = new StatementDetailsModel();
		model.setFinished(true);
		model.setDueDate(sDueDate);
		model.setBusinessKey(sBusinessKey);
		model.setTitle(title);
		model.setReceiptDate(sReceiptDate);
		model.setCity(city);
		model.setDistrict(district);
		model.setTypeId(1L);
		model.setContactId(contactId);
		model.setCreationDate(sCreationDate);
		model.setCustomerReference(customerReference);

		Long id = 12L;

		TblStatementtype tst = Mockito.mock(TblStatementtype.class);
		Mockito.when(tst.getId()).thenReturn(1L);
		Mockito.when(statementTypeRepository.findById(1L)).thenReturn(Optional.of(tst));

		TblStatement tblstatementchanged = Mockito.mock(TblStatement.class);
		Mockito.when(tblstatementchanged.getDueDate()).thenReturn(TypeConversion.dateOfDateString(sDueDate).get());
		Mockito.when(tblstatementchanged.getReceiptDate())
				.thenReturn(TypeConversion.dateOfDateString(sReceiptDate).get());
		Mockito.when(tblstatementchanged.getFinished()).thenReturn(true);
		Mockito.when(tblstatementchanged.getId()).thenReturn(12L);
		Mockito.when(tblstatementchanged.getBusinessKey())
				.thenReturn(sBusinessKey);
		Mockito.when(tblstatementchanged.getTitle()).thenReturn(title);
		Mockito.when(tblstatementchanged.getCity()).thenReturn(city);
		Mockito.when(tblstatementchanged.getDistrict()).thenReturn(district);
		Mockito.when(tblstatementchanged.getCreationDate())
				.thenReturn(TypeConversion.dateOfDateString(sCreationDate).get());
		Mockito.when(tblstatementchanged.getCustomerReference()).thenReturn(customerReference);
		Mockito.when(tblstatementchanged.getType()).thenReturn(tst);

		CompanyContactBlockModel contact = Mockito.mock(CompanyContactBlockModel.class);
		Mockito.when(contactService.getContactDetails(Mockito.eq(contactId), Mockito.eq(false)))
				.thenReturn(Optional.of(contact));
		Mockito.when(statementRepository.save(Mockito.any(TblStatement.class))).thenAnswer(new Answer<TblStatement>() {
			@Override
			public TblStatement answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				if (args[0] instanceof TblStatement) {
					TblStatement statement = (TblStatement) args[0];
					// Replaced by the service if set.
					assertNull(statement.getId());
					assertEquals(model.getBusinessKey(),statement.getBusinessKey());
					assertEquals(false, statement.getFinished());
					// Should match the model.
					assertEquals(model.getDueDate(),
							TypeConversion.dateStringOfLocalDate(statement.getDueDate()).get());
					assertEquals(model.getReceiptDate(),
							TypeConversion.dateStringOfLocalDate(statement.getReceiptDate()).get());
					assertEquals(model.getTitle(), statement.getTitle());
					assertEquals(model.getCity(), statement.getCity());
					assertEquals(model.getDistrict(), statement.getDistrict());
					assertEquals(1L, statement.getType().getId());
					return tblstatementchanged;
				}

				Mockito.when(tblstatementchanged.getId()).thenReturn(404L);
				return tblstatementchanged;
			}
		});

		List<TblDepartmentstructure> depstructs = new ArrayList<>();
		TblDepartmentstructure dps = new TblDepartmentstructure();
		dps.setId(1L);
		depstructs.add(dps);

		Mockito.when(departmentStructureRepository.getLatestDepartmentStructure()).thenReturn(depstructs);

		StatementDetailsModel response = statementService.createStatement(model).get();

		assertEquals(id, response.getId());
		assertEquals(model.getFinished(), response.getFinished());
		assertEquals(model.getDueDate(), response.getDueDate());
		assertEquals(model.getReceiptDate(), response.getReceiptDate());
		assertEquals(model.getBusinessKey(), response.getBusinessKey());
		assertEquals(model.getTitle(), response.getTitle());
		assertEquals(model.getCity(), response.getCity());
		assertEquals(model.getDistrict(), response.getDistrict());
		assertEquals(1, response.getTypeId());
		assertEquals(model.getCreationDate(), response.getCreationDate());
		assertEquals(model.getCustomerReference(), response.getCustomerReference());

	}

	@Test
	void createStatementWithInValidStatementDetailsModelRespondsWithOptionalEmpty()
			throws InternalErrorServiceException, BadRequestServiceException, ForbiddenServiceException,
			NotFoundServiceException, InterruptedException, UnprocessableEntityServiceException {
		assertFalse(statementService.createStatement(null).isPresent());
	}

	@Test
	void getStatementAttachmentsWithValidStatementIdRespondsWithOptionalListOfAttachmentModel()
			throws ForbiddenServiceException {
		List<TblAttachment> attachments = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			TblAttachment attachment = Mockito.mock(TblAttachment.class);
			Mockito.when(attachment.getFileName()).thenReturn("file.txt");
			Mockito.when(attachment.getFileType()).thenReturn("text/plain");
			Mockito.when(attachment.getId()).thenReturn(34L + i);
			attachments.add(attachment);
		}
		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statement.getAttachments()).thenReturn(attachments);
		Mockito.when(statementRepository.findById(12L)).thenReturn(Optional.of(statement));

		List<TblAttachment2Tag> a2ts = new ArrayList<>();
		TblAttachment2Tag a2t = new TblAttachment2Tag();
		a2t.setTag_id("2");
		Mockito.when(attachment2TagRepository.findByAttachmentId(Mockito.anyLong())).thenReturn(a2ts);

		Optional<List<AttachmentModel>> response = statementService.getStatementAttachments(12L);
		assertTrue(response.isPresent());
		List<AttachmentModel> rAttachments = response.get();
		assertEquals(attachments.size(), rAttachments.size());
		for (int i = 0; i < rAttachments.size(); i++) {
			TblAttachment a = attachments.get(i);
			AttachmentModel ra = rAttachments.get(i);
			assertEquals(a.getFileName(), ra.getName());
			assertEquals(a.getFileType(), ra.getType());
			assertEquals(a.getId(), ra.getId());
		}
	}

	@Test
	void getStatementAttachmentsWithInValidStatementIdRespondsWithOptionalEmpty() throws ForbiddenServiceException {
		Mockito.when(statementRepository.findById(404L)).thenReturn(Optional.empty());
		assertFalse(statementService.getStatementAttachments(404L).isPresent());
		assertFalse(statementService.getStatementAttachments(null).isPresent());
	}

	@Test
	void createStatementAttachmentWithValidStatementIdValidFileNameValidFileInputStreamResponsdWithoptionalAttachmentId()
			throws ForbiddenServiceException {
		String fileName = "testFile.txt";
		String type = "text/plain";
		Long id = 34L;
		TblStatement statement = Mockito.mock(TblStatement.class);
		FileInputStream fis = Mockito.mock(FileInputStream.class);
		TblAttachment tblattachment = Mockito.mock(TblAttachment.class);
		Mockito.when(tblattachment.getFileName()).thenReturn(fileName);
		Mockito.when(tblattachment.getFileType()).thenReturn(type);
		Mockito.when(tblattachment.getId()).thenReturn(id);
		Mockito.when(statementRepository.findById(12L)).thenReturn(Optional.of(statement));
		Mockito.when(attachmentRepository.save(Mockito.any(TblAttachment.class)))
				.thenAnswer(new Answer<TblAttachment>() {
					@Override
					public TblAttachment answer(InvocationOnMock invocation) throws Throwable {
						Object[] args = invocation.getArguments();
						if (args[0] instanceof TblAttachment) {
							TblAttachment iattachment = (TblAttachment) args[0];
							assertEquals(0L, iattachment.getId());
							assertEquals(statement, iattachment.getStatement());
							assertEquals(fileName, iattachment.getFileName());
							assertEquals(type, iattachment.getFileType());
							return tblattachment;
						}

						Mockito.when(tblattachment.getId()).thenReturn(404L);
						return tblattachment;
					}
				});
		TblAttachmentLob attachmentLob = new TblAttachmentLob();
		attachmentLob.setId(23L);
		Mockito.when(attachmentLobRepository.save(Mockito.any(TblAttachmentLob.class))).thenReturn(attachmentLob);
		Optional<Long> oModel = statementService.createStatementAttachment(12L, fileName, type, fis, 0);
		assertTrue(oModel.isPresent());
		assertEquals(id, oModel.get());
	}

	@Test
	void createStatementAttachmentWithInValidStatementIdRespondsWithOptionalEmpty() throws ForbiddenServiceException {
		FileInputStream fis = Mockito.mock(FileInputStream.class);
		assertFalse(statementService.createStatementAttachment(null, "testfile.txt", "text/plain", fis, 0).isPresent());

		Mockito.when(statementRepository.findById(404L)).thenReturn(Optional.empty());
		assertFalse(statementService.createStatementAttachment(404L, "testfile.txt", "text/plain", fis, 0).isPresent());
	}

	@Test
	void createStatementAttachmentWithValidStatementIdInValidFileNameRespondsWithOptionalEmpty()
			throws ForbiddenServiceException {
		TblStatement statement = Mockito.mock(TblStatement.class);
		FileInputStream fis = Mockito.mock(FileInputStream.class);
		Mockito.when(statementRepository.findById(12L)).thenReturn(Optional.of(statement));
		assertFalse(statementService.createStatementAttachment(12L, null, "text/plain", fis, 0).isPresent());

	}

	@Test
	void createStatementAttachmentWithValidStatementIdValidFileNameInvalidFileInputStreamRespondsWithOptionalEmpty()
			throws ForbiddenServiceException {

		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statementRepository.findById(12L)).thenReturn(Optional.of(statement));
		assertFalse(statementService.createStatementAttachment(12L, "testfile.txt", "text/plain", null, 0).isPresent());

	}

	@Test
	void getStatementAttachmentWithValidStatementIdAndValidAttachmentIdRespondsWithOptionalAttachmentModel()
			throws ForbiddenServiceException {
		String fileName = "testfile.txt";
		String type = "text/plain";
		Long id = 34L;
		TblAttachment attachment = Mockito.mock(TblAttachment.class);
		Mockito.when(attachment.getFileName()).thenReturn(fileName);
		Mockito.when(attachment.getFileType()).thenReturn(type);
		Mockito.when(attachment.getId()).thenReturn(id);
		List<TblAttachment> attachments = new ArrayList<>();
		attachments.add(attachment);
		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statement.getAttachments()).thenReturn(attachments);
		Mockito.when(statementRepository.findById(12L)).thenReturn(Optional.of(statement));
		Optional<AttachmentModel> oModel = statementService.getStatementAttachment(12L, id);
		assertTrue(oModel.isPresent());
		AttachmentModel model = oModel.get();
		assertEquals(fileName, model.getName());
		assertEquals(type, model.getType());
		assertEquals(id, model.getId());
	}

	@Test
	void getStatementAttachmentWithInValidStatementIdRespondsWithOptionalEmpty() throws ForbiddenServiceException {
		assertFalse(statementService.getStatementAttachment(null, 12L).isPresent());

		Mockito.when(statementRepository.findById(404L)).thenReturn(Optional.empty());
		assertFalse(statementService.getStatementAttachment(404L, 12L).isPresent());

	}

	@Test
	void getStatementAttachmentWithValidStatementIdAndInValidAttachmentIdRespondsWithOptionalEmpty()
			throws ForbiddenServiceException {
		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statement.getAttachments()).thenReturn(new ArrayList<>());
		Mockito.when(statementRepository.findById(12L)).thenReturn(Optional.of(statement));
		assertFalse(statementService.getStatementAttachment(12L, 404L).isPresent());
	}

	@Test
	void getStatementAttachmentFileWithValidStatementIdAndValidAttachmentIdRespondsWithOptionalAttachmentFile()
			throws IOException, ForbiddenServiceException {
		String fileName = "testfile.txt";
		String type = "text/plain";
		String fileContent = "this is some test data.";
		Long attachmentLobId = 14L;
		byte[] rawFile = fileContent.getBytes();
		Long id = 34L;
		TblAttachment attachment = Mockito.mock(TblAttachment.class);
		Mockito.when(attachment.getFileName()).thenReturn(fileName);
		Mockito.when(attachment.getFileType()).thenReturn(type);
		Mockito.when(attachment.getId()).thenReturn(id);
		Mockito.when(attachment.getAttachmentLobId()).thenReturn(attachmentLobId);
		List<TblAttachment> attachments = new ArrayList<>();
		TblAttachmentLob attachmentLob = new TblAttachmentLob();
		attachmentLob.setId(attachmentLobId);
		attachmentLob.setValue(rawFile);
		attachments.add(attachment);
		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statement.getAttachments()).thenReturn(attachments);
		Mockito.when(statementRepository.findById(12L)).thenReturn(Optional.of(statement));
		Mockito.when(attachmentLobRepository.findById(attachmentLobId)).thenReturn(Optional.of(attachmentLob));
		Optional<AttachmentFile> oModel = statementService.getStatementAttachmentFile(12L, id);
		assertEquals(fileName, oModel.get().getName());
		assertEquals(type, oModel.get().getType());
		assertEquals(rawFile.length, oModel.get().getLength());
		byte[] targetArray = new byte[oModel.get().getRessource().available()];
		oModel.get().getRessource().read(targetArray);
		String s = new String(targetArray);
		assertEquals(fileContent, s);

	}

	@Test
	void getContactBlockWithValidStatementIdShouldRespondWithContactBlockModel()
			throws ForbiddenServiceException, InternalErrorServiceException {
		Long statementId = 1234L;
		String contactId = "contactId";

		CompanyContactBlockModel contactBlockModel = Mockito.mock(CompanyContactBlockModel.class);
		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statement.getContactDbId()).thenReturn(contactId);
		Mockito.when(contactService.getContactDetails(Mockito.eq(contactId), Mockito.eq(true)))
				.thenReturn(Optional.of(contactBlockModel));
		Mockito.when(statementRepository.findById(statementId)).thenReturn(Optional.of(statement));
		Optional<CompanyContactBlockModel> oContactBlockModel = statementService.getContactBlock(statementId);
		assertTrue(oContactBlockModel.isPresent());
		assertTrue(contactBlockModel == oContactBlockModel.get());
	}

	@Test
	void getContactBlockWithInralidStatementIdShouldRespondWithOptionalEmpty()
			throws ForbiddenServiceException, InternalErrorServiceException {
		Long statementId = 1234L;
		Mockito.when(statementRepository.findById(statementId)).thenReturn(Optional.empty());
		assertFalse(statementService.getContactBlock(statementId).isPresent());
	}

	@Test
	void updateStatementWithValidStatementIdStatementModelShouldRespondWithStatementModel()
			throws NotFoundServiceException, BadRequestServiceException, InternalErrorServiceException,
			ForbiddenServiceException, UnprocessableEntityServiceException {

		Long statementId = 1234L;
		StatementDetailsModel newStatement = new StatementDetailsModel();

		Boolean finished = false;
		Long typeId = 1L;
		String businessKey = TypeConversion.stringOfBusinessKey(UUID.randomUUID()).get();
		String city = "city";
		String contactId = "contactId";
		String district = "district";
		String dueDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String receiptDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String creationDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String title = "title";
		String customerReference = "customerReference";

		newStatement.setBusinessKey(businessKey);
		newStatement.setCity(city);
		newStatement.setContactId(contactId);
		newStatement.setDistrict(district);
		newStatement.setDueDate(dueDate);
		newStatement.setFinished(finished);
		newStatement.setId(statementId);
		newStatement.setReceiptDate(receiptDate);
		newStatement.setTitle(title);
		newStatement.setTypeId(typeId);
		newStatement.setCreationDate(creationDate);
		newStatement.setCustomerReference(customerReference);

		TblStatement statement = new TblStatement();
		Mockito.when(statementRepository.findById(statementId)).thenReturn(Optional.of(statement));

		CompanyContactBlockModel ccbm = Mockito.mock(CompanyContactBlockModel.class);
		Mockito.when(contactService.getContactDetails(Mockito.eq(contactId), Mockito.eq(false)))
				.thenReturn(Optional.of(ccbm));

		TblStatementtype sType = Mockito.mock(TblStatementtype.class);
		Mockito.when(statementTypeRepository.findById(typeId)).thenReturn(Optional.of(sType));
		Mockito.when(sType.getId()).thenReturn(typeId);

		Mockito.when(statementRepository.save(Mockito.any(TblStatement.class))).thenAnswer(new Answer<TblStatement>() {
			@Override
			public TblStatement answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				if (args[0] instanceof TblStatement) {
					TblStatement statement = (TblStatement) args[0];
					assertNull(statement.getId());
					statement.setId(statementId);
					assertNull(statement.getFinished());
					statement.setFinished(finished);
					assertEquals(typeId, statement.getType().getId());
					assertNull(statement.getBusinessKey());
					statement.setBusinessKey(businessKey);
					assertEquals(city, statement.getCity());
					assertEquals(contactId, statement.getContactDbId());
					assertEquals(district, statement.getDistrict());
					assertEquals(dueDate, TypeConversion.dateStringOfLocalDate(statement.getDueDate()).get());
					assertEquals(receiptDate, TypeConversion.dateStringOfLocalDate(statement.getReceiptDate()).get());
					assertEquals(title, statement.getTitle());

					return statement;
				}
				return null;
			}
		});
		Optional<StatementDetailsModel> oSavedStatement = statementService.updateStatement(statementId, newStatement);
		assertTrue(oSavedStatement.isPresent());
		StatementDetailsModel respStatement = oSavedStatement.get();

		assertEquals(statementId, respStatement.getId());
		assertEquals(finished, respStatement.getFinished());
		assertEquals(typeId, respStatement.getTypeId());
		assertEquals(businessKey, respStatement.getBusinessKey());
		assertEquals(city, respStatement.getCity());
		assertEquals(contactId, respStatement.getContactId());
		assertEquals(district, respStatement.getDistrict());
		assertEquals(dueDate, respStatement.getDueDate());
		assertEquals(receiptDate, respStatement.getReceiptDate());
		assertEquals(title, respStatement.getTitle());
		assertEquals(creationDate, respStatement.getCreationDate());
		assertEquals(customerReference, respStatement.getCustomerReference());
	}

	@Test
	void updateStatementWithValidStatementIdInvalidStatementModelShouldThrowBadRequestServiceException()
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException,
			UnprocessableEntityServiceException {
		Long statementId = 1234L;
		StatementDetailsModel newStatement = new StatementDetailsModel();

		Boolean finished = false;
		String businessKey = TypeConversion.stringOfBusinessKey(UUID.randomUUID()).get();
		String city = "city";
		String contactId = "contactId";
		String district = "district";
		String dueDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String receiptDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String title = "title";

		newStatement.setBusinessKey(businessKey);
		newStatement.setCity(city);
		newStatement.setContactId(contactId);
		newStatement.setDistrict(district);
		newStatement.setDueDate(dueDate);
		newStatement.setFinished(finished);
		newStatement.setId(statementId);
		newStatement.setReceiptDate(receiptDate);
		newStatement.setTitle(title);
		newStatement.setTypeId(null);

		try {
			statementService.updateStatement(statementId, newStatement);
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}
	}

	@Test
	void updateStatementWithInvalidStatementIdValidStatementModelShouldThrowNotFoundServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException,
			UnprocessableEntityServiceException {
		Long statementId = 1234L;
		StatementDetailsModel newStatement = new StatementDetailsModel();

		Long typeId = 1L;
		Boolean finished = false;
		String businessKey = TypeConversion.stringOfBusinessKey(UUID.randomUUID()).get();
		String city = "city";
		String contactId = "contactId";
		String district = "district";
		String dueDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String receiptDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String title = "title";

		newStatement.setBusinessKey(businessKey);
		newStatement.setCity(city);
		newStatement.setContactId(contactId);
		newStatement.setDistrict(district);
		newStatement.setDueDate(dueDate);
		newStatement.setFinished(finished);
		newStatement.setId(statementId);
		newStatement.setReceiptDate(receiptDate);
		newStatement.setTitle(title);
		newStatement.setTypeId(typeId);

		Mockito.when(statementRepository.findById(statementId)).thenReturn(Optional.empty());

		try {
			statementService.updateStatement(statementId, newStatement);
			fail("Should have thrown NotFoundServiceException");
		} catch (NotFoundServiceException e) {
			// pass
		}
	}

	@Test
	void updateStatementWithValidStatementIdStatementModelIllegalArgumentExceptionSaveStatementShouldThrowInternalErrorServiceException()
			throws NotFoundServiceException, BadRequestServiceException, ForbiddenServiceException,
			InternalErrorServiceException, UnprocessableEntityServiceException {

		Long statementId = 1234L;
		StatementDetailsModel newStatement = new StatementDetailsModel();

		Boolean finished = false;
		Long typeId = 1L;
		String businessKey = TypeConversion.stringOfBusinessKey(UUID.randomUUID()).get();
		String city = "city";
		String contactId = "contactId";
		String district = "district";
		String dueDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String receiptDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String creationDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String title = "title";
		String customerReference = "customerReference";

		newStatement.setBusinessKey(businessKey);
		newStatement.setCity(city);
		newStatement.setContactId(contactId);
		newStatement.setDistrict(district);
		newStatement.setDueDate(dueDate);
		newStatement.setFinished(finished);
		newStatement.setId(statementId);
		newStatement.setReceiptDate(receiptDate);
		newStatement.setTitle(title);
		newStatement.setTypeId(typeId);
		newStatement.setCustomerReference(customerReference);
		newStatement.setCreationDate(creationDate);

		TblStatement statement = new TblStatement();
		Mockito.when(statementRepository.findById(statementId)).thenReturn(Optional.of(statement));

		CompanyContactBlockModel ccbm = Mockito.mock(CompanyContactBlockModel.class);
		Mockito.when(contactService.getContactDetails(Mockito.eq(contactId), Mockito.eq(false)))
				.thenReturn(Optional.of(ccbm));

		TblStatementtype sType = Mockito.mock(TblStatementtype.class);
		Mockito.when(statementTypeRepository.findById(typeId)).thenReturn(Optional.of(sType));
		Mockito.when(sType.getId()).thenReturn(typeId);

		Mockito.doThrow(new IllegalArgumentException()).when(statementRepository).save(Mockito.any(TblStatement.class));

		try {
			statementService.updateStatement(statementId, newStatement);
			fail("Should have thrown InternalErrorServiceException");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getAttachmentTagsForAttachmentIdShouldRespondTagModels() {
		String tagId = "tagId";
		String label = "label";
		TblTag tTag = new TblTag();
		tTag.setId(tagId);
		tTag.setName(label);

		Long attachmentId = 23L;
		List<TblAttachment2Tag> a2ts = new ArrayList<>();
		TblAttachment2Tag a2t = new TblAttachment2Tag();
		a2t.setAttachment_id(attachmentId);
		a2t.setTag_id(tagId);
		a2ts.add(a2t);
		Mockito.when(attachment2TagRepository.findByAttachmentId(Mockito.eq(attachmentId))).thenReturn(a2ts);

		Mockito.when(tagRepository.findById(Mockito.eq(tagId))).thenReturn(Optional.of(tTag));

		List<TagModel> tags = statementService.getAttachmentTags(attachmentId);
		assertTrue(tags.size() == 1);
		TagModel tag = tags.get(0);
		assertEquals(tagId, tag.getId());
		assertEquals(label, tag.getLabel());
	}

	@Test
	void addTagWithNewLabelShouldNotThrowException()
			throws ForbiddenServiceException, ConflictServiceException, InternalErrorServiceException {
		String label = "label";

		Mockito.when(tagRepository.findById(Mockito.eq(label))).thenReturn(Optional.empty());

		ArgumentCaptor<TblTag> tagCaptor = ArgumentCaptor.forClass(TblTag.class);
		statementService.addTag(label);

		Mockito.verify(tagRepository).save(tagCaptor.capture());
		TblTag tag = tagCaptor.getValue();
		assertEquals(label, tag.getName());
	}

	void addTagWithExistingLabelShouldThrowConflictServiceException()
			throws ForbiddenServiceException, InternalErrorServiceException {
		String label = "label";

		List<TblTag> tags = new ArrayList<>();
		TblTag tag = new TblTag();
		tags.add(tag);
		Mockito.when(tagRepository.findById(Mockito.eq(label))).thenReturn(Optional.of(tag));

		try {
			statementService.addTag(label);
			fail("Should have trhown ConflictServiceException");
		} catch (ConflictServiceException e) {
			// pass
		}
	}

	@Test
	void deleteStatementAttachmentWithValidStatementIdTaskIdAttachmentIdThrowsNoException()
			throws InternalErrorServiceException, NotFoundServiceException, ForbiddenServiceException {

		Long statementId = 1234L;
		Long attachmentId = 23L;

		TblStatement mockedStatement = Mockito.mock(TblStatement.class);

		List<TblAttachment> attachments = new ArrayList<>();
		TblAttachment attachment = new TblAttachment();
		attachment.setId(attachmentId);
		attachments.add(attachment);
		Mockito.when(mockedStatement.getAttachments()).thenReturn(attachments);
		Mockito.when(statementRepository.findById(statementId)).thenReturn(Optional.of(mockedStatement));

		Mockito.when(attachmentRepository.existsById(Mockito.eq(attachmentId))).thenReturn(false);

		ArgumentCaptor<Long> attachmentCaptor = ArgumentCaptor.forClass(Long.class);

		statementService.deleteStatementAttachments("ANY", statementId, attachmentId, "ANY");

		Mockito.verify(attachmentRepository).deleteById(attachmentCaptor.capture());

		Long deleteAttachmentId = attachmentCaptor.getValue();
		assertEquals(attachmentId, deleteAttachmentId);
	}

	@Test
	void deleteStatementAttachmentWithInvalidStatementIdTaskIdAttachmentIdThrowsNotFoundServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		Long attachmentId = null;

		try {
			statementService.deleteStatementAttachments("ANY", statementId, attachmentId, "ANY");
			fail("Should have thrown NotFoundServiceException");
		} catch (NotFoundServiceException e) {
			// pass
		}
	}

	@Test
	void tagIdsForTblTagsShouldReturnIdsOfProvidedTblTags() {

		Set<TblTag> tags = new HashSet<>();
		for (int i = 0; i < 10; i++) {
			TblTag tag = new TblTag();
			tag.setId("" + i);
			tags.add(tag);
		}

		Set<String> ids = statementService.tagIdsOf(tags);
		for (int i = 0; i < 10; i++) {
			assertTrue(ids.contains("" + i));
		}

	}

	@Test
	void getAllSectorsShouldRespondWithSectors() throws ForbiddenServiceException {

		List<TblDepartmentstructure> depStructures = new ArrayList<>();

		TblDepartmentstructure depStructure = new TblDepartmentstructure();
		depStructure.setId(1);
		depStructure.setVersion(1L);
		String key = "Ort#Ortsteil";
		List<String> sectors = new ArrayList<>();
		sectors.add("Sector1");
		sectors.add("Sector2");
		DistrictDepartmentsModel depmodel = new DistrictDepartmentsModel();
		depmodel.setProvides(sectors);

		Map<String, DistrictDepartmentsModel> definition = new HashMap<>();

		definition.put(key, depmodel);

		depStructure.setDefinition(definition);
		depStructures.add(depStructure);

		Mockito.when(departmentStructureRepository.getLatestDepartmentStructure()).thenReturn(depStructures);

		Map<String, List<String>> rSectors = statementService.getAllSectors();

		assertTrue(rSectors.containsKey(key));
		assertEquals(sectors, rSectors.get(key));

	}

	@Test
	void getAllSectorsNoDepartmentStructureInRepositoryShouldRespondWithEmptySectors()
			throws ForbiddenServiceException {

		List<TblDepartmentstructure> depStructures = new ArrayList<>();

		Mockito.when(departmentStructureRepository.getLatestDepartmentStructure()).thenReturn(depStructures);

		Map<String, List<String>> rSectors = statementService.getAllSectors();
		assertTrue(rSectors.isEmpty());

	}

	@Test
	void getAllSectorsWithValidStatementIdShouldRespondWithSectors()
			throws NotFoundServiceException, ForbiddenServiceException {
		Long statementId = 1234L;

		TblDepartmentstructure depStructure = new TblDepartmentstructure();
		depStructure.setId(1);
		depStructure.setVersion(1L);
		String key = "Ort#Ortsteil";
		List<String> sectors = new ArrayList<>();
		sectors.add("Sector1");
		sectors.add("Sector2");
		DistrictDepartmentsModel depmodel = new DistrictDepartmentsModel();
		depmodel.setProvides(sectors);

		Map<String, DistrictDepartmentsModel> definition = new HashMap<>();

		definition.put(key, depmodel);

		depStructure.setDefinition(definition);

		TblStatement mockedStatement = Mockito.mock(TblStatement.class);
		Mockito.when(mockedStatement.getDepartmentstructure()).thenReturn(depStructure);

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockedStatement));

		Map<String, List<String>> rSectors = statementService.getAllSectors(statementId);

		assertTrue(rSectors.containsKey(key));
		assertEquals(sectors, rSectors.get(key));
	}

	@Test
	void getAllSectorsWithInvalidStatementIdShouldThrowNotFoundServiceException()
			throws NotFoundServiceException, ForbiddenServiceException {
		Long statementId = 404L;

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());
		try {
			statementService.getAllSectors(statementId);
			fail("Should have thrown NotFoundServiceException");
		} catch (NotFoundServiceException e) {
			// pass
		}
	}

	@Test
	void getAllTags() throws ForbiddenServiceException {

		List<TblTag> tblTags = new ArrayList<>();
		TblTag tag = new TblTag();
		tag.setId("id");
		tag.setName("name");
		tblTags.add(tag);
		Mockito.when(tagRepository.findAll()).thenReturn(tblTags);
		List<TagModel> rTags = statementService.getAllTags();
		assertTrue(rTags.size() == 1);
		TagModel rTag = rTags.get(0);
		assertEquals("id", rTag.getId());
		assertEquals("name", rTag.getLabel());

	}

	@Test
	void dispatchStatementResponse() throws ForbiddenServiceException, InternalErrorServiceException {

		Long statementId = 1234L;

		CompanyContactBlockModel recipient = Mockito.mock(CompanyContactBlockModel.class);
		String email = "recipient@host.tld";
		Mockito.when(recipient.getEmail()).thenReturn(email);

		String contactPersonUUID = "contactPersonUUID";

		TblStatement tblStatement = Mockito.mock(TblStatement.class);
		Mockito.when(tblStatement.getContactDbId()).thenReturn(contactPersonUUID);
		List<TblAttachment> attachments = new ArrayList<>();
		TblAttachment attachment = Mockito.mock(TblAttachment.class);
		attachments.add(attachment);
		Mockito.when(attachment.getFileName()).thenReturn("fileName");
		Mockito.when(attachment.getFileType()).thenReturn("type");
		Mockito.when(attachment.getId()).thenReturn(1L);
		Mockito.when(attachment.getSize()).thenReturn(10L);
		Mockito.when(attachment.getTimestamp()).thenReturn(LocalDateTime.now());
		Long attachmentLobId = 23L;;
		Mockito.when(attachment.getAttachmentLobId()).thenReturn(attachmentLobId);
		TblAttachmentLob attachmentLob = new TblAttachmentLob();
		attachmentLob.setId(23L);
		attachmentLob.setValue("sdf".getBytes());
		Mockito.when(attachmentLobRepository.findById(attachmentLobId)).thenReturn(Optional.of(attachmentLob));

		List<TblAttachment2Tag> a2ts = new ArrayList<>();
		TblAttachment2Tag a2toutbox = Mockito.mock(TblAttachment2Tag.class);
		Mockito.when(a2toutbox.getTag_id()).thenReturn("outbox");
		a2ts.add(a2toutbox);

		TblAttachment2Tag a2tstatement = Mockito.mock(TblAttachment2Tag.class);
		Mockito.when(a2tstatement.getTag_id()).thenReturn("statement");
		a2ts.add(a2tstatement);
		Mockito.when(attachment2TagRepository.findByAttachmentId(1L)).thenReturn(a2ts);

		TblTag outboxtag = Mockito.mock(TblTag.class);
		Mockito.when(outboxtag.getId()).thenReturn("outbox");
		Mockito.when(outboxtag.getName()).thenReturn("outbox");
		Mockito.when(tagRepository.findById("outbox")).thenReturn(Optional.of(outboxtag));

		TblTag statementtag = Mockito.mock(TblTag.class);
		Mockito.when(statementtag.getId()).thenReturn("statement");
		Mockito.when(statementtag.getName()).thenReturn("statement");
		Mockito.when(tagRepository.findById("statement")).thenReturn(Optional.of(statementtag));

		Mockito.when(tblStatement.getAttachments()).thenReturn(attachments);
		Mockito.when(statementRepository.findById(statementId)).thenReturn(Optional.of(tblStatement));

		Mockito.when(contactService.getContactDetails(contactPersonUUID, true)).thenReturn(Optional.of(recipient));

		Mockito.when(mailUtil.validEmailAddress(Mockito.eq(email))).thenReturn(true);

		MailSendReport report = new MailSendReport();
		report.setSuccessful(true);
		Mockito.when(mailService.sendStatementMail(Mockito.any())).thenReturn(report);
		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setStatementResponse(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);

		assertTrue(statementService.dispatchStatementResponse(statementId).getSuccessful());
	}

	@Test
	void transferMailTextToAttachment()
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		String sourceMailId = "sourceMailId";
		Long attachmentId = 43L;

		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statement.getSourceMailId()).thenReturn(sourceMailId);

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(statement));

		MailEntry mailEntry = new MailEntry();
		String textPlain = "das ist ein Test";
		mailEntry.setTextPlain(textPlain);
		Mockito.when(mailService.getMail(Mockito.eq(sourceMailId))).thenReturn(Optional.of(mailEntry));

		TblAttachment attachment = Mockito.mock(TblAttachment.class);
		Mockito.when(attachmentRepository.save(Mockito.any(TblAttachment.class))).thenReturn(attachment);
		Mockito.when(attachment.getId()).thenReturn(attachmentId);

		Mockito.when(attachmentRepository.findById(attachmentId)).thenReturn(Optional.of(attachment));
		Mockito.when(attachment.getStatement()).thenReturn(statement);
		Mockito.when(statement.getId()).thenReturn(statementId);

		// tagsExist
		List<TblTag> allTags = new ArrayList<>();
		TblTag emailTag = new TblTag();
		emailTag.setId("email");
		emailTag.setName("Email");
		allTags.add(emailTag);
		TblTag emailTextTag = new TblTag();
		emailTextTag.setId("email-text");
		emailTextTag.setName("Email text");
		allTags.add(emailTextTag);
		Mockito.when(tagRepository.findAll()).thenReturn(allTags);

		List<TblAttachment2Tag> a2ts = new ArrayList<>();
		Mockito.when(attachment2TagRepository.findByAttachmentId(Mockito.eq(attachmentId))).thenReturn(a2ts);
		
		TblAttachmentLob attachmentLob = new TblAttachmentLob();
		attachmentLob.setId(23L);
		attachmentLob.setValue("sdf".getBytes());
		Mockito.when( attachmentLobRepository.save(Mockito.any(TblAttachmentLob.class))).thenReturn(attachmentLob);
		
		AttachmentModel resp = statementService.transferMailTextToAttachment(statementId);

		Mockito.verify(attachment2TagRepository).saveAll(Mockito.any());

		assertTrue(resp != null);
		assertEquals(attachmentId, resp.getId());
		assertEquals(StatementService.FILE_NAME_MAIL_TEXT, resp.getName());
		assertEquals(StatementService.FILE_TYPE_MAIL_TEXT, resp.getType());

	}

	@Test
	void transferMailAttachments()
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		String sourceMailId = "sourceMailId";
		Long attachmentId = 43L;

		List<MailTransferAttachment> tAs = new ArrayList<>();
		MailTransferAttachment tA = new MailTransferAttachment();
		String attachmentName = "attachmentName";
		tA.setName(attachmentName);
		Set<String> tagIds = new HashSet<>();
		tA.setTagIds(tagIds);
		tAs.add(tA);

		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statement.getSourceMailId()).thenReturn(sourceMailId);

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(statement));

		MailEntry mailEntry = new MailEntry();
		mailEntry.setIdentifier(sourceMailId);
		String textPlain = "das ist ein Test";
		mailEntry.setTextPlain(textPlain);
		Mockito.when(mailService.getMail(Mockito.eq(sourceMailId))).thenReturn(Optional.of(mailEntry));

		Set<String> attachmentNames = new HashSet<>();
		attachmentNames.add(attachmentName);
		Map<String, AttachmentFile> mailAttachments = new HashMap<>();
		AttachmentFile attachmentFile = new AttachmentFile();
		attachmentFile.setName(attachmentName);
		byte[] bytes = "Das ist ein weiterer Test".getBytes();
		attachmentFile.setLength(bytes.length);
		ByteArrayInputStream ressource = new ByteArrayInputStream(bytes);
		attachmentFile.setRessource(ressource);
		String type = "type";
		attachmentFile.setType(type);
		mailAttachments.put(attachmentName, attachmentFile);

		// tagsExist
		List<TblTag> allTags = new ArrayList<>();
		TblTag emailTag = new TblTag();
		emailTag.setId("email");
		emailTag.setName("Email");
		allTags.add(emailTag);
		TblTag emailTextTag = new TblTag();
		emailTextTag.setId("email-text");
		emailTextTag.setName("Email text");
		allTags.add(emailTextTag);
		Mockito.when(tagRepository.findAll()).thenReturn(allTags);

		List<TblAttachment2Tag> a2ts = new ArrayList<>();
		Mockito.when(attachment2TagRepository.findByAttachmentId(Mockito.eq(attachmentId))).thenReturn(a2ts);

		Mockito.when(mailService.getStatementInboxMailAttachment(Mockito.eq(sourceMailId), Mockito.eq(attachmentNames)))
				.thenReturn(mailAttachments);

		TblAttachment attachment = Mockito.mock(TblAttachment.class);
		Mockito.when(attachmentRepository.save(Mockito.any(TblAttachment.class))).thenReturn(attachment);
		Mockito.when(attachment.getId()).thenReturn(attachmentId);

		Mockito.when(attachmentRepository.findById(attachmentId)).thenReturn(Optional.of(attachment));
		Mockito.when(attachment.getStatement()).thenReturn(statement);
		Mockito.when(statement.getId()).thenReturn(statementId);

		Mockito.when(attachmentRepository.save(Mockito.any())).thenReturn(attachment);

		Mockito.when(attachmentRepository.findById(attachmentId)).thenReturn(Optional.of(attachment));

		TblAttachmentLob attachmentLob = new TblAttachmentLob();
		attachmentLob.setId(23L);
		Mockito.when(attachmentLobRepository.save(Mockito.any(TblAttachmentLob.class))).thenReturn(attachmentLob );
		List<AttachmentModel> resps = statementService.transferMailAttachments(statementId, tAs);

		Mockito.verify(attachment2TagRepository).saveAll(Mockito.any());

		assertTrue(resps.size() == 1);
		AttachmentModel resp = resps.get(0);
		assertTrue(resp != null);
		assertEquals(attachmentId, resp.getId());
		assertEquals(attachmentName, resp.getName());
		assertEquals(type, resp.getType());
		assertEquals(bytes.length, resp.getSize());

	}

	@Test
	void editLog() {
		Long statementId = 1234L;
		String accessType = "accessType";
		String username = "username";

		Mockito.when(userInfoService.getUserName()).thenReturn(username);
		statementService.editLog(statementId, accessType);
		TblUser user = new TblUser();
		Mockito.when(usersService.getTblUser(Mockito.eq(username))).thenReturn(Optional.of(user));

		statementService.editLog(statementId, accessType);

		TblStatement statement = new TblStatement();
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(statement));

		statementService.editLog(statementId, accessType);
		Mockito.verify(statementEditLogRepository).save(Mockito.any(TblStatementEditLog.class));
	}
	
	@Test
	void getBusinessKey() {
		Long statementId = 1234L;
		UUID businessKey = UUID.randomUUID();
	
		TblStatement statement = new TblStatement();
		statement.setBusinessKey(TypeConversion.stringOfBusinessKey(businessKey).get());
		Mockito.when(statementRepository.findById(statementId)).thenReturn(Optional.of(statement));
		Optional<String> oBusinessKey = statementService.getStatementBusinessKey(statementId);
		assertTrue(oBusinessKey.isPresent());
		assertEquals(TypeConversion.stringOfBusinessKey(businessKey).get(), oBusinessKey.get());
	}
	
	@Test
	void addConsideration() throws NotFoundServiceException, ForbiddenServiceException, BadRequestServiceException, InternalErrorServiceException {
		
		Long statementId = 1234L;
		String fileName = "test.txt";
		String fileType = "txt";
		InputStream is = Mockito.mock(InputStream.class);
		Long length = 123L;
		
	
		Long attachmentId = 2L;

		TblStatement statement = new TblStatement();
		statement.setFinished(true);
		statement.setFinishedDate(LocalDate.now());
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(statement));

		Set<String> tagIds = new HashSet<>();
		String statementTag = Tags.CONSIDERATION.tagId();
		tagIds.add(statementTag);
		
		Mockito.when(attachment2TagRepository.findByAttachmentId(Mockito.eq(attachmentId))).thenReturn(new ArrayList<>());
		
		TblAttachment attachmenttbl = new TblAttachment();
		attachmenttbl.setId(attachmentId);
		attachmenttbl.setFileName(fileName);
		attachmenttbl.setSize(length);
		attachmenttbl.setFileType(fileType);

		Mockito.when(attachmentRepository.save(Mockito.any(TblAttachment.class))).thenReturn(attachmenttbl);
	
		TblAttachmentLob attachmentLob = new TblAttachmentLob();
		attachmentLob.setId(23L);
		Mockito.when(attachmentLobRepository.save(Mockito.any(TblAttachmentLob.class))).thenReturn(attachmentLob);
		Optional<AttachmentModel> oAttachment = statementService.addConsideration(statementId, fileName, fileType, is, length);
		assertTrue(oAttachment.isPresent());

	}

}
