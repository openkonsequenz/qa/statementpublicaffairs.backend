/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.openk.statementpublicaffairs.api.AuthNAuthApi;
import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationSessionService;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.JwtToken;
import org.eclipse.openk.statementpublicaffairs.model.LoginCredentials;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;

import feign.Request.HttpMethod;
import feign.Response;

/**
 * Test verifies SessionService interfaces.
 * 
 * @author Tobias Stummer
 *
 */
@SpringBootTest(classes = TestConfigurationSessionService.class)
@ActiveProfiles("test")

class SessionServiceTest {

	@Autowired
	private SessionService sessionService;

	@Autowired
	private AuthNAuthApi authNAuthApi;

	private feign.Response mockResponse(int statusCode, String body) {
		feign.Request rq = feign.Request.create(HttpMethod.GET, "http://testurl", new HashMap<>(), new byte[0],
				Charset.defaultCharset());
		return feign.Response.builder().request(rq).body(body, StandardCharsets.UTF_8).status(statusCode)
				.headers(new HashMap<>()).build();
	}

	@Test
	void testLogout() {
		int testResponseStatus = 201;
		String testToken = "testToken";
		Authentication authentication = mock(Authentication.class);
		when(authentication.getDetails()).thenReturn(testToken);
		SecurityContext securityContext = mock(SecurityContext.class);
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		feign.Response resp = mockResponse(testResponseStatus, "");
		assertEquals(testResponseStatus, resp.status());
		when(authNAuthApi.logout(testToken)).thenReturn(resp);
		assertEquals(testResponseStatus, sessionService.logout());
	}

	@Test
	void validateTokenWithValidTokenShouldRespondTrue() {
		String token = "token";

		Response response = mockResponse(200, "");
		Mockito.when(authNAuthApi.isTokenValid(Mockito.eq(token))).thenReturn(response);

		assertTrue(sessionService.validateToken(token));
	}

	@Test
	void hasRolesWithValidRolesShouldRespondTrue() {

		String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJHN0MtQ1E4ckdYUGpzSlY0R0xEWHI2dTFiOUwzV1pYVkxEa1VJOWV1d1FnIn0.eyJqdGkiOiIxMWFjMGQwNy00YzYwLTRkZjctODBhNS1hMjFmMDk2MDAxYTkiLCJleHAiOjE1OTQ3MTQ3ODUsIm5iZiI6MCwiaWF0IjoxNTk0NzE0NDg1LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiI2MDI0YjZiOS1kYWY4LTRjNjEtOGJjMy1jZDYxMGMxYmI3MTMiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiOGRiNTNjMWQtN2JiOS00YjhjLTkwOWYtMjJjZTI1NWRhNzUyIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJzcGFfZGl2aXNpb25fbWVtYmVyIiwia29uLWFkbWluIiwic3BhX2FjY2VzcyIsImtvbi1hY2Nlc3MiLCJzcGFfYXBwcm92ZXIiLCJzcGFfb2ZmaWNpYWxfaW5fY2hhcmdlIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJyb2xlcyI6IltzcGFfZGl2aXNpb25fbWVtYmVyLCBvZmZsaW5lX2FjY2VzcywgdW1hX2F1dGhvcml6YXRpb24sIHNwYV9hY2Nlc3MsIGtvbi1hZG1pbiwga29uLWFjY2Vzcywgc3BhX29mZmljaWFsX2luX2NoYXJnZSwgc3BhX2FwcHJvdmVyXSIsIm5hbWUiOiJUZXN0IE1pdGFyYmVpdGVyIiwicHJlZmVycmVkX3VzZXJuYW1lIjoic3BhX211bHRpIiwiZ2l2ZW5fbmFtZSI6IlRlc3QiLCJmYW1pbHlfbmFtZSI6Ik1pdGFyYmVpdGVyIn0.YrLti7OQmVsKe4Nk1qmFclJ_FooeFuBYyIReNH6AtDvA1SdKUbCThshxeoIA1Ar6VyEE9P6JI4SYTLtXW5BZbzLSCfSTqe8IVLjy2k3HZuLq4rgv0QoEFXbvxiQbmtctQGRhRefdbBbAD-WatMxyUvjE_PU5t-pndwBdhaY1DdroBRtO5J7JCmpAn58Wddg4xVV_eEsnczgZgzaLejJfpZ9cpiC6MaV7WpWvO4aQRD5KyA6g6ie_zThSJ7c0pkuDHaxaWCBDsQc5gqyMdUiwxDxra-ZcWgPDSVuM1OyOHkgaWVyrOvGkK4AgFaMuaf_jl_l64uh0tTlDuty7XwvahA";
		List<String> requiredRoles = new ArrayList<>();
		requiredRoles.add(UserRoles.SPA_ACCESS);
		requiredRoles.add(UserRoles.SPA_OFFICIAL_IN_CHARGE);
		assertTrue(sessionService.hasRoles(requiredRoles, token));
	}

	@Test
	void getTokenShouldRespondToken() throws InternalErrorServiceException {
		String token = "token";
		String username = "username";
		String password = "password";

		JwtToken jwtToken = Mockito.mock(JwtToken.class);
		Mockito.when(jwtToken.getAccessToken()).thenReturn(token);
		Mockito.when(authNAuthApi.login(Mockito.any(LoginCredentials.class))).thenReturn(jwtToken);
		String rToken = sessionService.getToken(username, password);
		assertEquals(token, rToken);
	}
}
