/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationTextblockService;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.model.TaskInfo;
import org.eclipse.openk.statementpublicaffairs.model.Textblock;
import org.eclipse.openk.statementpublicaffairs.model.Textblock.TextblockType;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;
import org.eclipse.openk.statementpublicaffairs.model.TextblockGroup;
import org.eclipse.openk.statementpublicaffairs.model.TextblockItem;
import org.eclipse.openk.statementpublicaffairs.model.TextblockRequirement;
import org.eclipse.openk.statementpublicaffairs.model.TextblockRequirement.TextblockRequirementType;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementtype;
import org.eclipse.openk.statementpublicaffairs.model.db.TblTextblockdefinition;
import org.eclipse.openk.statementpublicaffairs.model.db.TblWorkflowdata;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.WorkflowDataRepository;
import org.eclipse.openk.statementpublicaffairs.viewmodel.TextConfiguration;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ValidationError;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ValidationResult;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationTextblockService.class)
@ActiveProfiles("test")
class TextblockServiceTest {

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private StatementProcessService statementProcessService;

	@Autowired
	private WorkflowDataRepository workflowDataRepository;

	@Autowired
	private StatementService statementService;

	@Autowired
	private ContactService contactService;

	@Autowired
	private TextblockService textblockService;

	@Autowired
	private StatementCompileService statementCompileService;

	@Test
	void getTextblockDefinitionWithValidStatementIdShouldRespondWithTextblockDefinition()
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);
		TblStatementtype mockStatementType = Mockito.mock(TblStatementtype.class);
		Mockito.when(mockStatementType.getName()).thenReturn("StatementType");
		Mockito.when(mockStatement.getType()).thenReturn(mockStatementType);

		TblTextblockdefinition mockTextblockDefinition = Mockito.mock(TblTextblockdefinition.class);
		Mockito.when(mockWorkflowData.getTextBlockDefinition()).thenReturn(mockTextblockDefinition);

		TextblockDefinition def = new TextblockDefinition();
		List<TextblockGroup> groups = new ArrayList<>();
		def.setGroups(groups);
		def.setSelects(new HashMap<>());
		Mockito.when(mockTextblockDefinition.getDefinition()).thenReturn(def);

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));
		Optional<TextConfiguration> oTbDef = textblockService.getTextblockConfiguration(statementId);
		assertTrue(oTbDef.isPresent());
		TextConfiguration tbDef = oTbDef.get();
		assertTrue(def == tbDef.getConfiguration());

	}

	@Test
	void getTextblockDefinitionWithInValidStatementIdShouldRespondOptionalEmpty()
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 404L;

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());
		assertFalse(textblockService.getTextblockConfiguration(statementId).isPresent());

	}

	@Test
	void getTextblockDefinitionWithValidStatementIdNotYetWorkflowDataShouldThrowNotFoundServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(null);

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));
		try {
			textblockService.getTextblockConfiguration(statementId);
			fail("Should have thrown NotFoundServiceException");
		} catch (NotFoundServiceException e) {
			// pass
		}
	}

	@Test
	void getTextblockDefinitionWithValidStatementIdInvlidTextblockDefinitionShouldThrowInternalErrorServiceException()
			throws NotFoundServiceException, ForbiddenServiceException {
		Long statementId = 1234L;

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);
		Mockito.when(mockWorkflowData.getTextBlockDefinition()).thenReturn(null);

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));
		try {
			textblockService.getTextblockConfiguration(statementId);
			fail("Should have thrown InternalErrorServiceException");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void getTextArrangementWithValidStatementIdShouldRespondWithTextArrangement()
			throws NotFoundServiceException, ForbiddenServiceException {
		Long statementId = 1234L;

		List<Textblock> draft = new ArrayList<>();

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);

		Mockito.when(mockWorkflowData.getDraft()).thenReturn(draft);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		Optional<List<Textblock>> oTextArr = textblockService.getTextArrangement(statementId);
		assertTrue(oTextArr.isPresent());
		List<Textblock> textArr = oTextArr.get();
		assertTrue(draft == textArr);

	}

	@Test
	void getTextArrangementWithInValidStatementIdShouldRespondWithOptionalEmpty()
			throws NotFoundServiceException, ForbiddenServiceException {
		Long statementId = 404L;

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());

		assertFalse(textblockService.getTextArrangement(statementId).isPresent());

	}

	@Test
	void getTextArrangementWithValidStatementNotYetWorkflowDataShouldRespondWithEmptyTextArrangement()
			throws NotFoundServiceException, ForbiddenServiceException {
		Long statementId = 1234L;

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(null);

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		Optional<List<Textblock>> oTextArr = textblockService.getTextArrangement(statementId);
		assertTrue(oTextArr.isPresent());
		List<Textblock> textArr = oTextArr.get();
		assertTrue(textArr.isEmpty());

	}

	@Test
	void getTextArrangementWithValidStatementDraftIsNullShouldRespondWithEmptyTextArrangement()
			throws NotFoundServiceException, ForbiddenServiceException {
		Long statementId = 1234L;

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);

		Mockito.when(mockWorkflowData.getDraft()).thenReturn(null);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		Optional<List<Textblock>> oTextArr = textblockService.getTextArrangement(statementId);
		assertTrue(oTextArr.isPresent());
		List<Textblock> textArr = oTextArr.get();
		assertTrue(textArr.isEmpty());

	}

	@Test
	void verifyTextArrangementWithValidStatementIdValidTextArrangementShouldNotThrowAnyException()
			throws BadRequestServiceException, NotFoundServiceException, InternalErrorServiceException,
			ForbiddenServiceException {
		Long statementId = 1234L;
		String statementTypeName = "statementTypeName";

		TblStatement mockedStatement = Mockito.mock(TblStatement.class);

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockedStatement));

		TblWorkflowdata mockedWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockedStatement.getWorkflowdata()).thenReturn(mockedWorkflowData);

		TblTextblockdefinition textblockDefinition = Mockito.mock(TblTextblockdefinition.class);
		TblStatementtype statementType = Mockito.mock(TblStatementtype.class);
		Mockito.when(statementType.getName()).thenReturn(statementTypeName);

		Mockito.when(mockedStatement.getType()).thenReturn(statementType);
		Mockito.when(mockedWorkflowData.getTextBlockDefinition()).thenReturn(textblockDefinition);

		List<Textblock> textArrangement = new ArrayList<>();

		textblockService.verifyTextArrangement(textArrangement, statementId);

	}

	@Test
	void verifyTextArrangementWithValidStatementIdInValidTextArrangementShouldThrowBadRequestServiceException()
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {

		Long statementId = 1234L;

		try {
			textblockService.verifyTextArrangement(null, statementId);
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}

	}

	@Test
	void setTextArrangementWithValidStatementIdValidTaskIdValidTextArrangemenetShouldSaveTextArrangement()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException,
			NotFoundServiceException {

		Long statementId = 1234L;
		String taskId = "taskId";
		String statementTypeName = "statementTypeName";
		List<Textblock> textArrangement = new ArrayList<>();

		TblStatement mockedStatement = Mockito.mock(TblStatement.class);

		TaskInfo ti = new TaskInfo();
		Mockito.when(statementProcessService.getTaskInfo(Mockito.eq(statementId), Mockito.eq(taskId)))
				.thenReturn(Optional.of(ti));

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockedStatement));

		TblWorkflowdata mockedWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockedStatement.getWorkflowdata()).thenReturn(mockedWorkflowData);

		TblTextblockdefinition textblockDefinition = Mockito.mock(TblTextblockdefinition.class);
		TblStatementtype statementType = Mockito.mock(TblStatementtype.class);
		Mockito.when(statementType.getName()).thenReturn(statementTypeName);

		Mockito.when(mockedStatement.getType()).thenReturn(statementType);
		Mockito.when(mockedWorkflowData.getTextBlockDefinition()).thenReturn(textblockDefinition);

		ArgumentCaptor<TblWorkflowdata> wfdCaptor = ArgumentCaptor.forClass(TblWorkflowdata.class);
		ArgumentCaptor<List> taCaptor = ArgumentCaptor.forClass(List.class);

		textblockService.setTextArrangement(statementId, taskId, textArrangement);
		Mockito.verify(mockedWorkflowData).setDraft(taCaptor.capture());
		Mockito.verify(workflowDataRepository).save(wfdCaptor.capture());

		assertTrue(textArrangement == taCaptor.getValue());
		assertTrue(mockedWorkflowData == wfdCaptor.getValue());

	}

	@Test
	void setTextArrangementWithInValidStatementIdShouldThrowNotFoundServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {
		Long statementId = 404L;
		String taskId = "taskId";
		List<Textblock> textArrangement = new ArrayList<>();

		TaskInfo ti = new TaskInfo();
		Mockito.when(statementProcessService.getTaskInfo(Mockito.eq(statementId), Mockito.eq(taskId)))
				.thenReturn(Optional.of(ti));

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());

		try {
			textblockService.setTextArrangement(statementId, taskId, textArrangement);
			fail("Should have thrown NotFoundServiceException");
		} catch (NotFoundServiceException e) {
			// pass
		}

	}

	@Test
	void setTextArrangementWithValidStatementIdNotYetWorkflowDataShouldThrowNotFoundServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {
		Long statementId = 1234L;
		String taskId = "taskId";
		List<Textblock> textArrangement = new ArrayList<>();

		TblStatement mockedStatement = Mockito.mock(TblStatement.class);

		TaskInfo ti = new TaskInfo();
		Mockito.when(statementProcessService.getTaskInfo(Mockito.eq(statementId), Mockito.eq(taskId)))
				.thenReturn(Optional.of(ti));

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockedStatement));

		Mockito.when(mockedStatement.getWorkflowdata()).thenReturn(null);

		try {
			textblockService.setTextArrangement(statementId, taskId, textArrangement);
			fail("Should have thrown NotFoundServiceException");
		} catch (NotFoundServiceException e) {
			// pass
		}
	}

	@Test
	void compileTextArrangement() throws NotFoundServiceException, InternalErrorServiceException,
			ForbiddenServiceException, BadRequestServiceException {
		Long statementId = 1234L;
		List<Textblock> blocks = new ArrayList<>();

		AttachmentFile attachment = new AttachmentFile();

		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(statement));

		TblWorkflowdata wfd = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(statement.getWorkflowdata()).thenReturn(wfd);

		TblTextblockdefinition tbd = new TblTextblockdefinition();
		Mockito.when(wfd.getTextBlockDefinition()).thenReturn(tbd);

		Mockito.when(statement.getDueDate()).thenReturn(LocalDate.now());
		Mockito.when(statement.getReceiptDate()).thenReturn(LocalDate.now());
		Mockito.when(statement.getCity()).thenReturn("city");
		Mockito.when(statement.getDistrict()).thenReturn("district");
		Mockito.when(statement.getId()).thenReturn(statementId);
		Mockito.when(statement.getTitle()).thenReturn("title");
		String contactId = "contactId";
		Mockito.when(statement.getContactDbId()).thenReturn(contactId);
		TblStatementtype statementType = Mockito.mock(TblStatementtype.class);
		Mockito.when(statementType.getName()).thenReturn("statementType");
		Mockito.when(statement.getType()).thenReturn(statementType);
		Map<String, List<String>> sectors = new HashMap<>();
		sectors.put("district0", new ArrayList<>());
		List<String> d1sectors = new ArrayList<>();
		d1sectors.add("sector1");
		d1sectors.add("sector2");
		sectors.put("city#district", d1sectors);
		Map<String, List<String>> selects = new HashMap<>();
		List<String> select = new ArrayList<>();
		select.add("sel1");
		select.add("sel2");
		select.add("sel3");
		selects.put("select", select);
		Mockito.when(statementService.getAllSectors(Mockito.eq(statementId))).thenReturn(sectors);

		CompanyContactBlockModel contact = new CompanyContactBlockModel();
		contact.setCommunity("community");
		contact.setCommunitySuffix("communitySuffix");
		contact.setCompany("company");
		contact.setEmail("email");
		contact.setFirstName("firstName");
		contact.setHouseNumber("houseNumber");
		contact.setLastName("lastname");
		contact.setPostCode("postCode");
		contact.setSalutation("salutation");
		contact.setStreet("street");
		contact.setTitle("title");
		Mockito.when(statementService.getContactBlock(Mockito.eq(statementId))).thenReturn(Optional.of(contact));

		Mockito.when(statementCompileService.generatePDF(Mockito.any(TextConfiguration.class), Mockito.eq(contact),
				Mockito.eq(blocks))).thenReturn(attachment);
		AttachmentFile result = textblockService.compileTextArrangement(statementId, blocks);
		assertTrue(attachment == result);
	}

	@Test
	void compileTextArrangementWithInvalidStatementIdShouldThrowNotFoundServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {
		Long statementId = 1234L;
		List<Textblock> blocks = new ArrayList<>();

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());

		try {
			textblockService.compileTextArrangement(statementId, blocks);
			fail("Should have thrown NotFoundServiceException");
		} catch (NotFoundServiceException e) {
			// pass
		}
	}

	@Test
	void compileTextArrangementWithInvalidContactBlockShouldThrowInternalErrorServiceException()
			throws NotFoundServiceException, ForbiddenServiceException, InternalErrorServiceException,
			BadRequestServiceException {
		Long statementId = 1234L;
		List<Textblock> blocks = new ArrayList<>();

		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(statement));
		TblWorkflowdata wfd = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(statement.getWorkflowdata()).thenReturn(wfd);

		TblTextblockdefinition tbd = new TblTextblockdefinition();
		Mockito.when(wfd.getTextBlockDefinition()).thenReturn(tbd);

		Mockito.when(statement.getDueDate()).thenReturn(LocalDate.now());
		Mockito.when(statement.getReceiptDate()).thenReturn(LocalDate.now());
		Mockito.when(statement.getCity()).thenReturn("city");
		Mockito.when(statement.getDistrict()).thenReturn("district");
		Mockito.when(statement.getId()).thenReturn(statementId);
		Mockito.when(statement.getTitle()).thenReturn("title");
		String contactId = "contactId";
		Mockito.when(statement.getContactDbId()).thenReturn(contactId);
		TblStatementtype statementType = Mockito.mock(TblStatementtype.class);
		Mockito.when(statementType.getName()).thenReturn("statementType");
		Mockito.when(statement.getType()).thenReturn(statementType);
		Map<String, List<String>> sectors = new HashMap<>();
		sectors.put("district0", new ArrayList<>());
		List<String> d1sectors = new ArrayList<>();
		d1sectors.add("sector1");
		d1sectors.add("sector2");
		sectors.put("city#district", d1sectors);
		Map<String, List<String>> selects = new HashMap<>();
		List<String> select = new ArrayList<>();
		select.add("sel1");
		select.add("sel2");
		select.add("sel3");
		selects.put("select", select);
		Mockito.when(statementService.getAllSectors(Mockito.eq(statementId))).thenReturn(sectors);

		Mockito.when(statementService.getContactBlock(Mockito.eq(statementId))).thenReturn(Optional.empty());

		try {
			textblockService.compileTextArrangement(statementId, blocks);
			fail("Should have thrown InternalErrorServiceException");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void validateTextArrangement()
			throws InternalErrorServiceException, NotFoundServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		List<Textblock> blocks = new ArrayList<>();

		Textblock block = new Textblock();
		block.setType(TextblockType.text);
		String content = "Test block I Infodata:\n" + "**Contact data:**\n" + "* Community: <t:c-community>\n"
				+ "* CommunitySuffix: <t:c-communitySuffix>\n" + "* Company: <t:c-company>\n" + "* Email: <t:c-email>\n"
				+ "* FirstName: <t:c-firstName>\n" + "* LastName: <t:c-lastName>\n"
				+ "* HouseNumber: <t:c-houseNumber>\n" + "* PostCode: <t:c-postCode>\n"
				+ "* Salutation: <t:c-salutation>\n" + "* Street: <t:c-street>\n" + "* Title: <t:c-title>\n"
				+ "**Info data:**\n" + "* Title: <t:title>\n" + "* DueDate: <t:dueDate>\n"
				+ "* ReceiptDate: <t:receiptDate>\n" + "* id:  <t:id>\n" + "* City: <t:city>\n"
				+ "* District: <t:district>\n" + "* Sectors: <t:sectors>\n";
		block.setReplacement(content);
		blocks.add(block);

		block = new Textblock();
		block.setType(TextblockType.block);
		block.setTextblockId("tbId1");

		blocks.add(block);

		block = new Textblock();
		block.setType(TextblockType.block);
		block.setTextblockId("tbId2");
		block.setReplacement(content);
		blocks.add(block);

		block = new Textblock();
		block.setType(TextblockType.block);
		block.setTextblockId("tbId3");
		Map<String, String> placeholderValues = new HashMap<>();
		placeholderValues.put("<s:select>", "1");
		placeholderValues.put("<d:date>", "10.10.2020");
		placeholderValues.put("<f:freetext>", "a freetext");
		block.setPlaceholderValues(placeholderValues);
		blocks.add(block);

		block = new Textblock();
		block.setType(TextblockType.newline);
		blocks.add(block);

		block = new Textblock();
		block.setType(TextblockType.pagebreak);
		blocks.add(block);

		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(statement));

		TblWorkflowdata wfd = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(statement.getWorkflowdata()).thenReturn(wfd);

		TblTextblockdefinition tbd = new TblTextblockdefinition();
		Mockito.when(wfd.getTextBlockDefinition()).thenReturn(tbd);

		TextblockDefinition definition = new TextblockDefinition();
		List<TextblockGroup> groups = new ArrayList<>();
		TextblockGroup group = new TextblockGroup();
		groups.add(group);
		group.setGroupName("group1");
		List<TextblockItem> textBlocks = new ArrayList<>();
		TextblockItem tb1 = new TextblockItem();
		tb1.setId("tbId1");
		tb1.setText("That is a simple Text");
		tb1.setExcludes(new ArrayList<>());
		List<TextblockRequirement> tb1requires = new ArrayList<>();
		TextblockRequirement reqtb1and = new TextblockRequirement();
		reqtb1and.setType(TextblockRequirementType.and);
		reqtb1and.setIds(new ArrayList<>());
		reqtb1and.getIds().add("tbId2");
		reqtb1and.getIds().add("tbId3");
		tb1requires.add(reqtb1and);
		tb1.setRequires(tb1requires);
		textBlocks.add(tb1);

		TextblockItem tb2 = new TextblockItem();
		tb2.setId("tbId2");
		tb2.setText("Full of placeholders <s:select> <d:date> <f:freetext> ");
		tb2.setExcludes(new ArrayList<>());
		List<TextblockRequirement> tb2requires = new ArrayList<>();
		TextblockRequirement reqtb2and = new TextblockRequirement();
		reqtb2and.setType(TextblockRequirementType.and);
		reqtb2and.setIds(new ArrayList<>());
		reqtb2and.getIds().add("tbId1");
		reqtb2and.getIds().add("tbId3");
		tb2requires.add(reqtb2and);
		tb2.setRequires(tb2requires);
		textBlocks.add(tb2);

		TextblockItem tb3 = new TextblockItem();
		tb3.setId("tbId3");
		tb3.setText("Full of placeholders <s:select> <d:date> <f:freetext> ");
		tb3.setExcludes(new ArrayList<>());
		List<TextblockRequirement> tb3requires = new ArrayList<>();
		TextblockRequirement reqtb3and = new TextblockRequirement();
		reqtb3and.setType(TextblockRequirementType.and);
		reqtb3and.setIds(new ArrayList<>());
		reqtb3and.getIds().add("tbId1");
		reqtb3and.getIds().add("tbId2");
		tb3requires.add(reqtb3and);
		tb3.setRequires(tb3requires);
		textBlocks.add(tb3);

		TextblockItem tb4 = new TextblockItem();
		tb4.setId("tbId4");
		tb4.setText("Full");
		tb4.setExcludes(new ArrayList<>());
		List<TextblockRequirement> tb4requires = new ArrayList<>();
		TextblockRequirement reqtb4or = new TextblockRequirement();
		reqtb4or.setType(TextblockRequirementType.or);
		reqtb4or.setIds(new ArrayList<>());
		reqtb4or.getIds().add("tbId1");
		reqtb4or.getIds().add("tbId2");
		reqtb4or.getIds().add("tbId3");
		tb4requires.add(reqtb4or);
		tb4.setRequires(tb4requires);

		textBlocks.add(tb4);

		TextblockItem tb5 = new TextblockItem();
		tb5.setId("tbId5");
		tb5.setText("Full");
		List<TextblockRequirement> tb5requires = new ArrayList<>();
		TextblockRequirement reqtb5xor = new TextblockRequirement();
		reqtb5xor.setType(TextblockRequirementType.xor);
		reqtb5xor.setIds(new ArrayList<>());
		reqtb5xor.getIds().add("tbId4");
		reqtb5xor.getIds().add("tbId6");
		tb5requires.add(reqtb5xor);
		tb5.setRequires(tb5requires);
		tb5.setExcludes(new ArrayList<>());
		tb5.getExcludes().add("tbId6");
		textBlocks.add(tb5);

		TextblockItem tb6 = new TextblockItem();
		tb6.setId("tbId6");
		tb6.setText("Nobody wants me");
		tb6.setExcludes(new ArrayList<>());
		tb6.setRequires(new ArrayList<>());
		textBlocks.add(tb6);

		group.setTextBlocks(textBlocks);
		definition.setGroups(groups);
		tbd.setDefinition(definition);

		Mockito.when(statement.getDueDate()).thenReturn(LocalDate.now());
		Mockito.when(statement.getReceiptDate()).thenReturn(LocalDate.now());
		Mockito.when(statement.getCity()).thenReturn("city");
		Mockito.when(statement.getDistrict()).thenReturn("district");
		Mockito.when(statement.getId()).thenReturn(statementId);
		Mockito.when(statement.getTitle()).thenReturn("title");
		String contactId = "contactId";
		Mockito.when(statement.getContactDbId()).thenReturn(contactId);
		TblStatementtype statementType = Mockito.mock(TblStatementtype.class);
		Mockito.when(statementType.getName()).thenReturn("statementType");
		Mockito.when(statement.getType()).thenReturn(statementType);
		Map<String, List<String>> sectors = new HashMap<>();
		sectors.put("district0", new ArrayList<>());
		List<String> d1sectors = new ArrayList<>();
		d1sectors.add("sector1");
		d1sectors.add("sector2");
		sectors.put("city#district", d1sectors);
		Map<String, List<String>> selects = new HashMap<>();
		List<String> select = new ArrayList<>();
		select.add("sel1");
		select.add("sel2");
		select.add("sel3");
		selects.put("select", select);
		definition.setSelects(selects);
		definition.setNegativeGroups(new ArrayList<>());

		Mockito.when(statementService.getAllSectors(Mockito.eq(statementId))).thenReturn(sectors);

		CompanyContactBlockModel contact = new CompanyContactBlockModel();
		contact.setCommunity("community");
		contact.setCommunitySuffix("communitySuffix");
		contact.setCompany("company");
		contact.setEmail("email");
		contact.setFirstName("firstName");
		contact.setHouseNumber("houseNumber");
		contact.setLastName("lastname");
		contact.setPostCode("postCode");
		contact.setSalutation("salutation");
		contact.setStreet("street");
		contact.setTitle("title");
		Mockito.when(contactService.getContactDetails(Mockito.eq(contactId), Mockito.eq(true)))
				.thenReturn(Optional.of(contact));

		ValidationResult result = textblockService.validate(statementId, blocks);

		assertTrue(result.isValid());
	}

	@Test
	void validateWithInvalidTextArrangement()
			throws NotFoundServiceException, ForbiddenServiceException, InternalErrorServiceException {
		Long statementId = 1234L;
		List<Textblock> blocks = new ArrayList<>();

		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(statement));

		TblWorkflowdata wfd = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(statement.getWorkflowdata()).thenReturn(wfd);

		TblTextblockdefinition tbd = new TblTextblockdefinition();
		Mockito.when(wfd.getTextBlockDefinition()).thenReturn(tbd);

		TextblockDefinition definition = new TextblockDefinition();
		List<TextblockGroup> groups = new ArrayList<>();

		TextblockGroup group = new TextblockGroup();
		groups.add(group);
		group.setGroupName("group1");

		List<TextblockItem> textBlocks = new ArrayList<>();
		TextblockItem tb1 = new TextblockItem();
		tb1.setId("tbId1");
		tb1.setText("<f:freetext>");
		List<TextblockRequirement> tb1requires = new ArrayList<>();
		TextblockRequirement reqtb1and = new TextblockRequirement();
		reqtb1and.setType(TextblockRequirementType.and);
		reqtb1and.setIds(new ArrayList<>());
		reqtb1and.getIds().add("tbId2");
		tb1requires.add(reqtb1and);
		TextblockRequirement reqtb1xor = new TextblockRequirement();
		reqtb1xor.setType(TextblockRequirementType.xor);
		reqtb1xor.setIds(new ArrayList<>());
		reqtb1xor.getIds().add("tbId3");
		reqtb1xor.getIds().add("tbId4");
		tb1requires.add(reqtb1xor);
		TextblockRequirement reqtb1or = new TextblockRequirement();
		reqtb1or.setType(TextblockRequirementType.or);
		reqtb1or.setIds(new ArrayList<>());
		reqtb1or.getIds().add("tbId2");
		tb1requires.add(reqtb1or);
		tb1.setRequires(tb1requires);
		tb1.setExcludes(new ArrayList<>());
		tb1.getExcludes().add("tbId3");
		textBlocks.add(tb1);

		TextblockItem tb2 = new TextblockItem();
		tb2.setId("tbId2");
		tb2.setText("Empty");
		tb2.setExcludes(new ArrayList<>());
		tb2.setRequires(new ArrayList<>());
		textBlocks.add(tb2);

		TextblockItem tb3 = new TextblockItem();
		tb3.setId("tbId3");
		tb3.setText("Empty");
		tb3.setExcludes(new ArrayList<>());
		tb3.setRequires(new ArrayList<>());
		textBlocks.add(tb3);

		TextblockItem tb4 = new TextblockItem();
		tb4.setId("tbId4");
		tb4.setText("Empty");
		tb4.setExcludes(new ArrayList<>());
		tb4.setRequires(new ArrayList<>());
		textBlocks.add(tb4);

		group.setTextBlocks(textBlocks);
		definition.setGroups(groups);
		tbd.setDefinition(definition);

		Textblock block;

		block = new Textblock();
		block.setType(TextblockType.block);
		block.setTextblockId("tbId3");
		blocks.add(block);

		block = new Textblock();
		block.setType(TextblockType.block);
		block.setTextblockId("tbId1");
		blocks.add(block);

		block = new Textblock();
		block.setType(TextblockType.block);
		block.setTextblockId("tbId4");
		blocks.add(block);

		Mockito.when(statement.getDueDate()).thenReturn(LocalDate.now());
		Mockito.when(statement.getReceiptDate()).thenReturn(LocalDate.now());
		Mockito.when(statement.getCity()).thenReturn("city");
		Mockito.when(statement.getDistrict()).thenReturn("district");
		Mockito.when(statement.getId()).thenReturn(statementId);
		Mockito.when(statement.getTitle()).thenReturn("title");
		String contactId = "contactId";
		Mockito.when(statement.getContactDbId()).thenReturn(contactId);
		TblStatementtype statementType = Mockito.mock(TblStatementtype.class);
		Mockito.when(statementType.getName()).thenReturn("statementType");
		Mockito.when(statement.getType()).thenReturn(statementType);
		Map<String, List<String>> sectors = new HashMap<>();
		sectors.put("district0", new ArrayList<>());
		List<String> d1sectors = new ArrayList<>();
		d1sectors.add("sector1");
		d1sectors.add("sector2");
		sectors.put("city#district", d1sectors);
		Map<String, List<String>> selects = new HashMap<>();
		List<String> select = new ArrayList<>();
		select.add("sel1");
		select.add("sel2");
		select.add("sel3");
		selects.put("select", select);
		definition.setSelects(selects);
		definition.setNegativeGroups(new ArrayList<>());

		Mockito.when(statementService.getAllSectors(Mockito.eq(statementId))).thenReturn(sectors);

		CompanyContactBlockModel contact = new CompanyContactBlockModel();
		contact.setCommunity("community");
		contact.setCommunitySuffix("communitySuffix");
		contact.setCompany("company");
		contact.setEmail("email");
		contact.setFirstName("firstName");
		contact.setHouseNumber("houseNumber");
		contact.setLastName("lastname");
		contact.setPostCode("postCode");
		contact.setSalutation("salutation");
		contact.setStreet("street");
		contact.setTitle("title");

		Mockito.when(contactService.getContactDetails(Mockito.eq(contactId), Mockito.eq(true)))
				.thenReturn(Optional.of(contact));

		ValidationResult result = textblockService.validate(statementId, blocks);
		assertFalse(result.isValid());

		List<ValidationError> errors = result.getErrors();
		assertTrue(errors.size() == 2);
		ValidationError errortb3 = errors.get(0);
		assertEquals(errortb3.getArrangementId(), 0);
		assertEquals(errortb3.getTextBlockId(), "tbId3");
		assertEquals(errortb3.getTextBlockGroup(), "group1");
		assertEquals("tbId1", errortb3.getAfter());

		ValidationError errortb1 = errors.get(1);
		assertEquals(errortb1.getArrangementId(), 1);
		assertEquals(errortb1.getTextBlockId(), "tbId1");
		assertEquals(errortb1.getTextBlockGroup(), "group1");

		assertEquals(1, errortb1.getExcludes().size());
		assertEquals("tbId3", errortb1.getExcludes().get(0));

		assertEquals(1, errortb1.getMissingVariables().size());
		assertEquals("<f:freetext>", errortb1.getMissingVariables().get(0));

		List<TextblockRequirement> requires = errortb1.getRequires();
		assertEquals(3, requires.size());
		TextblockRequirement andReq = null;
		TextblockRequirement xorReq = null;
		TextblockRequirement orReq = null;

		for (TextblockRequirement req : requires) {
			switch (req.getType()) {
			case and:
				andReq = req;
				break;
			case xor:
				xorReq = req;
				break;
			case or:
				orReq = req;
				break;
			}
		}

		assertNotNull(andReq);
		assertEquals(1, andReq.getIds().size());
		assertEquals("tbId2", andReq.getIds().get(0));

		assertNotNull(xorReq);
		assertEquals(2, xorReq.getIds().size());
		assertTrue(xorReq.getIds().contains("tbId3"));
		assertTrue(xorReq.getIds().contains("tbId4"));

		assertNotNull(orReq);
		assertEquals(1, orReq.getIds().size());
		assertTrue(orReq.getIds().contains("tbId2"));

	}

	@Test
	void verifyTextArrangementWithInvalidBlockNull()
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		List<Textblock> blocks = new ArrayList<>();

		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(statement));

		TblWorkflowdata wfd = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(statement.getWorkflowdata()).thenReturn(wfd);

		TblTextblockdefinition tbd = new TblTextblockdefinition();
		Mockito.when(wfd.getTextBlockDefinition()).thenReturn(tbd);
		TextblockDefinition definition = new TextblockDefinition();
		definition.setGroups(new ArrayList<>());
		definition.setNegativeGroups(new ArrayList<>());
		definition.setSelects(new HashMap<>());

		tbd.setDefinition(definition);

		TblStatementtype statementType = Mockito.mock(TblStatementtype.class);
		Mockito.when(statementType.getName()).thenReturn("statementType");
		Mockito.when(statement.getType()).thenReturn(statementType);

		// null block
		try {
			blocks.clear();
			blocks.add(null);
			textblockService.verifyTextArrangement(blocks, statementId);
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}

		// null type
		try {
			blocks.clear();
			Textblock block = new Textblock();
			block.setType(null);
			blocks.add(block);
			textblockService.verifyTextArrangement(blocks, statementId);
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}

		// invalid textblockId
		try {
			blocks.clear();
			Textblock block = new Textblock();
			block.setType(TextblockType.block);
			block.setTextblockId("notFound");
			blocks.add(block);
			textblockService.verifyTextArrangement(blocks, statementId);
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}

		// text block without replacement
		try {
			blocks.clear();
			Textblock block = new Textblock();
			block.setType(TextblockType.text);
			block.setReplacement(null);
			blocks.add(block);
			textblockService.verifyTextArrangement(blocks, statementId);
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}

	}

}
