/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import org.eclipse.openk.statementpublicaffairs.api.MailUtil;
import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationNotifyService;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailConfiguration;
import org.eclipse.openk.statementpublicaffairs.model.mail.MessageConfiguration;
import org.eclipse.openk.statementpublicaffairs.model.mail.NewMailContext;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationNotifyService.class)
@ActiveProfiles("test")
class NotifyServiceTest {

	@Autowired
	private MailUtil mailUtil;
	@Autowired
	private NotifyService notificationService;

	@Autowired
	private MailService mailService;

	@Test
	void notifyNewStatementMailInboxNotificationShouldcallMailServiceSendNotifyMail() throws InternalErrorServiceException {
		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationInboxNewMail(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);
		notificationService.notifyNewStatementMailInboxNotification();
		Mockito.verify(mailService).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

	@Test
	void notifyApprovedShouldCallMailServiceSendNotifyMail() throws InternalErrorServiceException {
		Long statementId = 1234L;
		Object value = true;

		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationApprovedAndSent(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);
		notificationService.notifyApproved(statementId, value);
		Mockito.verify(mailService).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

	@Test
	void notifyEnrichDraft() throws InternalErrorServiceException {
		Long statementId = 1234L;

		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationEnrichDraft(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);
		notificationService.notifyEnrichDraft(statementId);
	}

	@Test
	void notifyApprovalRequired() throws InternalErrorServiceException {
		Long statementId = 1234L;

		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationApprovalRequired(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);
		notificationService.notifyApprovalRequired(statementId);

	}

	@Test
	void notifyOnAllMandatoryContributions() throws InternalErrorServiceException {
		Long statementId = 1234L;
		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationAllMandatoryContributions(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);
		notificationService.notifyOnAllMandatoryContributions(statementId);
		Mockito.verify(mailService).sendNotifyMail(Mockito.any(NewMailContext.class));
	}
	
	@Test
	void notifyNotApproves() throws InternalErrorServiceException {
		Long statementId = 1234L;
		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationNotApproved(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);
		notificationService.notifyNotApproved(statementId);
		Mockito.verify(mailService).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

}
