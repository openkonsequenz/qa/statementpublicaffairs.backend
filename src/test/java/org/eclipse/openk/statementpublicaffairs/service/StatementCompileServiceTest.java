/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationStatementCompileService;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.model.Textblock;
import org.eclipse.openk.statementpublicaffairs.model.Textblock.TextblockType;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;
import org.eclipse.openk.statementpublicaffairs.model.TextblockGroup;
import org.eclipse.openk.statementpublicaffairs.model.TextblockItem;
import org.eclipse.openk.statementpublicaffairs.viewmodel.TextConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationStatementCompileService.class)
@ActiveProfiles("test")
class StatementCompileServiceTest {

	@Autowired
	private StatementCompileService statementCompileService;

	@Test
	void test() throws InternalErrorServiceException, IOException, BadRequestServiceException {
	
		TextConfiguration textConfiguration = new TextConfiguration();
		
		TextblockDefinition configuration = new TextblockDefinition();
		Map<String, List<String>> selects = new HashMap<>();
		configuration.setSelects(selects);
	
		List<TextblockGroup> groups = new ArrayList<>();
		TextblockGroup group = new TextblockGroup();
		group.setGroupName("GroupName1");
		List<TextblockItem> textBlocks = new ArrayList<>();
		TextblockItem item = new TextblockItem();
		
		String content = "Test block I Infodata:\n" 
				+ "**Contact data:**\n"
				+ "* Community: <t:c-community>\n"
				+ "* CommunitySuffix: <t:c-communitySuffix>\n"
				+ "* Company: <t:c-company>\n"
				+ "* Email: <t:c-email>\n"
				+ "* FirstName: <t:c-firstName>\n"
				+ "* LastName: <t:c-lastName>\n"
				+ "* HouseNumber: <t:c-houseNumber>\n"
				+ "* PostCode: <t:c-postCode>\n"
				+ "* Salutation: <t:c-salutation>\n"
				+ "* Street: <t:c-street>\n"
				+ "* Title: <t:c-title>\n"
				+ "**Info data:**\n"
				+ "* Title: <t:title>\n"
				+ "* DueDate: <t:dueDate>\n"
				+ "* ReceiptDate: <t:receiptDate>\n"
				+ "* id:  <t:id>\n"
				+ "* City: <t:city>\n"
				+ "* District: <t:district>\n"
				+ "* Sectors: <t:sectors>\n";
				
		item.setText(content);
		item.setId("TextTextblock");
		textBlocks.add(item);
		group.setTextBlocks(textBlocks);
		groups.add(group);
		configuration.setGroups(groups);

		CompanyContactBlockModel contact = new CompanyContactBlockModel();
		contact.setCommunity("Community");
		contact.setCommunitySuffix("CommunitySuffix");
		contact.setCompany("Company");
		contact.setEmail("name@company.tld");
		contact.setFirstName("FirstName");
		contact.setHouseNumber("12b");
		contact.setLastName("LastName");
		contact.setPostCode("91234");
		contact.setSalutation("Herr");
		contact.setStreet("Main Street");
		contact.setTitle("Dr. Prof.");
		
		Map<String, String> replacements = new HashMap<>();

		replacements.put("id", "Id");
		replacements.put("title", "Title");

		replacements.put("dueDate", "DueDate");

		replacements.put("receiptDate", "ReceiptDate");

		replacements.put("city", "City");
		replacements.put("district", "District");
		replacements.put("type", "StatementType");
		replacements.put("sectors", "Sector1, Sector2");
		
		replacements.put("c-community", contact.getCommunity());
		replacements.put("c-communitySuffix", contact.getCommunitySuffix());
		replacements.put("c-company", contact.getCompany());
		replacements.put("c-email", contact.getEmail());
		replacements.put("c-firstName", contact.getFirstName());
		replacements.put("c-houseNumber", contact.getHouseNumber());
		replacements.put("c-lastName", contact.getLastName());
		replacements.put("c-postCode", contact.getPostCode());
		replacements.put("c-salutation", contact.getSalutation());
		replacements.put("c-street", contact.getStreet());
		replacements.put("c-title", contact.getTitle());
		
		textConfiguration.setReplacements(replacements);
		textConfiguration.setConfiguration(configuration);
		
		List<Textblock> blocks = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			
			Textblock block = new Textblock();
			block.setType(TextblockType.text);
			block.setReplacement("" + i + " .That is a text twith newlines\nand **bold words** and __some italic__ words.\n* Bullet point one\n* Bullet point two dsf sdfadsf asdf asefsad fasd fsad f adsf adsf dsaf ads fsd fas df adsf ads f sadf asdf sda fdsa f sdaf dsaf sad fsad f asdf sdaf ds fds\n* Bullet point two\n");
			blocks.add(block);
			
			for (int j = 0; j < 5; j++) {
				block= new Textblock();
				block.setType(TextblockType.newline);
				blocks.add(block);
			}
			
			Textblock pagebreak = new Textblock();
			pagebreak.setType(TextblockType.pagebreak);
			blocks.add(pagebreak);

			block = new Textblock();
			block.setType(TextblockType.text);
			block.setReplacement("sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.");
			blocks.add(block);
			
			block = new Textblock();
			block.setType(TextblockType.block);
			block.setReplacement("That's a textblock block\n with replacementtext\n");
			block.setTextblockId("TextTextblock");
			blocks.add(block);
			
			block = new Textblock();
			block.setType(TextblockType.block);
			block.setTextblockId("TextTextblock");
			blocks.add(block);
			
			block = new Textblock();
			block.setType(TextblockType.text);
			block.setReplacement("A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A B B B B A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A A B B B B");
			blocks.add(block);
		}
		AttachmentFile attachmentFile = statementCompileService.generatePDF(textConfiguration, contact, blocks);
	  byte[] buffer = new byte[attachmentFile.getRessource().available()];
	  attachmentFile.getRessource().read(buffer);
	  assertNotNull(attachmentFile);
	  assertEquals("Statement.pdf", attachmentFile.getName());
	  assertTrue(attachmentFile.getLength() > 10);
	  assertNotNull(attachmentFile.getRessource());
	  assertEquals("application/pdf", attachmentFile.getType());
	 		
	}

}
