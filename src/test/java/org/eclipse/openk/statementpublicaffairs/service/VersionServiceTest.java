/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Test verifies VersionService interfaces.
 * 
 * @author Tobias Stummer
 *
 */
@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@ActiveProfiles("test")

class VersionServiceTest {

    @Autowired
    private VersionService versionService;

    @Test
    void test() {
        String testBuildVersion = "1.2.3-test";
        String testApplicationName = "testApplicatioName";
        ReflectionTestUtils.setField(versionService, "buildVersion", testBuildVersion);
        ReflectionTestUtils.setField(versionService, "applicationName", testApplicationName);
        assertEquals(testBuildVersion, versionService.getBuildVersion());
        assertEquals(testApplicationName, versionService.getApplicationName());
    }

}
