/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.api;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConfigurationException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailConfiguration;
import org.eclipse.openk.statementpublicaffairs.service.mail.MailSession;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@ActiveProfiles("test")
class MailUtilTest {

	@Autowired
	private MailUtil mailUtil;

	@Value("${mail.account.statement.properties}")
	private String statementMailPropertiesPath;

	@Test
	void loadMailPropertiesShouldRespondWithProperties() throws ConfigurationException {

		Properties props = mailUtil.loadMailProperties("classpath:statement.properties");
		assertNotNull(props);
	}

	@Test
	void loadMailPropertiesInvalidPathShouldThrowConfigurationException() throws ConfigurationException {

		try {
			mailUtil.loadMailProperties("classpath:notexistentFile.properies");
			fail("Should have thrown ConfigurationException");
		} catch (ConfigurationException e) {
			// pass
		}
	}

	@Test
	void sessionForShouldRespondMailSession() throws ConfigurationException {
		MailSession mailSession = mailUtil.sessionFor("classpath:statement.properties");
		assertNotNull(mailSession);
	}

	@Test
	void sessionForInvalidPathShouldThrowConfigurationException() throws ConfigurationException {
		try {
			mailUtil.sessionFor("classpath:notexistentFile.properties");
			fail("Should have thrown ConfigurationException");
		} catch (ConfigurationException e) {
			// pass
		}
	}

	@Test
	void validEmailAddressShouldReturnTrue() {
		assertTrue(mailUtil.validEmailAddress("abc@def.de"));
	}

	@Test
	void validEmailAddressInvalidShouldReturnFalse() {
		assertFalse(mailUtil.validEmailAddress("thats not an e-mail address"));
	}

	@Test
	void getMailConfigAndfilterRecipients() throws InternalErrorServiceException {
		MailConfiguration mailConfig = mailUtil.getMailConfiguration();
		Set<String> recipients = new HashSet<>();
		recipients.add("test@host.tld");
		mailUtil.filterRecipients(recipients, mailConfig);
		assertTrue(recipients.contains("test@host.tld"));
	}
}
