/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.config;

import javax.annotation.PostConstruct;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.api.MailUtil;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConfigurationException;
import org.eclipse.openk.statementpublicaffairs.service.MailService;
import org.eclipse.openk.statementpublicaffairs.service.NotifyService;
import org.eclipse.openk.statementpublicaffairs.service.StatementAuthorizationService;
import org.eclipse.openk.statementpublicaffairs.service.mail.MailContext;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

@EntityScan(basePackageClasses = StatementPublicAffairsApplication.class)
@ContextConfiguration(initializers = { ConfigFileApplicationContextInitializer.class })
@TestPropertySource("spring.config.location=classpath:application-test.yml")
public class TestConfigurationMailService {

	@MockBean
	private MailUtil mailUtil;

	@MockBean
	private NotifyService notificationService;

	@MockBean
	private StatementAuthorizationService authorizationService;

	@Bean
	public MailService myMailService() {
		return new MailService();
	}

	@Value("${mail.account.statement.properties}")
	private String statementMailPropertiesPath;

	@Value("${mail.account.notification.properties}")
	private String notificationMailPropertiesPath;

	@PostConstruct
	public void prepare() throws ConfigurationException {
		String statementEmail = "statement@testmail.xyz";
		String notificationEmail = "notify@testmail.xyz";
		MailContext statementMailContext = Mockito.mock(MailContext.class);
		MailContext notificationMailContext = Mockito.mock(MailContext.class);
		Mockito.when(statementMailContext.getSender()).thenReturn(statementEmail);
		Mockito.when(notificationMailContext.getSender()).thenReturn(notificationEmail);
		Mockito.when(mailUtil.contextFor(Mockito.eq(statementMailPropertiesPath))).thenReturn(statementMailContext);
		Mockito.when(mailUtil.contextFor(Mockito.eq(notificationMailPropertiesPath)))
				.thenReturn(notificationMailContext);
	}

}
