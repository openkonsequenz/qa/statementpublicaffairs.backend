/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.config;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.api.MailUtil;
import org.eclipse.openk.statementpublicaffairs.service.MailService;
import org.eclipse.openk.statementpublicaffairs.service.NotifyService;
import org.eclipse.openk.statementpublicaffairs.service.StatementAuthorizationService;
import org.eclipse.openk.statementpublicaffairs.service.UsersService;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

@EntityScan(basePackageClasses = StatementPublicAffairsApplication.class)
@ContextConfiguration(initializers = { ConfigFileApplicationContextInitializer.class })
@TestPropertySource("spring.config.location=classpath:application-test.yml")
public class TestConfigurationNotifyService {

	@MockBean
	private MailService mailService;

	@MockBean
	private UsersService usersService;

	@MockBean
	private MailUtil mailUtil;

	@MockBean
	private StatementAuthorizationService authorizationService;

	@Bean
	public NotifyService myNotifyService() {
		return new NotifyService();
	}

}
