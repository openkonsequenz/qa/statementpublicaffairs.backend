/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.config;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.api.AuthNAuthApi;
import org.eclipse.openk.statementpublicaffairs.repository.ReqDepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.UserRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementReqdepartmentUsersRepository;
import org.eclipse.openk.statementpublicaffairs.service.SessionService;
import org.eclipse.openk.statementpublicaffairs.service.StatementAuthorizationService;
import org.eclipse.openk.statementpublicaffairs.service.UserInfoService;
import org.eclipse.openk.statementpublicaffairs.service.UsersService;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

@EntityScan(basePackageClasses = StatementPublicAffairsApplication.class)
@ContextConfiguration(initializers = { ConfigFileApplicationContextInitializer.class })
@TestPropertySource("spring.config.location=classpath:application-test.yml")
public class TestConfigurationUsersService {

	@MockBean
	private AuthNAuthApi authNAuthApi;

	@MockBean
	private SessionService sessionService;

	@MockBean
	private UserRepository userRepository;

	@MockBean
	private StatementAuthorizationService authorizationService;

	@MockBean
	private VwStatementReqdepartmentUsersRepository vwStatemetnReqDepartmentUsersRepository;

	@MockBean
	private ReqDepartmentRepository reqDepartmentRepository;

	@MockBean
	private UserInfoService userInfoService;

	@Bean
	public UsersService myUsersService() {
		return new UsersService();
	}

}
