*******************************************************************************
  Copyright (c) 2019 Contributors to the Eclipse Foundation

  See the NOTICE file(s) distributed with this work for additional
  information regarding copyright ownership.

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License v. 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
*******************************************************************************

# Camunda docker build configuration

Please use the docker-compose from the project root to build the camunda docker image.

## Initial setup

The setup.sh contains a list of REST requests to set up the camunda administrator and spa_service user and deploy the 'Stellungnahmen.bpmn'.

This can be used as a reference for an initial configuration of the camunda service for the statement module.

